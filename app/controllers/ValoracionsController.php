<?php

use CodolGestio\Repositories\ImmoblesRepository;
use CodolGestio\Repositories\CiutatsRepository;
use CodolGestio\Repositories\OficinesRepository;
use CodolGestio\Repositories\RegimsRepository;
use CodolGestio\Repositories\EstatsRepository;
use CodolGestio\Repositories\TipusRepository;
use CodolGestio\Repositories\PropietarisRepository;
use CodolGestio\Repositories\TagsRepository;
use CodolGestio\Repositories\ValoracionsRepository;

class ValoracionsController extends \BaseController {

	function __construct(ValoracionsRepository $valoracionsRepo, CiutatsRepository $ciutatsRepo, OficinesRepository $oficinesRepo,
                         RegimsRepository $regimsRepo, EstatsRepository $estatsRepo, TipusRepository $tipusRepo,
                         PropietarisRepository $propietarisRepo, TagsRepository $tagsRepo, ImmoblesRepository $immoblesRepo)
    {
        $this->valoracionsRepo = $valoracionsRepo;
        $this->immoblesRepo = $immoblesRepo;
        $this->ciutatsRepo = $ciutatsRepo;
        $this->oficinesRepo = $oficinesRepo;
        $this->regimsRepo = $regimsRepo;
        $this->estatsRepo = $estatsRepo;
        $this->tipusRepo = $tipusRepo;
        $this->propietarisRepo = $propietarisRepo;
        $this->tagsRepo = $tagsRepo;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$valoracions = $this->valoracionsRepo->getAllByOffice(Auth::user()->oficina_id);

        return View::make('valoracions.index', compact('valoracions'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $oficines = $this->oficinesRepo->getAll();
        $regims = $this->regimsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');
        $tipus = $this->tipusRepo->getAll()->lists('nom', 'id');
        $tags = $this->tagsRepo->getAll()->lists('nom', 'id');
        $propietaris = $this->propietarisRepo->getAllOnOffice(Auth::user()->oficina_id)->lists('full_name', 'id');

        return View::make('valoracions.create', compact('ciutats', 'oficines', 'regims', 'estats', 'tipus', 'tags', 'propietaris'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();

        if ($input['propietari_id'] == "" && $input['nom'] == "")
            return Redirect::back()->withInput()->with('notification', 'Has d\'associar l\'immoble amb un propietari.');

        if (! isset($input['financament']))
            $input['financament'] = 0;

        if (! isset($input['cedula_habitabilitat']))
            $input['cedula_habitabilitat'] = 0;

        if (Input::get('ciutat_name') != "")
        {
            $ciutat = $this->ciutatsRepo->store(Input::get('ciutat_name'));
            $input['ciutat_id'] = $ciutat->id;
        }

        $oficina_id = Auth::user()->oficina->id;
        $input['oficina_id'] = $oficina_id;

        if (Input::get('nom')) {
            $propietari = $this->propietarisRepo->store(Input::only(array('oficina_id', 'nom', 'cognoms', 'nif', 'adreca', 'email', 'telefon')));
            $input['propietari_id'] = $propietari->id;
        }
        $immoble = $this->immoblesRepo->store($input);

        $this->oficinesRepo->augmentarExpedient($oficina_id);

        return Redirect::route('valoracio.pujar-imatges', array('id' => $immoble->id));
	}

	/**
     * Mostra el formulari per pujar imatges a l'immoble $id.
     *
     * @param $id
     * @return mixed
     */
    public function createImages($id)
    {
        $images = $this->immoblesRepo->getImages($id);

        return View::make('valoracions.uploadImages', compact('id', 'images'));
    }


    /**
     * Puja la imatge destacada de l'immoble.
     *
     * @param $id
     * @return mixed
     */
    public function uploadImages($id)
    {
        ini_set('memory_limit', '256M'); // Augmentem la memòria per tractar les imatges

        $image = Image::make(Input::file('file'));

        $destinationPath = '/var/www/vhosts/codolgestio.net/httpdocs/content/';

        $filename = $id . '-' . str_random(12);

        // Pujem tres tamanys d'imatge.
        $upload_success3 = $image->resize(null, 1000, function ($constraint) { $constraint->aspectRatio(); })->save($destinationPath . $filename . '-big.jpg');
        $upload_success1 = $image->fit(900, 600)->save($destinationPath . $filename . '.jpg');
        $upload_success2 = $image->fit(350, 233)->save($destinationPath . $filename . '-thumb.jpg');
        $this->immoblesRepo->attachImage($id, $filename);

        if ($upload_success1 && $upload_success2 && $upload_success3) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$immoble = $this->valoracionsRepo->getById($id);

        return View::make('valoracions.show', compact('immoble'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$immoble = $this->immoblesRepo->getById($id);
        if (Auth::user()->oficina_id != $immoble->oficina_id)
        {
            return Redirect::home()->with('notification', 'Ep! No tens permís per accedir aquí.');
        }
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $regims = $this->regimsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');
        $tipus = $this->tipusRepo->getAll()->lists('nom', 'id');
        $tags = $this->tagsRepo->getAll()->lists('nom', 'id');
        $propietaris = $this->propietarisRepo->getAllOnOffice(Auth::user()->oficina_id)->lists('full_name', 'id');

        return View::make('valoracions.edit', compact('immoble', 'ciutats', 'regims', 'estats', 'tipus', 'tags', 'propietaris'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();

        if (! isset($input['financament']))
            $input['financament'] = 0;

        if (! isset($input['cedula_habitabilitat']))
            $input['cedula_habitabilitat'] = 0;

        if (Input::get('ciutat_name') != "")
        {
            $ciutat = $this->ciutatsRepo->store(Input::get('ciutat_name'));
            $input['ciutat_id'] = $ciutat->id;
        }
        if (Input::get('nom')) {
            $propietari = $this->propietarisRepo->store(Input::only(array('oficina_id', 'nom', 'cognoms', 'nif', 'adreca', 'email', 'telefon')));
            $input['propietari_id'] = $propietari->id;
        }

        $immoble = $this->immoblesRepo->update($id, $input);

        return Redirect::route('valoracions.visualitzar', ['id' => $immoble->id]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->valoracionsRepo->destroy($id);

        return Redirect::route('valoracions');
	}

    public function convertir($id)
    {
        $this->valoracionsRepo->convertir($id);
        $this->oficinesRepo->augmentarExpedient(Auth::user()->oficina->id);

        return Redirect::route('immoble.visualitzar', ['id' => $id]);
    }


}
