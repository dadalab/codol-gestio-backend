<?php

use CodolGestio\Repositories\PropietarisRepository;

class PropietarisController extends \BaseController {

    protected $propietarisRepo;

    protected $rules = array(
        'nom' => 'required',
        'cognoms' => 'required',
        'adreca' => 'required',
        'email' => 'required:email',
        'telefon' => 'required'
    );

    function __construct(PropietarisRepository $propietarisRepo)
    {
        $this->propietarisRepo = $propietarisRepo;
    }


    /**
	 * Mostra el llistat de propietaris.
	 *
	 * @return Response
	 */
	public function index()
	{
        $propietaris = $this->propietarisRepo->getAllOnOffice(Auth::user()->oficina_id);

        return View::make('propietaris.index', compact('propietaris'));
	}


	/**
	 * Mostra el formulari per crear un nou propietari.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('propietaris.create');
	}


	/**
	 * Guarda un nou propietari a la base de dades.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['oficina_id'] = Auth::user()->id;
        $this->propietarisRepo->store($input);

        return Redirect::intended('propietaris');
	}


	/**
	 * Mostra el propietari.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$propietari = $this->propietarisRepo->getById($id);

        return View::make('propietaris.show', compact('propietari'));
	}


	/**
	 * Mostra el formulari per modificar el formulari.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$propietari = $this->propietarisRepo->getById($id);

        return View::make('propietaris.edit', compact('propietari'));
	}


	/**
	 * Actualitza el propietari.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        /*$validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails())
            return Redirect::back()->withInput()->withErrors($validator);
		*/
        $this->propietarisRepo->update($id, Input::all());

        return Redirect::route('propietari.visualitzar', $id);
	}


	/**
	 * Arxiva el propietari.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->propietarisRepo->destroy($id, Input::get('motiu_arxivar'));

		return Redirect::route('propietaris');
	}


}
