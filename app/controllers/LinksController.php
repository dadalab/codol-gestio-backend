<?php

use CodolGestio\Repositories\ImmoblesRepository;

class LinksController extends \BaseController
{
    public function __construct(ImmoblesRepository $immoblesRepo)
    {
        $this->immoblesRepo = $immoblesRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $immobles = $this->immoblesRepo->getByOffice(Auth::user()->oficina_id);

        return View::make('links.index', compact('immobles'));
    }

    public function enviar()
    {
        $nom = Input::get('nom');
        $email = Input::get('email');
        $text = Input::get('text');
        $immobles = $this->immoblesRepo->get(Input::get('marcats'));
        $link = "http://codolgestio.net/cercador/?ids=" . implode(',', Input::get('marcats'));

        Mail::send('links.email', ['nom' => $nom, 'email' => $email, 'text' => $text, 'link' => $link, 'immobles' => $immobles], function ($m) use ($nom, $email, $link, $immobles, $text) {
            $m->from(Auth::user()->email, 'CòdolGestió');

            $m->to($email, $nom)->subject('Els immobles que hem seleccionat');
        });

        return Redirect::route('enllacos')->with('missatge', 'Email enviat correctament a ' . $nom . ' (' . $email . ').');
    }
}
