<?php

class DashboardController extends \BaseController {


	/**
	 * Mostra la portada de l'usuari registrat.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('dashboard.index');
	}



}
