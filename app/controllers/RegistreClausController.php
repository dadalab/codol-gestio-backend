
<?php

use CodolGestio\Repositories\RegistreClausRepository;
use CodolGestio\Repositories\ImmoblesRepository;

class RegistreClausController extends \BaseController {


    protected $registreClausRepo;
    protected $immoblesRepo;

    function __construct(RegistreClausRepository $registreClausRepo, ImmoblesRepository $immoblesRepo)
    {
        $this->registreClausRepo = $registreClausRepo;
        $this->immoblesRepo = $immoblesRepo;

    }


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $immobles = $this->immoblesRepo->getByOffice(Auth::user()->oficina_id);

        return View::make('registre_claus.index', compact('immobles'));
	}


}
