<?php

use CodolGestio\Repositories\ImmoblesRepository;

class PDFController extends \BaseController {

	protected $immoblesRepo;

	public function __construct(ImmoblesRepository $immoblesRepo)
	{
		$this->immoblesRepo = $immoblesRepo;
	}

	public function selectImmobles()
	{
		$immobles = $this->immoblesRepo->getByOffice(Auth::user()->oficina_id);

		return View::make('pdfs.immobles-selector', compact('immobles'));
	}

	public function generate()
	{
		$numImmobles = count(Input::get('immobles'));

		$immobles = CodolGestio\Immoble::find(Input::get('immobles'));

		if ($numImmobles == 4)
		{
			return View::make('pdfs.4immobles', compact('immobles'));
		}
		else if ($numImmobles == 1)
		{
			return View::make('pdfs.1immoble', compact('immobles'));
		}
		else return 'no has seleccionat el nombre d\'immobles correcte...';
	}


}
