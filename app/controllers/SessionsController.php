<?php

class SessionsController extends \BaseController
{

    protected $validator;

    protected $rules = array(
        'username' => 'required',
        'password' => 'required'
    );


    public function __construct()
    {

    }


    /**
     * Formulari de login.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('sessions.create');
    }


    /**
     * Logueja l'usuari a l'aplicació.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')))) {
            return Redirect::intended('/');
        }

        return Redirect::back()->withInput()->withErrors('Aquesta combinació d\'usuari i contrasenya no existeix');
    }


    /**
     * Sortir de l'aplicació.
     *
     * @return Response
     */
    public function destroy()
    {
        Auth::logout();

        return Redirect::intended('/login')->with('notification', 'Has sortit correctament del sistema');
    }

}
