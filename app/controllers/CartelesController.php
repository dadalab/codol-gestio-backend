<?php

use CodolGestio\Repositories\ImmoblesRepository;

class CartelesController extends \BaseController {

	protected $immoblesRepo;

	public function __construct(ImmoblesRepository $immoblesRepo)
	{
		$this->immoblesRepo = $immoblesRepo;
	}

	public function selectImmobles()
	{
		$immobles = $this->immoblesRepo->getByOffice(Auth::user()->oficina_id);

		return View::make('carteles.immobles-selector', compact('immobles'));
	}

	public function generate($immobleId)
	{
		$immoble = CodolGestio\Immoble::find($immobleId);

		return View::make('carteles.immoble', compact('immoble'));
	}


}
