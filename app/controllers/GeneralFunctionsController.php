<?php

use CodolGestio\Repositories\ImmoblesRepository;


class GeneralFunctionsController extends \BaseController
{

    public function __construct(ImmoblesRepository $immoblesRepo)
    {
        $this->immoblesRepo = $immoblesRepo;
    }

    private function cleanString($text)
    {
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'A',
            '/[ÍÌÎÏ]/u' => 'I',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'O',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'U',
            '/ç/' => 'c',
            '/Ç/' => 'C',
            '/ñ/' => 'n',
            '/Ñ/' => 'N',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => '', // Literally a single quote
            '/[“”«»„]/u' => '', // Double quote
            '/ /' => '-', // nonbreaking space (equiv. to 0x160)
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }

    public function generateURL()
    {
        $immobles = $this->immoblesRepo->getAll();

        foreach ($immobles as $immoble) {
            $url = $immoble->tipus->nom . '-' . $immoble->regim->nom . '-' . $immoble->ciutat->nom . '-' . $immoble->id;
            $url = strtolower($url);
            $url = $this->cleanString($url);
            $url = str_replace(' ', '-', $url);
            $immoble->url = $url;
            $immoble->save();
        }
        echo "Proces completat correctament!";
    }


}