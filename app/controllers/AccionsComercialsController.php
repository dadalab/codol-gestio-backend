<?php

use CodolGestio\Repositories\AccionsComercialsRepository;

class AccionsComercialsController extends \BaseController
{
    public function __construct(AccionsComercialsRepository $accionsComercialsRepo)
    {
        $this->accionsComercialsRepo = $accionsComercialsRepo;
    }


    public function store($immoble_id)
    {
        $immobles = $this->accionsComercialsRepo->store($immoble_id, Input::all());

        return Redirect::back();
    }

    public function update($id)
    {
        $this->accionsComercialsRepo->update($id, Input::all());

        return Redirect::back();
    }

    public function destroy($id)
    {
        $this->accionsComercialsRepo->destroy($id);

        return Redirect::back();
    }
}
