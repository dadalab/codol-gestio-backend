<?php

use CodolGestio\Repositories\ImmoblesRepository;
use CodolGestio\Repositories\CiutatsRepository;
use CodolGestio\Repositories\OficinesRepository;
use CodolGestio\Repositories\RegimsRepository;
use CodolGestio\Repositories\EstatsRepository;
use CodolGestio\Repositories\TipusRepository;
use CodolGestio\Repositories\PropietarisRepository;
use CodolGestio\Repositories\TagsRepository;

class ExportController extends \BaseController
{
    protected $immoblesRepo;
    protected $ciutatsRepo;
    protected $regimsRepo;
    protected $estatsRepo;
    protected $tipusRepo;
    protected $propietarisRepo;
    protected $tagsRepo;

    protected $rules = array(
        'oficina_id' => 'required',
        'num_expedient' => 'required',
        'titol' => 'required',
        'descripcio' => 'required',
        'tipus_id' => 'required',
        'regim_id' => 'required',
        'estat_id' => 'required',
        'ciutat_id' => 'required',
    );

    function __construct(ImmoblesRepository $immoblesRepo, CiutatsRepository $ciutatsRepo, OficinesRepository $oficinesRepo,
                         RegimsRepository $regimsRepo, EstatsRepository $estatsRepo, TipusRepository $tipusRepo,
                         PropietarisRepository $propietarisRepo, TagsRepository $tagsRepo)
    {
        $this->immoblesRepo = $immoblesRepo;
        $this->ciutatsRepo = $ciutatsRepo;
        $this->oficinesRepo = $oficinesRepo;
        $this->regimsRepo = $regimsRepo;
        $this->estatsRepo = $estatsRepo;
        $this->tipusRepo = $tipusRepo;
        $this->propietarisRepo = $propietarisRepo;
        $this->tagsRepo = $tagsRepo;
    }


    public function index()
    {
        $immobles = $this->immoblesRepo->getByOffice(Auth::user()->oficina_id);

        return View::make('export.index', compact('immobles'));
    }

    /**
     * Exporta immobles a Yaencontre.
     *
     * @return mixed
     */
    public function yaencontre()
    {
        $immobles = $this->immoblesRepo->getAll();
        $oficines = $this->oficinesRepo->getAll();
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');

        $content = View::make('export.yaencontre', compact('immobles', 'oficines', 'ciutats'));
        return Response::make($content, '200')->header('Content-Type', 'text/xml');
    }

    /**
     * Exporta immobles a Fotocasa.
     *
     * @return mixed
     */
    public function fotocasa($id_oficina)
    {
        $immobles = $this->immoblesRepo->getFotocasaByOffice($id_oficina);
        $oficines = $this->oficinesRepo->getAll();
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');

        $content = View::make('export.fotocasa', compact('immobles', 'oficines', 'ciutats','estatst'));
        return Response::make($content, '200')->header('Content-Type', 'text/xml');
    }

    public function reclam($idOficina)
    {
        $immobles = $this->immoblesRepo->getReclamByOffice($idOficina);
        $oficines = $this->oficinesRepo->getAll();
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');

        $content = View::make('export.reclam', compact('immobles', 'oficines', 'ciutats','estatst'));
        return Response::make($content, '200')->header('Content-Type', 'text/xml');
    }

    public function habitaclia($idOficina)
    {
        $immobles = $this->immoblesRepo->getHabitacliaByOffice($idOficina);
        $oficines = $this->oficinesRepo->getAll();
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');

        $content = View::make('export.habitaclia', compact('immobles', 'oficines', 'ciutats','estatst'));
        return Response::make($content, '200')->header('Content-Type', 'text/xml');   
    }

}
