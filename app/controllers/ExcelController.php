<?php

use Carbon\Carbon;
use CodolGestio\Immoble;
use CodolGestio\Client;
use CodolGestio\Propietari;
use CodolGestio\Visita;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ExcelController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return View::make('excels.index');
    }

    public function download()
    {
        $fields = Input::get('checks');
        $table = Input::get('taula');

        $resultats = ""; 
        if ($table == 'immoble')
        {
            $resultats = Immoble::with('propietari', 'tipus', 'regim', 'estat', 'ciutat')->where('oficina_id', '=', Auth::user()->oficina_id)->get();
            foreach ($resultats as $resultat)
            {
                if (isset($resultat->propietari->nom)) $resultat->propietari_id = $resultat->propietari->nom . ' ' . $resultat->propietari->cognoms;
                $resultat->tipus_id = $resultat->tipus->nom;
                $resultat->regim_id = $resultat->regim->nom;
                $resultat->estat_id = $resultat->estat->nom;
                $resultat->ciutat_id = $resultat->ciutat->nom;
            }

        }
        else if ($table == 'client')
        {
            $resultats = Client::with('regim', 'poblacioImmoble', 'poblacioClient', 'tipus')->where('oficina_id', '=', Auth::user()->oficina_id)->get();
            foreach($resultats as $resultat)
            {
                if (isset($resultat->regim->nom)) $resultat->regim_id = $resultat->regim->nom;
                if (isset($resultat->tipus->nom)) $resultat->tipus_id = $resultat->tipus->nom;
                if (isset($resultat->poblacioClient->nom)) $resultat->ciutat_id = $resultat->poblacioClient->nom;
                if (isset($resultat->poblacioImmoble->nom)) $resultat->poblacio_id = $resultat->poblacioImmoble->nom;
            }
        }
        else if ($table == 'propietari')
        {
            $resultats = Propietari::where('oficina_id', '=', Auth::user()->oficina_id)->get();
        }

        else if ($table == 'visites')
        {
            $inici = Carbon::createFromFormat('d-m-Y', Input::get('data_inici'))->format('Y-m-d');
            $fi = Carbon::createFromFormat('d-m-Y', Input::get('data_fi'))->format('Y-m-d');
            //dd($inici . $fi);
            $resultats = Visita::where('data', '>=', $inici)->where('data', '<=', $fi)->with('client')->leftJoin('immobles', 'immobles.id', '=', 'visites.immoble_id')->where('oficina_id', Auth::user()->oficina_id)->get();
            foreach($resultats as $resultat)
            {
                if (isset($resultat->client->nom)) $resultat->client_id = $resultat->client->nom . ' ' . $resultat->client->cognoms . ' | ' . $resultat->client->telefon_1;
                //if (isset($resultat->data)) $resultat->data = $resultat->data->format('d-m-Y');
            }
        }
        
        $resultats = $resultats->toArray();

        Excel::create('Exportació ' . $table . ' ' . Carbon::now()->format('d-m-Y'), function ($excel) use ($table, $resultats, $fields)
        {
            $excel->sheet('1', function ($sheet) use ($resultats, $fields)
            {
                $sheet->loadView('excels.general')->with('fields', $fields)->with('resultats', $resultats);
            });
        })->export('xls');

        //return Redirect::back();
    }
}