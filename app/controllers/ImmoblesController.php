<?php

use CodolGestio\Repositories\ImmoblesRepository;
use CodolGestio\Repositories\CiutatsRepository;
use CodolGestio\Repositories\OficinesRepository;
use CodolGestio\Repositories\RegimsRepository;
use CodolGestio\Repositories\EstatsRepository;
use CodolGestio\Repositories\TipusRepository;
use CodolGestio\Repositories\PropietarisRepository;
use CodolGestio\Repositories\TagsRepository;

class ImmoblesController extends \BaseController
{
    protected $immoblesRepo;
    protected $ciutatsRepo;
    protected $regimsRepo;
    protected $estatsRepo;
    protected $tipusRepo;
    protected $propietarisRepo;
    protected $tagsRepo;

    protected $rules = array(
        'oficina_id' => 'required',
        'num_expedient' => 'required',
        'titol' => 'required',
        'descripcio' => 'required',
        'tipus_id' => 'required',
        'regim_id' => 'required',
        'estat_id' => 'required',
        'ciutat_id' => 'required',
    );

    public function __construct(ImmoblesRepository $immoblesRepo, CiutatsRepository $ciutatsRepo, OficinesRepository $oficinesRepo,
                         RegimsRepository $regimsRepo, EstatsRepository $estatsRepo, TipusRepository $tipusRepo,
                         PropietarisRepository $propietarisRepo, TagsRepository $tagsRepo)
    {
        $this->immoblesRepo = $immoblesRepo;
        $this->ciutatsRepo = $ciutatsRepo;
        $this->oficinesRepo = $oficinesRepo;
        $this->regimsRepo = $regimsRepo;
        $this->estatsRepo = $estatsRepo;
        $this->tipusRepo = $tipusRepo;
        $this->propietarisRepo = $propietarisRepo;
        $this->tagsRepo = $tagsRepo;
    }


    /**
     * Mostra el llistat dels immobles.
     *
     * @return mixed
     */
    public function index()
    {
        $immobles = $this->immoblesRepo->getAll();
        $oficines = $this->oficinesRepo->getAll();
        $regims = $this->regimsRepo->getAll();
        $tipus = $this->tipusRepo->getAll();
        $estats = $this->estatsRepo->getAll();

        return View::make('immobles.index-list', compact('immobles', 'oficines', 'tipus', 'regims', 'estats'));
    }

    public function indexQuadricula()
    {
        $immobles = $this->immoblesRepo->getAll();
        $oficines = $this->oficinesRepo->getAll();
        $regims = $this->regimsRepo->getAll();
        $tipus = $this->tipusRepo->getAll();
        $estats = $this->estatsRepo->getAll();
        
        return View::make('immobles.index', compact('immobles', 'oficines', 'tipus', 'regims', 'estats'));
    }


    /**
     * Mostra el formulari per crear un immoble
     *
     * @return mixed
     */
    public function create()
    {
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $oficines = $this->oficinesRepo->getAll();
        $regims = $this->regimsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');
        $tipus = $this->tipusRepo->getAll()->lists('nom', 'id');
        $tags = $this->tagsRepo->getAll()->lists('nom', 'id');
        $propietaris = $this->propietarisRepo->getAllOnOffice(Auth::user()->oficina_id)->lists('full_name', 'id');

        return View::make('immobles.create', compact('ciutats', 'oficines', 'regims', 'estats', 'tipus', 'tags', 'propietaris'));
    }


    /**
     * Guarda un immoble a la base de dades.
     *
     * @return mixed
     */
    public function store()
    {
        $input = Input::all();

        if ($input['propietari_id'] == "" && $input['nom'] == "") {
            return Redirect::back()->withInput()->with('notification', 'Has d\'associar l\'immoble amb un propietari.');
        }

        if (! isset($input['financament'])) {
            $input['financament'] = 0;
        }

        if (! isset($input['cedula_habitabilitat'])) {
            $input['cedula_habitabilitat'] = 0;
        }

        if (Input::get('ciutat_name') != "") {
            $ciutat = $this->ciutatsRepo->store(Input::get('ciutat_name'));
            $input['ciutat_id'] = $ciutat->id;
        }

        $oficina_id = Auth::user()->oficina->id;
        $input['oficina_id'] = $oficina_id;

        if (Input::get('nom')) {
            $propietari = $this->propietarisRepo->store(Input::only(array('oficina_id', 'nom', 'cognoms', 'nif', 'adreca', 'email', 'telefon')));
            $input['propietari_id'] = $propietari->id;
        }
        $immoble = $this->immoblesRepo->store($input);

        $this->oficinesRepo->augmentarExpedient($oficina_id);

        return Redirect::route('immoble.pujar-imatges', array('id' => $immoble->id));
    }


    /**
     * Mostra el formulari per pujar imatges a l'immoble $id.
     *
     * @param $id
     * @return mixed
     */
    public function createImages($id)
    {
        $images = $this->immoblesRepo->getImages($id);

        return View::make('immobles.uploadImages', compact('id', 'images'));
    }


    /**
     * Puja la imatge destacada de l'immoble.
     *
     * @param $id
     * @return mixed
     */
    public function uploadImages($id)
    {
        ini_set('memory_limit', '256M'); // Augmentem la memòria per tractar les imatges

        $image = Image::make(Input::file('file'));
        //$destinationPath = 'imatges/immobles/';
        $destinationPath = '/var/www/vhosts/codolgestio.net/httpdocs/content/';

        $filename = $id . '-' . str_random(12);

        // Pujem tres tamanys d'imatge.
        $upload_success3 = $image->resize(null, 1000, function ($constraint) { $constraint->aspectRatio(); })->save($destinationPath . $filename . '-big.jpg');
        $upload_success1 = $image->fit(900, 600)->save($destinationPath . $filename . '.jpg');
        $upload_success2 = $image->fit(350, 233)->save($destinationPath . $filename . '-thumb.jpg');
        $this->immoblesRepo->attachImage($id, $filename);

        if ($upload_success1 && $upload_success2 && $upload_success3) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }


    /**
     * Canviem la imatge destacada de l'immoble.
     *
     * @param $id
     */
    public function changeFeaturedImage($id)
    {
        $this->immoblesRepo->changeFeaturedImage($id, Input::get('imatge_id'));
    }


    /**
     * Eliminem una imatge de l'immoble
     *
     * @param $id
     */
    public function removeImage($id)
    {
        return $this->immoblesRepo->removeImage($id, Input::get('imatge_id'));
    }


    /**
     * Mostra l'immoble.
     *
     * @param  int $id
     * @param  int $seg 1 = seguent | 2 = anterior
     * @return Response
     */
    public function show($id, $seg = 0)
    {
        $immoble = $this->immoblesRepo->getById($id);

        if (! $immoble) {
            $immoble = $this->immoblesRepo->getNextByOffice($id, Auth::user()->oficina_id, $seg);
        }

        return View::make('immobles.show', compact('immoble'));
    }

    public function imprimir($id)
    {
        $immoble = $this->immoblesRepo->getById($id);

        return View::make('immobles.imprimir', compact('immoble'));
    }


    /**
     * Mostra el formulari per modificar un immoble.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $immoble = $this->immoblesRepo->getById($id);
        if (Auth::user()->oficina_id != $immoble->oficina_id) {
            return Redirect::home()->with('notification', 'Ep! No tens permís per accedir aquí.');
        }
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $regims = $this->regimsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');
        $tipus = $this->tipusRepo->getAll()->lists('nom', 'id');
        $tags = $this->tagsRepo->getAll()->lists('nom', 'id');
        $propietaris = $this->propietarisRepo->getAllOnOffice(Auth::user()->oficina_id)->lists('full_name', 'id');

        return View::make('immobles.edit', compact('immoble', 'ciutats', 'regims', 'estats', 'tipus', 'tags', 'propietaris'));
    }


    /**
     * Actualitza l'immoble.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();

        if (! isset($input['financament'])) {
            $input['financament'] = 0;
        }

        if (! isset($input['cedula_habitabilitat'])) {
            $input['cedula_habitabilitat'] = 0;
        }

        if (Input::get('ciutat_name') != "") {
            $ciutat = $this->ciutatsRepo->store(Input::get('ciutat_name'));
            $input['ciutat_id'] = $ciutat->id;
        }
        if (Input::get('nom')) {
            $propietari = $this->propietarisRepo->store(Input::only(array('oficina_id', 'nom', 'cognoms', 'nif', 'adreca', 'email', 'telefon')));
            $input['propietari_id'] = $propietari->id;
        }

        $immoble = $this->immoblesRepo->update($id, $input);

        return Redirect::route('immoble.visualitzar', ['id' => $immoble->id]);
    }


    /**
     * Arxiva l'immoble.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->immoblesRepo->destroy($id, Input::get('motiu_arxivar'));

        return Redirect::route('immobles');
    }


    /**
     * Reordenar les imatges de l'immoble
     *
     * @param $id
     */
    public function ordenarImatges($id)
    {
        $imatges = Input::get('imatges');

        $this->immoblesRepo->orderImages($imatges);
    }


    /**
     * Actualitza els valors 'radio' de l'immoble (per ajax).
     *
     * @param $id
     */
    public function updateRadios($id)
    {
        $this->immoblesRepo->updateRadios($id, Input::get('data_name'), Input::get('data_value'));
    }

    public function updateTipusReclam($id)
    {
        $this->immoblesRepo->updateTipusReclam($id, Input::get('data_name'), Input::get('data_value'));
    }

    /**
     * Mostra el formulari per pujar documents a l'immoble $id.
     *
     * @param $id
     * @return mixed
     */
    public function createDocuments($id)
    {
        $images = $this->immoblesRepo->getImages($id);

        return View::make('immobles.createDocuments', compact('id', 'images'));
    }

    /**
     * Puja document a l'immoble.
     *
     * @param $id
     * @return mixed
     */
    public function uploadDocuments($id)
    {
        $file = Input::file('file');

        $destinationPath = 'documents/immobles';
        $filename = $id . '-' . $file->getClientOriginalName();

        $upload_success = $file->move($destinationPath, $filename);

        $this->immoblesRepo->attachFile($id, $filename);

        if ($upload_success) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }

    /**
     * Eliminem un document de l'immoble
     *
     * @param $id
     */
    public function eliminarDocument($id)
    {
        return $this->immoblesRepo->removeDocument($id, Input::get('fitxer'));
    }

    public function visibilitatImatge($id)
    {
        return $this->immoblesRepo->visibilitatImatge($id, Input::get('imatge_id'), Input::get('visible'));
    }

    public function arxivats()
    {
        $immobles = $this->immoblesRepo->getArxivats(Auth::user()->oficina_id);

        return View::make('immobles.archived', compact('immobles'));
    }

    public function recuperar($id)
    {
        $this->immoblesRepo->recuperar($id);

        return Redirect::back()->with('notification', 'Immoble desarxivat. <a href="' . URL::route('immoble.visualitzar', $id) . '">Anar a veure\'l</a>');
    }

    public function eliminar($id)
    {
        $this->immoblesRepo->eliminar($id);

        return Redirect::back()->with('notification', 'Immoble eliminat del sistema.');
    }

    public function triarDestacat()
    {
        $immobles = $this->immoblesRepo->getByOffice(Auth::user()->oficina_id)->lists('buscador_select', 'id');
        ;
        $destacat = $this->oficinesRepo->getDestacat(Auth::user()->oficina_id);

        return View::make('immobles.triar-destacat', compact('immobles', 'destacat'));
    }

    public function canviarDestacat()
    {
        $this->oficinesRepo->canviarDestacat(Auth::user()->oficina_id, Input::get('immoble_id'));

        return Redirect::back();
    }

    public function segueixWeb($id)
    {
        $this->immoblesRepo->afegirSeguidorWeb($id, Input::get('email'));

        return Response::json(['type' => 'success']);
    }

    public function destroyClientInteressat($immoble, $client)
    {
        $this->immoblesRepo->eliminarClientInteressat($immoble, $client);

        return Redirect::back();
    }

    public function enviarEmail($id)
    {
        $clients = $this->immoblesRepo->getById($id)->clientsInteressats;
        $data = array('missatge' => Input::get('missatge'), 'assumpte' => Input::get('assumpte'));
       
        foreach ($clients as $client) {
            if ($client->email != "") {
                Mail::send('emails.plain', $data, function ($message) use ($data) {
                    $message->to($client->email, 'CòdolGestió')->subject($data['assumpte'])->replyTo(Auth::user()->email);
                });
            }
        }

        return Redirect::back()->with('message', 'Emails enviats correctament!');
    }

    public function APIGet($id)
    {
        return $this->immoblesRepo->getById($id);
    }
}
