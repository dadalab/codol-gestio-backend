<?php

use CodolGestio\Repositories\TVRepository;
use CodolGestio\Repositories\ImmoblesRepository;

class TVController extends \BaseController {

	protected $tvRepo;
	protected $immoblesRepo;

	public function __construct(TVRepository $tvRepo, ImmoblesRepository $immoblesRepo)
	{
		$this->tvRepo = $tvRepo;
		$this->immoblesRepo = $immoblesRepo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tvs = $this->tvRepo->getAll(Auth::user()->oficina_id);
		$immobles = $this->immoblesRepo->getByOffice(Auth::user()->oficina_id)->lists('buscador_select', 'id');

		return View::make('tvs.index', compact('tvs', 'immobles'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($immobleId)
	{
		$id = $this->tvRepo->store(Auth::user()->oficina_id, $immobleId);
			
		return $this->tvRepo->get($id);
	}

	public function show($oficina)
	{
		$oficines = ['vic' => 1, 'manlleu' => 2, 'torello' => 3];
		$tvs = $this->tvRepo->getAll($oficines[$oficina]);

		return View::make('tvs.show', compact('tvs'));		
	}

	public function destroy($id)
	{
		$this->tvRepo->destroy($id, Auth::user()->oficina_id);
	}

	public function orderUp($id, $order)
	{
		$this->tvRepo->setOrderUp($id, $order, Auth::user()->oficina_id);
	}

	public function orderDown($id, $order)
	{
		$this->tvRepo->setOrderDown($id, $order, Auth::user()->oficina_id);
	}

	public function setNumFotos($id, $numFotos)
	{
		$this->tvRepo->setFotos($id, $numFotos);
	}
}
