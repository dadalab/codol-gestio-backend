<?php

use CodolGestio\Repositories\VisitesRepository;
use CodolGestio\Repositories\ClientsRepository;

class VisitesController extends \BaseController {

    protected $visitesRepo;

    function __construct(VisitesRepository $visitesRepo, ClientsRepository $clientsRepo)
    {
        $this->visitesRepo = $visitesRepo;
        $this->clientsRepo = $clientsRepo;
    }


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$visites = $this->visitesRepo->getByOffice(Auth::user()->oficina_id);

		return View::make('visites.index', compact('visites'));
	}


	/**
	 * Guarda una nova visita a la base de dades.
	 *
	 * @return Response
	 */
	public function store($id)
    {
		$visita = $this->visitesRepo->store($id, Input::all());
		if (Input::get('immoble_id') != "")
			$this->clientsRepo->follow($id, Input::all());

        return Redirect::back();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$id = Input::get('visita_id');
		$eliminar = Input::get('eliminar');
		if (isset($eliminar))
		{
			$this->visitesRepo->destroy($id);
		}
		else
		{
			$observacions = Input::get('observacions');
			$this->visitesRepo->update($id, $observacions);
		}
		return Redirect::back();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->visitesRepo->destroy($id);
	}


}
