<?php

use CodolGestio\Repositories\ClientsRepository;
use CodolGestio\Repositories\VisitesRepository;
use CodolGestio\Repositories\CiutatsRepository;
use CodolGestio\Repositories\RegimsRepository;
use CodolGestio\Repositories\EstatsRepository;
use CodolGestio\Repositories\TipusRepository;
use CodolGestio\Repositories\ImmoblesRepository;

class ClientsController extends \BaseController {

    protected $clientRepo;
    protected $visitesRepo;
    protected $ciutatsRepo;
    protected $regimsRepo;
    protected $estatsRepo;
    protected $tipusRepo;
    protected $immoblesRepo;

    function __construct(ClientsRepository $clientRepo, VisitesRepository $visitesRepo,
                         CiutatsRepository $ciutatsRepo, RegimsRepository $regimsRepo, EstatsRepository $estatsRepo,
                         TipusRepository $tipusRepo, ImmoblesRepository $immoblesRepo)
    {
        $this->clientRepo = $clientRepo;
        $this->visitesRepo = $visitesRepo;
        $this->ciutatsRepo = $ciutatsRepo;
        $this->regimsRepo = $regimsRepo;
        $this->estatsRepo = $estatsRepo;
        $this->tipusRepo = $tipusRepo;
        $this->immoblesRepo = $immoblesRepo;
    }


    /**
	 * Mostra el llistat d'usuaris.
	 *
	 * @return Response
	 */
	public function index()
	{
        $clients = $this->clientRepo->getAllOnOffice(Auth::user()->oficina_id);

        return View::make('clients.index', compact('clients'));
	}


	/**
	 * Mostra el formulari per afegir un immoble
	 *
	 * @return Response
	 */
	public function create()
	{
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $regims = $this->regimsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');
        $tipus = $this->tipusRepo->getAll()->lists('nom', 'id');

        return View::make('clients.create', compact('ciutats', 'regims', 'estats', 'tipus'));
	}


	/**
	 * Guarda un client a la base de dades.
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();
        $input['oficina_id'] = Auth::user()->oficina_id;
        if (Input::get('ciutat_name') != "")
        {
            $ciutat = $this->ciutatsRepo->store(Input::get('ciutat_name'));
            $input['ciutat_id'] = $ciutat->id;
        }

        $client = $this->clientRepo->store($input);

        return Redirect::route('clients');
	}


	/**
	 * Mostra el client.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$client = $this->clientRepo->getById($id);
        $visites = $this->visitesRepo->getByClient($id);
        $immobles = $this->immoblesRepo->getAll()->lists('buscador_select', 'id');

        return View::make('clients.show', compact('client', 'visites', 'immobles'));
	}

    public function imprimir($id)
    {
        $client = $this->clientRepo->getById($id);
        $visites = $this->visitesRepo->getByClient($id);
        $immobles = $this->immoblesRepo->getAll()->lists('buscador_select', 'id');

        return View::make('clients.imprimir', compact('client', 'visites', 'immobles'));
    }

	/**
	 * Mostra el formulari per modificar un client.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $client = $this->clientRepo->getById($id);
        $ciutats = $this->ciutatsRepo->getAll()->lists('nom', 'id');
        $regims = $this->regimsRepo->getAll()->lists('nom', 'id');
        $estats = $this->estatsRepo->getAll()->lists('nom', 'id');
        $tipus = $this->tipusRepo->getAll()->lists('nom', 'id');

        return View::make('clients.edit', compact('client', 'ciutats', 'regims', 'estats', 'tipus'));
	}


	/**
	 * Actualitzar el client.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $client = $this->clientRepo->update($id, Input::all());

        return Redirect::route('client.visualitzar', $client->id)->with('notification', 'Client modificat correctament.');
	}


	/**
	 * Arxiva el client.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $this->clientRepo->destroy($id, Input::get('motiu_arxivar'));

        return Redirect::route('clients');
	}

    public function arxivats()
    {
        $clients = $this->clientRepo->getArxivats(Auth::user()->oficina_id);

        return View::make('clients.archived', compact('clients'));
    }

    public function recuperar($id)
    {
        $this->clientRepo->recuperar($id);

        return Redirect::back()->with('notification', 'Client desarxivat. <a href="' . URL::route('client.visualitzar', $id) . '">Anar a veure\'l</a>');
    }

    public function eliminar($id)
    {
        $this->clientRepo->eliminar($id);

        return Redirect::back()->with('notification', 'Client eliminat del sistema.');
    }

    /**
     * Mostra el formulari per pujar documents a l'immoble $id.
     *
     * @param $id
     * @return mixed
     */
    public function createDocuments($id)
    {
        return View::make('clients.createDocuments', compact('id'));
    }

    /**
     * Puja document a l'immoble.
     *
     * @param $id
     * @return mixed
     */
    public function uploadDocuments($id)
    {
        $file = Input::file('file');

        $destinationPath = 'documents/clients';
        $filename = $id . '-' . $file->getClientOriginalName();

        $upload_success = $file->move($destinationPath, $filename);

        $this->clientRepo->attachFile($id, $filename);

        if ($upload_success) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }

    /**
     * Eliminem un document de l'immoble
     *
     * @param $id
     */
    public function eliminarDocument($id)
    {
        return $this->clientRepo->removeDocument($id, Input::get('fitxer'));
    }


}
