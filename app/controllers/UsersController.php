<?php

class UsersController extends \BaseController {

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		return View::make('users.password');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$user = Auth::user();

		if (Input::get('master_password') != "" && Input::get('master_password') == $user->master_password)
		{
			if (Input::get('password') == Input::get('password_confirm'))
			{
				if (Input::get('password') == "")
				{
					return Redirect::back()->withErrors('La contrasenya no pot estar buida!');
				}
				$user->password = Hash::make(Input::get('password'));
				$user->save();
				Session::flash('success', "Contrasenya canviada correctament");
				return Redirect::back();

			}
			else return Redirect::back()->withErrors('Les contrasenyes no coincideixen!');
		}
		else
		{
			return Redirect::back()->withErrors('No has escrit correctament la contrasenya "master".');
		}
	}

}
