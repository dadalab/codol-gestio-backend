<?php namespace CodolGestio;


class Visita extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'visites';

    //protected $dates = array('data');

    protected $guarded = [];

    public function getDataAttribute($date) {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d-m-Y');
    }  

    public function client()
    {
        return $this->belongsTo('CodolGestio\Client')->withTrashed();
    }

    public function immoble()
    {
        return $this->belongsTo('CodolGestio\Immoble')->withTrashed();
    }

} 