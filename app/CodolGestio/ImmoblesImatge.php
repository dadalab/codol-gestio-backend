<?php namespace CodolGestio;


class ImmoblesImatge extends \Eloquent {

    protected $table = 'immobles_imatges';

    protected $guarded = [];

    public function immoble()
    {
        return $this->belongsTo('CodolGestio\Immoble');
    }


} 