<?php namespace CodolGestio;


class Tag extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    protected $guarded = ['id'];

    public function immobles()
    {
        return $this->belongsToMany('CodolGestio\Immoble');
    }
}
