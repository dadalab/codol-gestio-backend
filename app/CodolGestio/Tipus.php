<?php namespace CodolGestio;


class Tipus  extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tipus';

    protected $guarded = ['id'];

}


