<?php namespace CodolGestio;


class Oficina extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oficines';

    protected $guarded = ['id'];


    public function getSeguentNumExpedientAttribute($value)
    {
        return str_pad($value, '5', '0',  STR_PAD_LEFT);
    }

    public function ciutat()
    {
        return $this->belongsTo('CodolGestio\Ciutat');
    }

    public function usuaris()
    {
        return $this->hasMany('CodolGestio\User');
    }
} 