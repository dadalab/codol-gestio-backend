<?php namespace CodolGestio;


class RegistreClaus extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registre_claus';

    public function immoble()
    {
        return $this->belongsTo('CodolGestio\Immoble');
    }
} 