<?php namespace CodolGestio;


class SeguidorWeb extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'seguidors_web';

    protected $guarded = [];

    public function immoble()
    {
        return $this->belongsTo('CodolGestio\Immoble');
    }

} 