<?php namespace CodolGestio;

class Client extends \Eloquent {

    use \SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';

    protected $guarded = ['id', 'ciutat_name'];

    protected $dates = ['deleted_at'];

    public function getDataContacteAttribute($value)
    {
        if ($value == '0000-00-00')
            return 'No registrada';
        return \Carbon::createFromFormat("Y-m-d", $value)->format("d-m-Y");
    }

    public function setDataContacteAttribute($value)
    {
        if ($value == 'No registrada')
            $this->attributes['data_contacte'] = '0000-00-00';
        $this->attributes['data_contacte'] = \Carbon::createFromFormat("d-m-Y", $value)->format("Y-m-d");
    }


    public function poblacioClient()
    {
        return $this->belongsTo('CodolGestio\Ciutat', 'ciutat_id');
    }

    public function poblacioImmoble()
    {
        return $this->belongsTo('CodolGestio\Ciutat', 'poblacio_id');
    }

    public function regim()
    {
        return $this->belongsTo('CodolGestio\Regim');
    }

    public function tipus()
    {
        return $this->belongsTo('CodolGestio\Tipus');
    }

    public function getNumberFormat($value = 'preu')
    {
        return number_format($this->attributes[$value],2,',','.');
    }

    public function documents()
    {
        return $this->hasMany('CodolGestio\ClientDocument');
    }

    public function follows()
    {
        return $this->belongsToMany('CodolGestio\Immoble');
    }

}
