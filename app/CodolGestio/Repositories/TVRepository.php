<?php namespace CodolGestio\Repositories;

use CodolGestio\TV;

class TVRepository {

    /**
     * Obté el contingut de la televisió de l'usuari $userId
     *
     * @return mixed
     */
    public function getAll($oficinaId)
    {
        return TV::with('immoble', 'immoble.imatges')->whereOficinaId($oficinaId)->orderBy('order')->get();
    }

    /**
     * Obté el contingut de la televisió de l'usuari $userId
     *
     * @return mixed
     */
    public function get($id)
    {
        return TV::with('immoble')->find($id);
    }

    public function store($oficinaId, $immobleId)
    {
        $nextOrder = TV::whereOficinaId($oficinaId)->orderBy('order', 'DESC')->first();
        if (! $nextOrder) $nextOrder = 0;
        else $nextOrder = $nextOrder->order + 1;
        $tv = TV::create([
            'oficina_id' => $oficinaId,
            'immoble_id' => $immobleId,
            'order' => $nextOrder
        ]);
        return $tv->id;
    }

    public function destroy($id, $oficinaId)
    {
        $tv = TV::find($id);
        $tvs = TV::where('order', '>', $tv->order)->whereOficinaId($oficinaId)->get();
        foreach($tvs as $tv)
        {
            $tv->decrement('order');
        }
        
        TV::destroy($id);
    }

    public function setOrderUp($id, $order, $oficina)
    {
        TV::whereOrder($order)->whereOficinaId($oficina)->increment('order');
        TV::whereId($id)->whereOficinaId($oficina)->decrement('order');        
    }

    public function setOrderDown($id, $order)
    {
        TV::whereOrder($order)->whereOficinaId($oficina)->decrement('order');
        TV::whereId($id)->whereOficinaId($oficina)->increment('order');        
    }

    public function setFotos($id, $num)
    {
        $tv = TV::find($id);
        $tv->num_fotos = $num;
        $tv->save();
    }
} 