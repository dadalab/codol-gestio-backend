<?php namespace CodolGestio\Repositories;

use CodolGestio\RegistreClaus;

class RegistreClausRepository {


    /**
     * @return mixed
     */
    public function getAll()
    {
        return RegistreClaus::with('immoble')->get();
    }
} 