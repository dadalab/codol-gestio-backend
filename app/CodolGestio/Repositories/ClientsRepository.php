<?php namespace CodolGestio\Repositories;

use CodolGestio\Client;
use CodolGestio\ClientDocument;

class ClientsRepository {

    /**
     * Obté tots els clients del sistem
     *
     * @return mixed
     */
    public function getAllOnOffice($id)
    {
        return Client::whereOficinaId($id)->get();
    }


    /**
     * Obté el client del sistema per id.
     *
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return Client::find($id);
    }

    /**
     * Guarda el client a la base de dades.
     *
     * @param $input
     * @return Client
     */
    public function store($input)
    {
        $client = new Client();
        $client->fill($input)->save();

        return $client;
    }

    /**
     * Actualitza un client.
     *
     * @param $input
     * @return Client
     */
    public function update($id, $input)
    {
        $client = Client::find($id);
        $client->fill($input)->save();

        return $client;
    }

    public function destroy($id, $motiu)
    {
        $client = Client::find($id);
        $client->motiu_arxivar = $motiu;
        $client->save();
        $client->delete();
    }

    public function getArxivats($oficina_id)
    {
        return Client::onlyTrashed()->where('oficina_id', $oficina_id)->get();
    }

    public function recuperar($id)
    {
        Client::withTrashed()->where('id', $id)->restore();
    }

    public function eliminar($id)
    {
        Client::withTrashed()->where('id', $id)->forceDelete();
    }

    /**
     * Enllaça un document a l'immoble
     *
     * @param $id
     * @param $filename
     */
    public function attachFile($id, $filename)
    {
        $document = new ClientDocument;
        $document->nom = $filename;

        $client = Client::find($id);

        $client->documents()->save($document);
    }

    public function removeDocument($id, $fitxer_id)
    {
        $fitxer = ClientDocument::find($fitxer_id);

        if (\File::exists(public_path() . '/documents/clients/' . $fitxer->nom)) {
            \File::delete(public_path() . '/documents/clients/' . $fitxer->nom);
        }

        ClientDocument::destroy($fitxer_id);
    }

    public function follow($client_id, $input)
    {
        $client = Client::find($client_id);
        $client->follows()->attach($input['immoble_id']);
    }
}
