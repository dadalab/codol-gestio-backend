<?php namespace CodolGestio\Repositories;

use CodolGestio\Regim;

class RegimsRepository {


    /**
     * Retorna els règims de l'immoble
     *
     * @return mixed
     */
    public function getAll()
    {
        return Regim::limit(2)->get();
    }
} 