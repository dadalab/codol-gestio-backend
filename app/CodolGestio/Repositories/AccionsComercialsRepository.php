<?php namespace CodolGestio\Repositories;

use CodolGestio\AccioComercial;

class AccionsComercialsRepository {

    /**
     * Guarda una visita al sistema.
     *
     * @param $id
     * @param $input
     * @return Visita
     */
    public function store($immoble_id, $input)
    {
        $accioComercial = new AccioComercial;
        $accioComercial->data = \Carbon::createFromFormat("d-m-Y", $input['data'])->format("Y-m-d");
        $accioComercial->immoble_id = $immoble_id;
        $accioComercial->observacions = $input['observacions'];
        $accioComercial->save();

        return $accioComercial;
    }

    public function update($id, $input)
    {
        $accio = AccioComercial::find($id);
        $accio->observacions = $input['observacions'];
        $accio->save();
    }

    public function destroy($id)
    {
        AccioComercial::destroy($id);
    }
} 