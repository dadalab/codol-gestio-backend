<?php namespace CodolGestio\Repositories;

use CodolGestio\Tipus;

class TipusRepository {

    /**
     * Obté els tipus del sistema
     *
     * @return mixed
     */
    public function getAll()
    {
        return Tipus::all();
    }
} 