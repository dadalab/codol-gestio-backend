<?php namespace CodolGestio\Repositories;

use CodolGestio\Immoble;

class ValoracionsRepository {

    public function getAllByOffice($oficinaId)
    {
        return Immoble::where('es_valoracio', '=', '1')->whereOficinaId($oficinaId)->with('propietari')->get();
    }

    /**
     * Obté un immoble per id, amb el seu propietari, imatge, galeria i visites.
     *
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return Immoble::withTrashed()->with(['propietari', 'imatgeDestacada', 'imatges', 'visites', 'accionsComercials'])->get()->find($id);
    }

    public function convertir($id)
    {
        $immoble = Immoble::find($id);
        $immoble->num_expedient = \Auth::user()->oficina->expedient_prefix . str_pad(\Auth::user()->oficina->seguent_num_expedient, '5', '0',  STR_PAD_LEFT);
        $immoble->es_valoracio = 0;
        $immoble->save();
    }

    public function destroy($id)
    {
        Immoble::destroy($id);
    }

    
} 