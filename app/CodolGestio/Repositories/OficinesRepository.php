<?php namespace CodolGestio\Repositories;

use CodolGestio\Oficina;
use CodolGestio\Immoble;

class OficinesRepository {


    /**
     * Obté les oficines de l'immoble.
     *
     * @return mixed
     */
    public function getAll()
    {
        return Oficina::with('ciutat')->get();
    }

    public function augmentarExpedient($id)
    {
        Oficina::find($id)->increment('seguent_num_expedient');
    }

    public function getDestacat($id)
    {
        $oficina = Oficina::find($id);

        return Immoble::find($oficina->immoble_destacat);
    }

    public function canviarDestacat($oficina_id, $immoble)
    {
        if ($immoble) {
            $oficina = Oficina::find($oficina_id);
            $oficina->immoble_destacat = $immoble;
            $oficina->save();
        }
    }
} 