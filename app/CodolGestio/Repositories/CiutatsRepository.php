<?php namespace CodolGestio\Repositories;

use CodolGestio\Ciutat;

class CiutatsRepository {

    /**
     * Obte les ciutats del sistema.
     *
     * @return mixed
     */
    public function getAll()
    {
        return Ciutat::orderBy('nom');
    }

    public function store($nom)
    {
        return Ciutat::create(['nom' => $nom]);

    }
} 