<?php namespace CodolGestio\Repositories;

use CodolGestio\Propietari;

class PropietarisRepository {

    /**
     * Obté tots els propietaris
     *
     * @return mixed
     */
    public function getAll()
    {
        return Propietari::all();
    }

    /**
     * Obté tots els propietaris de la oficina
     *
     * @return mixed
     */
    public function getAllOnOffice($id)
    {
        return Propietari::whereOficinaId($id)->get();
    }


    /**
     * Obté un propietari amb els seus immobles.
     *
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return Propietari::with('immobles')->find($id);
    }


    /**
     * Guarda un propietari al sistema.
     *
     * @param $input
     * @return Propietari
     */
    public function store($input)
    {
        $propietari = new Propietari;
        $propietari->fill($input)->save();

        return $propietari;
    }


    /**
     * Actualitza un propietari del sistema.
     *
     * @param $id
     * @param $input
     * @return mixed
     */
    public function update($id, $input)
    {
        $propietari = Propietari::find($id)->update($input);

        return $propietari;
    }

    public function destroy($id, $motiu)
    {
        $propietari = Propietari::find($id);
        $propietari->motiu_arxivar = $motiu;
        $propietari->save();
        $propietari->delete();
    }
} 