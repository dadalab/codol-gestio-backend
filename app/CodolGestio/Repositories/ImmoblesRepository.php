<?php namespace CodolGestio\Repositories;


use CodolGestio\Immoble;
use CodolGestio\ImmoblesImatge;
use CodolGestio\Document;
use CodolGestio\SeguidorWeb;
use Illuminate\Support\Str;

class ImmoblesRepository {

    public function get($id)
    {
        return Immoble::find($id);
    }


    /**
     * Obté tots els immobles amb la seva imatge destacada.
     *
     * @return mixed
     */
    public function getAll()
    {
        return Immoble::with('imatgeDestacada')->with('oficina')->with('estat')->with('tipus')->with('regim')->with('ciutat')->get();
    }

    public function getByOffice($oficina_id)
    {
        return Immoble::with('propietari')->with('imatgeDestacada')->with('imatges')->with('visites')->whereOficinaId($oficina_id)->get();
    }

    public function getFotocasaByOffice($oficina_id)
    {
        return Immoble::with('propietari')->with('imatgeDestacada')->with('imatges')->with('visites')->whereOficinaId($oficina_id)->whereFotocasa(1)->get();
    }

    public function getReclamByOffice($oficina_id)
    {
        return Immoble::with('propietari')->with('imatgeDestacada')->with('imatges')->with('visites')->whereOficinaId($oficina_id)->whereReclam(1)->get();
    }

    public function getHabitacliaByOffice($oficina_id)
    {
        return Immoble::with('propietari')->with('imatgeDestacada')->with('imatges')->with('visites')->whereOficinaId($oficina_id)->whereHabitaclia(1)->get();
    }



    /**
     * Obté un immoble per id, amb el seu propietari, imatge, galeria i visites.
     *
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return Immoble::withTrashed()->with(['propietari', 'imatgeDestacada', 'imatges', 'visites', 'accionsComercials'])->get()->find($id);
        //return Immoble::withTrashed()->with('propietari')->with('imatgeDestacada')->with('imatges')->with('visites')->get()->find($id);
    }

    public function getNextByOffice($id, $oficina_id, $seg)
    {
        if ($seg == 1)
            return Immoble::with('propietari')->with('imatgeDestacada')->with('imatges')->with('visites')->whereOficinaId($oficina_id)->where('id', '>', $id)->get()->first();
        return Immoble::with('propietari')->with('imatgeDestacada')->with('imatges')->with('visites')->whereOficinaId($oficina_id)->where('id', '<', $id)->get()->last();

    }

    /**
     * Guarda un immoble al sistema.
     *
     * @param $input
     * @return Immoble
     */
    public function store($input)
    {
        $immoble = new Immoble;

        $immoble->fill($input)->save();

        $url = $immoble->tipus->nom . '-' . $immoble->regim->nom . '-' . $immoble->ciutat->nom . '-' . $immoble->id;
        $immoble->url = $this->cleanString($url);
        $immoble->save();

        if (isset($input['tags']))
            $immoble->tags()->sync($input['tags']);

        return $immoble;
    }


    /**
     * Actualitza un immoble del sistema
     *
     * @param $id
     * @param $input
     * @return mixed
     */
    public function update($id, $input)
    {
        $immoble = Immoble::find($id);
        $immoble->fill($input)->save();

        $url = $immoble->tipus->nom . '-' . $immoble->regim->nom . '-' . $immoble->ciutat->nom . '-' . $immoble->id;
        $immoble->url = $this->cleanString($url);
        $immoble->save();

        if (isset($input['tags']))
        $immoble->tags()->sync($input['tags']);

        return $immoble;
    }


    /**
     * Obté les imatges d'un immoble.
     *
     * @param $id
     * @return mixed
     */
    public function getImages($id)
    {
        return Immoble::find($id)->imatges()->get();
    }


    /**
     * Enllaça una nova imatge amb l'immoble.
     *
     * @param $id
     * @param $filename
     */
    public function attachImage($id, $filename)
    {
        $immoblesImatge = new ImmoblesImatge;
        $immoblesImatge->nom = $filename;

        $immoble = Immoble::find($id);

        $immoble->imatges()->save($immoblesImatge);
    }


    /**
     * Marca com a destacada la imatge $imatgeId de l'immoble
     *
     * @param $id
     * @param $imatgeId
     */
    public function changeFeaturedImage($id, $imatgeId)
    {
        $immoble = Immoble::find($id);
        $immoble->immobles_imatges_id = $imatgeId;
        $immoble->save();
    }


    /**
     * Elimina la imatge de l'immoble.
     *
     * @param $id
     * @param $imatgeId
     */
    public function removeImage($id, $imatgeId)
    {
        $image = ImmoblesImatge::find($imatgeId);

        if (\File::exists('/var/www/vhosts/codolgestio.net/httpdocs/content/' . $image->nom)) {
            \File::delete('/var/www/vhosts/codolgestio.net/httpdocs/content/' . $image->nom);
        }

        ImmoblesImatge::destroy($imatgeId);
    }


    /**
     * Arxiva l'immoble.
     *
     * @param $id
     * @param $motiu
     */
    public function destroy($id, $motiu)
    {
        $immoble = Immoble::find($id);
        $immoble->motiu_arxivar = $motiu;
        $immoble->save();
        $immoble->delete();
    }


    /**
     * Reordena les imatges de l'immoble.
     *
     * @param $images
     */
    public function orderImages($images)
    {
        $i = 1;
        foreach ($images as $image)
        {
            $img = ImmoblesImatge::find($image);
            $img->ordre = $i;
            $img->save();
            $i++;
        }
    }


    /**
     * Actualitza paràmetres booleans de l'immoble (per ajax)
     *
     * @param $id
     * @param $data_name
     * @param $data_value
     */
    public function updateRadios($id, $data_name, $data_value)
    {
        Immoble::where('id', $id)->update(array($data_name => !$data_value));
    }

    /**
     * Actualitza paràmetres pel tipus ID del reclam
     *
     * @param $id
     * @param $data_name
     * @param $data_value
     */
    public function updateTipusReclam($id, $data_name, $data_value)
    {
        Immoble::where('id', $id)->update(array($data_name => $data_value));
    }

    /**
     * Enllaça un document a l'immoble
     *
     * @param $id
     * @param $filename
     */
    public function attachFile($id, $filename)
    {
        $document = new Document;
        $document->nom = $filename;

        $immoble = Immoble::find($id);

        $immoble->documents()->save($document);
    }

    public function removeDocument($id, $fitxer_id)
    {
        $fitxer = Document::find($fitxer_id);

        if (\File::exists(public_path() . '/documents/immobles/' . $fitxer->nom)) {
            \File::delete(public_path() . '/documents/immobles/' . $fitxer->nom);
        }

        Document::destroy($fitxer_id);
    }

    public function visibilitatImatge($id, $imatge_id, $visibilitat)
    {
        $imatge = ImmoblesImatge::find($imatge_id);
        $imatge->es_visible = !$visibilitat;
        $imatge->save();
    }

    public function getArxivats($oficina_id)
    {
        return Immoble::onlyTrashed()->where('oficina_id', $oficina_id)->get();
    }

    public function recuperar($id)
    {
        Immoble::withTrashed()->where('id', $id)->restore();
    }

    public function eliminar($id)
    {
        Immoble::withTrashed()->where('id', $id)->forceDelete();
    }

    public function afegirSeguidorWeb($id, $email)
    {
        SeguidorWeb::firstOrCreate(['immoble_id' => $id, 'email' => $email]);
    }

    private function cleanString($text)
    {
        $text = strtolower($text);
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'A',
            '/[ÍÌÎÏ]/u' => 'I',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'O',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'U',
            '/ç/' => 'c',
            '/Ç/' => 'C',
            '/ñ/' => 'n',
            '/Ñ/' => 'N',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => '', // Literally a single quote
            '/[“”«»„]/u' => '', // Double quote
            '/ /' => '-', // nonbreaking space (equiv. to 0x160)
        );
        $text = preg_replace(array_keys($utf8), array_values($utf8), $text);
        $text = str_replace(' ', '-', $text);
        $text = str_replace('/', '-', $text);
        $text = Str::slug($text);

        return $text;
    }

    public function eliminarClientInteressat($immoble, $client)
    {
        $immoble = Immoble::find($immoble);
        $immoble->clientsInteressats()->detach($client);
    }
} 