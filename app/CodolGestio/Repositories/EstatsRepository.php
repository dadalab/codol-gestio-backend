<?php namespace CodolGestio\Repositories;

use CodolGestio\Estat;

class EstatsRepository {

    /**
     * Obté tots els estats del sistema.
     *
     * @return mixed
     */
    public function getAll()
    {
        return Estat::all();
    }
} 