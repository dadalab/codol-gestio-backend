<?php namespace CodolGestio\Repositories;

use CodolGestio\Tag;

class TagsRepository {

    public function getAll()
    {
        return Tag::all();
    }
}