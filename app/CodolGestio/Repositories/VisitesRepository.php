<?php namespace CodolGestio\Repositories;

use CodolGestio\Visita;

class VisitesRepository {

    /**
     * Guarda una visita al sistema.
     *
     * @param $id
     * @param $input
     * @return Visita
     */
    public function store($id, $input)
    {

        $visita = new Visita;
        $visita->data = \Carbon::createFromFormat("d-m-Y", $input['data'])->format("Y-m-d");
        $visita->client_id = $id;
        $visita->immoble_id = $input['immoble_id'];
        $visita->observacions = $input['observacions'];
        $visita->save();

        return $visita;
    }

    /**
     * Obté les visites per client.
     *
     * @param $id
     * @return mixed
     */
    public function getByClient($id)
    {
        return Visita::whereClientId($id)->orderBy('data', 'DESC')->get();
    }

    public function getByOffice($id)
    {
        $visites = Visita::with('client')->leftJoin('immobles', 'immobles.id', '=', 'visites.immoble_id')->where('oficina_id', $id)->get();
        return $visites;
    }

    public function update($id, $observacions)
    {
        $visita = Visita::find($id);
        $visita->observacions = $observacions;
        $visita->save();
    }

    public function destroy($id)
    {
        Visita::destroy($id);
    }
} 