<?php namespace CodolGestio;


class AccioComercial extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accions_comercials';

    protected $dates = array('data');

    protected $guarded = [];

    public function immoble()
    {
        return $this->belongsTo('CodolGestio\Immoble')->withTrashed();
    }

} 