<?php namespace CodolGestio;

class Immoble extends \Eloquent {

    use \SoftDeletingTrait;

    protected $dates = ['deleted_at'];

    protected $table = 'immobles';

    protected $fillable = ['oficina_id', 'propietari_id', 'num_expedient', 'titol', 'descripcio', 'tipus_id', 'regim_id',
        'estat_id', 'ciutat_id', 'preu', 'preu_lloguer', 'adreca_immoble', 'adreca_publica', 'superficie', 'dormitoris', 'banys', 'certificat_eficiencia', 'financament', 'oportunitat',
        'destacat', 'novetat', 'fet_a_mida', 'cedula_habitabilitat', 'hipoteca', 'llum', 'aigua', 'gas', 'retol_penjat', 'immobles_imatges_id', 'publicat', 'claus', 'coordx', 'coordy',
        'tipus_via', 'publicitat', 'ref_cadastral', 'ref_externa', 'inscripcio_registre', 'anys', 'interes', 'quota', 'motiu_arxivar', 'fotocasa', 'video_youtube',
        'es_valoracio', 'valoracio_client', 'valoracio_codol', 'valoracio_hipoteca', 'valoracio_entitat', 'valoracio_comentaris', 'valoracio_data', 'tipus_reclam', 'text_destacat' ];


    public function getRetolPenjatAttribute($value)
    {
        if ($value == "0000-00-00")
            return 'No';
        return \Carbon::createFromFormat("Y-m-d", $value)->format("d-m-Y");
    }

    public function setRetolPenjatAttribute($value)
    {
        if ($value == 'No' || $value == 'no' || $value == 'NO' || $value == '')
            $this->attributes['retol_penjat'] = '0000-00-00';
        else
            $this->attributes['retol_penjat'] = \Carbon::createFromFormat("d-m-Y", $value)->format("Y-m-d");
    }

    public function getPreuAttribute($value)
    {
        return str_replace('.', ',', $value);
    }

    public function setPreuAttribute($value)
    {
        $this->attributes['preu'] = str_replace(',', '.', $value);
    }

    public function getPreuLloguerAttribute($value)
    {
        return str_replace('.', ',', $value);
    }

    public function setPreuLloguerAttribute($value)
    {
        $this->attributes['preu_lloguer'] = str_replace(',', '.', $value);
    }

    public function getNumberFormat($value = 'preu')
    {
        return number_format($this->attributes[$value],0,',','.');
    }

    public function oficina()
    {
        return $this->belongsTo('CodolGestio\Oficina');
    }

    public function ciutat()
    {
        return $this->belongsTo('CodolGestio\Ciutat');
    }

    public function propietari()
    {
        return $this->belongsTo('CodolGestio\Propietari');
    }

    public function visites()
    {
        return $this->hasMany('CodolGestio\Visita');
    }

    public function accionsComercials()
    {
        return $this->hasMany('CodolGestio\AccioComercial');
    }

    public function tipus()
    {
        return $this->belongsTo('CodolGestio\Tipus');
    }

    public function regim()
    {
        return $this->belongsTo('CodolGestio\Regim');
    }

    public function estat()
    {
        return $this->belongsTo('CodolGestio\Estat');
    }

    public function imatges()
    {
        return $this->hasMany('CodolGestio\ImmoblesImatge')->orderBy('ordre')->orderBy('id');
    }

    public function documents()
    {
        return $this->hasMany('CodolGestio\Document');
    }

    public function imatgeDestacada()
    {
        return $this->belongsTo('CodolGestio\ImmoblesImatge', 'immobles_imatges_id');
    }

    public function tags()
    {
        return $this->belongsToMany('CodolGestio\Tag');
    }

    public function clientsInteressats()
    {
        return $this->belongsToMany('CodolGestio\Client');
    }

    public function getBuscadorSelectAttribute()
    {
        return $this->attributes['num_expedient'] . ' | ' . $this->attributes['adreca_immoble'];
    }

    public function seguidorsWeb()
    {
        return $this->belongsTo('CodolGestio\SeguidorWeb');
    }

} 