<?php namespace CodolGestio;


class Propietari extends \Eloquent {

    use \SoftDeletingTrait;

    protected $guarded = array('id');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'propietaris';

    public function immobles()
    {
        return $this->hasMany('CodolGestio\Immoble');
    }

    public function getFullNameAttribute()
    {
        return $this->attributes['nom'] . ' ' . $this->attributes['cognoms'];
    }
} 