<?php namespace CodolGestio;

class TV extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tvs';

    protected $guarded = ['id'];

    public function immoble()
    {
    	return $this->belongsTo('CodolGestio\Immoble');
    }
}
