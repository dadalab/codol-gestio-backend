<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/generateURL0987', 'GeneralFunctionsController@generateURL');
Route::get('/generateCoords0987', 'GeneralFunctionsController@generateCoords');

Route::get('/pantalla/{oficina}', array('as' => 'pantalla', 'uses' => 'TVController@show'));

Route::group(array('before' => 'guest'), function () {
    Route::get('/login', 'SessionsController@create');
    Route::get('/export/yaencontre', 'ExportController@yaencontre');
    Route::get('/export/fotocasa', 'ExportController@fotocasa');
    Route::get('/export/fotocasa/{id_oficina}', 'ExportController@fotocasa');
    Route::get('/export/reclam/{id_oficina}', 'ExportController@reclam');
    Route::get('/export/habitaclia/{id_oficina}', 'ExportController@habitaclia');
    Route::post('/login', 'SessionsController@store');

});

Route::group(array('before' => 'auth'), function () {
    Route::get('/sortir', 'SessionsController@destroy');
    Route::get('/canviar-contrasenya', array('as' => 'contrasenya', 'uses' => 'UsersController@edit'));
    Route::post('/canviar-contrasenya', array('as' => 'contrasenya', 'uses' => 'UsersController@update'));

    Route::get('/', ['as' => 'home', 'uses' => 'DashboardController@index']);
    Route::get('/dashboard', 'DashboardController@index');

    /* RUTES DELS IMMOBLES */
    Route::get('/immobles', array('as' => 'immobles', 'uses' => 'ImmoblesController@index'));
    Route::get('/immobles-2', array('as' => 'immobles.quadricula', 'uses' => 'ImmoblesController@indexQuadricula'));
    Route::get('/immobles/triar-destacat', array('as' => 'immoble.triar_destacat', 'uses' => 'ImmoblesController@triarDestacat'));
    Route::post('/immobles/triar-destacat', array('as' => 'immoble.triar_destacat', 'uses' => 'ImmoblesController@canviarDestacat'));
    Route::get('/immobles/arxivats', array('as' => 'immobles.arxivats', 'uses' => 'ImmoblesController@arxivats'));
    Route::get('/immobles/{id}/recuperar', array('as' => 'immobles.recuperar', 'uses' => 'ImmoblesController@recuperar'));
    Route::get('/immobles/{id}/eliminar_definitivament', array('as' => 'immobles.eliminar_definitivament', 'uses' => 'ImmoblesController@eliminar'));
    Route::get('/immobles/{id}/visualitzar/{seg?}', array('as' => 'immoble.visualitzar', 'uses' => 'ImmoblesController@show'));
    Route::get('/immobles/{id}/imprimir', array('as' => 'immoble.imprimir', 'uses' => 'ImmoblesController@imprimir'));
    Route::get('/immobles/afegir', array('as' => 'immoble.afegir', 'uses' => 'ImmoblesController@create'));
    Route::post('/immobles/afegir', array('as' => 'immoble.guardar', 'uses' => 'ImmoblesController@store'));
    Route::get('/immobles/{id}/pujar-imatges', array('as' => 'immoble.pujar-imatges', 'uses' => 'ImmoblesController@createImages'));
    Route::post('/immobles/{id}/pujar-imatges', array('as' => 'immoble.pujar-imatges', 'uses' => 'ImmoblesController@uploadImages'));
    Route::get('/immobles/{id}/pujar-documents', array('as' => 'immoble.pujar-documents', 'uses' => 'ImmoblesController@createDocuments'));
    Route::post('/immobles/{id}/pujar-documents', array('as' => 'immoble.pujar-documents', 'uses' => 'ImmoblesController@uploadDocuments'));
    Route::post('/immobles/{id}/eliminar-document', array('as' => 'immoble.eliminar-document', 'uses' => 'ImmoblesController@eliminarDocument'));
    Route::get('/immobles/{id}/modificar', array('as' => 'immoble.modificar', 'uses' => 'ImmoblesController@edit'));
    Route::post('/immobles/{id}/modificar', array('as' => 'immoble.actualitzar', 'uses' => 'ImmoblesController@update'));
    Route::post('/immobles/{id}/actualitzar-destacada', array('as' => 'immoble.actualitzar-destacada', 'uses' => 'ImmoblesController@changeFeaturedImage'));
    Route::post('/immobles/{id}/eliminar-imatge', array('as' => 'immoble.eliminarImatge', 'uses' => 'ImmoblesController@removeImage'));
    Route::post('/immobles/{id}/eliminar', array('as' => 'immoble.eliminar', 'uses' => 'ImmoblesController@destroy'));
    Route::post('/immobles/{id}/ordenar_imatges', array('as' => 'immoble.ordenar_imatges', 'uses' => 'ImmoblesController@ordenarImatges'));
    Route::post('/immobles/{id}/modificar-radios', array('as' => 'immoble.modificar_radios', 'uses' => 'ImmoblesController@updateRadios'));
    Route::post('/immobles/{id}/modificar-tipus-reclam', array('as' => 'immoble.modificar_radios', 'uses' => 'ImmoblesController@updateTipusReclam'));


    Route::post('/immobles/{id}/visibilitat-imatge', array('as' => 'immoble.visibilitatimatge', 'uses' => 'ImmoblesController@visibilitatImatge'));

    Route::post('/immobles/segueix/{id}', array('as' => 'immoble.segueix_web', 'uses' => 'ImmoblesController@segueixWeb'));

    Route::post('/immobles/{id}/accions_comercials', array('as' => 'accio.afegir', 'uses' => 'AccionsComercialsController@store'));
    Route::post('/immobles/{id}/accions_comercials', array('as' => 'accio.afegir', 'uses' => 'AccionsComercialsController@store'));
    Route::post('/acciocomercial/{id}', array('as' => 'accio.update', 'uses' => 'AccionsComercialsController@update'));
    Route::post('/immobles/{id}/enviar-email', array('as' => 'immoble.enviaremail', 'uses' => 'ImmoblesController@enviarEmail'));

    Route::get('/acciocomercial/{id}/eliminar', array('as' => 'accio.destroy', 'uses' => 'AccionsComercialsController@destroy'));
    
    /* RUTES PELS PROPIETARIS */
    Route::get('/propietaris', array('as' => 'propietaris', 'uses' => 'PropietarisController@index'));
    Route::get('/propietaris/{id}/visualitzar', array('as' => 'propietari.visualitzar', 'uses' => 'PropietarisController@show'));
    Route::get('/propietaris/afegir', array('as' => 'propietari.afegir', 'uses' => 'PropietarisController@create'));
    Route::post('/propietaris/afegir', array('as' => 'propietari.guardar', 'uses' => 'PropietarisController@store'));
    Route::get('/propietaris/{id}/modificar', array('as' => 'propietari.modificar', 'uses' => 'PropietarisController@edit'));
    Route::post('/propietaris/{id}/actualitzar', array('as' => 'propietari.actualitzar', 'uses' => 'PropietarisController@update'));
    Route::post('/propietaris/{id}/eliminar', array('as' => 'propietari.eliminar', 'uses' => 'PropietarisController@destroy'));

    /* RUTES PELS CLIENTS */
    Route::get('/clients', array('as' => 'clients', 'uses' => 'ClientsController@index'));
    Route::get('/clients/arxivats', array('as' => 'clients.arxivats', 'uses' => 'ClientsController@arxivats'));
    Route::get('/clients/{id}/visualitzar', array('as' => 'client.visualitzar', 'uses' => 'ClientsController@show'));
    Route::get('/clients/{id}/imprimir', array('as' => 'client.print', 'uses' => 'ClientsController@imprimir'));
    Route::get('/clients/afegir', array('as' => 'clients.afegir', 'uses' => 'ClientsController@create'));
    Route::post('/clients/afegir', array('as' => 'clients.guardar', 'uses' => 'ClientsController@store'));
    Route::get('/clients/{id}/modificar', array('as' => 'clients.modificar', 'uses' => 'ClientsController@edit'));
    Route::post('/clients/{id}/modificar', array('as' => 'clients.actualitzar', 'uses' => 'ClientsController@update'));
    Route::post('/clients/{id}/eliminar', array('as' => 'clients.eliminar', 'uses' => 'ClientsController@destroy'));
    Route::get('/clients/{id}/recuperar', array('as' => 'clients.recuperar', 'uses' => 'ClientsController@recuperar'));
    Route::get('/clients/{id}/eliminar_definitivament', array('as' => 'clients.eliminar_definitivament', 'uses' => 'ClientsController@eliminar'));
    Route::get('/clients/{id}/pujar-documents', array('as' => 'clients.pujar-documents', 'uses' => 'ClientsController@createDocuments'));
    Route::post('/clients/{id}/pujar-documents', array('as' => 'clients.pujar-documents', 'uses' => 'ClientsController@uploadDocuments'));
    Route::post('/clients/{id}/eliminar-document', array('as' => 'clients.eliminar-document', 'uses' => 'ClientsController@eliminarDocument'));

    Route::post('/clients/{id}/visita/afegir', array('as' => 'visita.afegir', 'uses' => 'VisitesController@store'));
    Route::post('/visita/modificar', array('as' => 'visita.modificar', 'uses' => 'VisitesController@update'));
    Route::get('/immobles/{id}/eliminar-interessat/{client}', array('as' => 'clientsinteressat.destroy', 'uses' => 'ImmoblesController@destroyClientInteressat'));


    Route::get('/visites', array('as' => 'visites', 'uses' => 'VisitesController@index'));
    Route::get('/visites/eliminar', array('as' => 'visites.eliminar', 'uses' => 'VisitesController@destroy'));
    Route::get('/visites/{id}/eliminar', 'VisitesController@destroy');

    /* RUTES DEL REGISTRE DE CLAUS */
    Route::get('/registre_claus', array('as' => 'registre_claus', 'uses' => 'RegistreClausController@index'));
    Route::get('/registre_claus/afegir', array('as' => 'registre_claus.afegir', 'uses' => 'RegistreClausController@create'));

    /* RUTES DELS PORTALS */
    Route::get('/portals', array('as' => 'portals', 'uses' => 'ExportController@index'));
    
    Route::get('/enllacos', array('as' => 'enllacos', 'uses' => 'LinksController@index'));
    Route::post('/enllacos', array('as' => 'enllacos.post', 'uses' => 'LinksController@enviar'));


    Route::get('/exportar', array('as' => 'exportar', 'uses' => 'ExcelController@index'));
    Route::post('/exportar', array('as' => 'exportar', 'uses' => 'ExcelController@download'));

    /* MÒDUL VALORACIONS */
    Route::get('/valoracions', array('as' => 'valoracions', 'uses' => 'ValoracionsController@index'));
    Route::get('/valoracions/create', array('as' => 'valoracions.create', 'uses' => 'ValoracionsController@create'));
    Route::post('/valoracions/afegir', array('as' => 'valoracio.afegir', 'uses' => 'ValoracionsController@store'));
    Route::get('/valoracions/{id}', array('as' => 'valoracions.visualitzar', 'uses' => 'ValoracionsController@show'));
    Route::get('/valoracions/{id}/modificar', array('as' => 'valoracio.modificar', 'uses' => 'ValoracionsController@edit'));
    Route::post('/valoracions/{id}/actualitzar', array('as' => 'valoracions.actualitzar', 'uses' => 'ValoracionsController@update'));
    Route::get('/valoracions/{id}/pujar-imatges', array('as' => 'valoracio.pujar-imatges', 'uses' => 'ValoracionsController@createImages'));
    Route::post('/valoracions/{id}/pujar-imatges', array('as' => 'valoracio.pujar-imatges', 'uses' => 'ValoracionsController@uploadImages'));
    Route::post('/valoracions/{id}/convertir', array('as' => 'valoracions.convertir', 'uses' => 'ValoracionsController@convertir'));
    Route::post('/valoracio/{id}/eliminar', array('as' => 'valoracio.eliminar', 'uses' => 'ValoracionsController@destroy'));


     /* MÒDUL TVS */
    Route::get('/tvs', array('as' => 'tvs', 'uses' => 'TVController@index'));
    Route::post('/tvs/immoble/{id}', array('as' => 'tvs.store', 'uses' => 'TVController@store'));
    Route::delete('/tvs/immoble/{id}', array('as' => 'tvs.destroy', 'uses' => 'TVController@destroy'));
    Route::put('/tvs/ordre/up/{id}/{ordre}', array('as' => 'tvs.order', 'uses' => 'TVController@orderUp'));
    Route::put('/tvs/ordre/down/{id}/{ordre}', array('as' => 'tvs.order', 'uses' => 'TVController@orderDown'));
    Route::put('/tvs/{id}/fotos/{numFotos}', array('as' => 'tvs.order', 'uses' => 'TVController@setNumFotos'));

    /* MÒDUL PDF'S */
    Route::get('/pdfs', array('as' => 'pdfs', 'uses' => 'PDFController@selectImmobles'));
    Route::get('/pdfs/generate', array('as' => 'pdfs.generate', 'uses' => 'PDFController@generate'));

    /* MÒDUL CARTELES */
    Route::get('/carteles', array('as' => 'carteles', 'uses' => 'CartelesController@selectImmobles'));
    Route::get('/carteles/{id}/generate', array('as' => 'carteles.generate', 'uses' => 'CartelesController@generate'));

});
