<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="col-md-12">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::to('/')}}"><img src="{{ url() }}/images/codol-gestio-logotip.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ URL::to('/') }}">Inici</a></li>

                    <li class="{{ Request::is('immobles*') ? 'active' : '' }}"><a href="{{ URL::route('immobles') }}">Immobles</a>
                    </li>
                    <li class="{{ Request::is('propietaris*') ? 'active' : '' }}"><a
                                href="{{ URL::route('propietaris') }}">Propietaris</a></li>
                    <li class="{{ Request::is('clients*') ? 'active' : '' }}"><a href="{{ URL::route('clients') }}">Clients</a>
                    </li>
                    <li class="{{ Request::is('registre_claus*') ? 'active' : '' }}"><a
                                href="{{ URL::route('registre_claus') }}">Registre de claus</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->username }} <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!--<li><a href="#">Preferències</a></li> -->
                            <li><a href="{{ URL::route('contrasenya') }}">Canviar contrasenya</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url() }}/sortir">Sortir</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>