 @if(Session::get('notification'))
    <div class="notification alert alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ Session::get('notification') }}
    </div>
@endif