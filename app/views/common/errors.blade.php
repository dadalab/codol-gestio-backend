@if ($errors->has())
    <div class="alert alert-warning">
        @foreach ($errors->all('<li>:message</li>') as $message)
            {{ $message }}
        @endforeach
    </div>
@endif

 @if(Session::get('missatge'))
    <div class="alert alert-success">
        {{ Session::get('missatge') }}
    </div>
@endif