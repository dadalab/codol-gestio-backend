@extends('layouts.layout')

@section('content')
	<style>
		#immobles_length, #immobles_paginate {
			display: none;
		}
	</style>

	<div id="generar-cartela">
	    <div class="page-header">
	        <h1>Generar Cartela</h1>
	    </div>
		
	    <div class="col-md-12">
	    	<table class="table" id="immobles">
		        <thead>
			        <th>Expedient</th>
			        <th>Adreça</th>
			        <th>Ciutat</th>
			        <th>Tipus</th>
			        <th>Règim</th>
			        <th></th>
		        </thead>
		        <tbody>
		        @foreach($immobles as $immoble)
		            <tr>
		                <td>{{ $immoble->num_expedient }}</td>
		                <td>{{ $immoble->adreca_immoble }}</td>
		                <td>{{ $immoble->ciutat->nom }}</td>
		                <td>{{ $immoble->tipus->nom }}</td>
		                <td>{{ $immoble->regim->nom }}</td>
		                <td><a href="{{ route('carteles.generate', $immoble->id) }}">Generar Cartela</td>
		            </tr>
		        @endforeach
		        </tbody>
	    	</table>
		</div>
	</div>


	<script>
		$.fn.dataTable.moment( 'DD-MM-YYYY' );
        var table = $('#immobles').DataTable({
            "iDisplayLength": 999
    	});
    </script>
@stop