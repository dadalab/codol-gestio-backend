<html>
	<head>
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700"><style type="text/css">.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px}</style><style type="text/css">@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style type="text/css">.gm-style{font-family:Roboto,Arial,sans-serif;font-size:11px;font-weight:400;text-decoration:none}.gm-style img{max-width:none}</style>
    <title>Impressió Cartela | Còdol Gestió</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" id="open-sans-css" href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=4.0" type="text/css" media="all">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/css/bootstrap.min.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/style.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/custom.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/print.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="{{ url() }}/assets/js/vue.min.js"></script>

    <style>
    	.imprimir_wrapper {
    		padding-bottom: 0;

    	}

		.titular {
			background-color: #d3460e !important;
			font-size: 28px;
			color: #fff;
			border-radius: 10px;
			padding: 15px;
			font-style: italic;
			font-weight: bold;
			font-family: 'PT-Serif';
		}

		.adreca {
			font-size: 28px;
			line-height: 35px;
			color: #000 !important;
			font-weight: 300;
		}

		.immoble-partial {
			margin-top: 15px;
			overflow: hidden;
			border-radius: 10px;
			padding: 0;
		}

		.img-responsive {
			margin: 0;
		}

		.preu {
			color: #d3460e !important;
			font-size: 39px;
			margin: 10px 0;
			font-weight: bold;
			line-height: 65px;
		}
		.quota {
			color: #d3460e !important;
			font-size: 25px;
			margin: 5px 0;
			display: block;

		}
		.topos {
			margin: 0;
		}

		.imatge-esquerra {
			border-right: 3px solid #d3460e;
			border-bottom: 6px solid #d3460e;
		}
		.imatge-dreta {
			border-left: 3px solid #d3460e;
			border-bottom: 6px solid #d3460e;
		}

		.etiqueta-immoble {	
			position: absolute;
		    z-index: 1;
		    background: #d3460e;
		    height: 35px;
		    line-height: 35px;    
		    color: #fff;
		    text-align: center;
			border-radius: 4px 4px 0 0;
			width: 100%;
			font-size: 18px;
			font-weight: bold;
		}

		.topo {
			width: 70px;
			height: 70px;
			font-size: 14px;
		}

		input, textarea {
			width: 100%;
		    font-size: 22px !important;
		    line-height: 25px !important;
		    font-weight: 100;
		}
		
		#photoSelector {
			width: 150px;
			position: fixed;
			left:0;
			top:0;
			overflow-y: scroll; 
			height: 100%;
		}
		#photoSelector img {
			border-bottom: 1px solid white;
		}
		img {
			cursor: pointer;
		}

		.anys {
			text-align: left;
			font-size: 18px;
			text-transform: uppercase;
			line-height: 22px;
			font-weight: bold;
			margin-left:40px;
			color: #fff;
			background-color: #000;
			display: inline-block;
			margin-bottom: 2px;
			padding: 2px 4px;
		}
		#descripcio {
			font-size: 32px;
			line-height: 38px;
			font-weight: 300;
			display: block;
		}
		.referencia {
			background-color: #f0f0f0;
			padding: 10px;
			margin-bottom: 20px;
		}
	    @page { 
	        size: landscape;
	    }
	    @page .container {
			max-width: 100%;
	    }

	    @media print {
	    	.imprimir_wrapper {
				max-width: 95%;
				width: 95%;
			}
		   	html, body {
       	 		margin:40px 0 0 0;
       	 		height: 96%;
    		}
		}
	    .row {
	    	margin: 0 -15px;
	    }
	    .imatge-2, .imatge-3 {
	    	height: 30%;
	    	background-size: cover;
	    	background-position: center;
	    }
	    .imatge-destacada {
			border-bottom: 6px solid #d3460e;
			background-size:cover;
			background-position: center;
			height: 50%;
		}
		.contingut-superior {
			height: 50%;
		}
	    .imatge-2 {
	    	border-right: 3px solid #d3460e;
	    }
	    .imatge-3 {
	    	border-left: 3px solid #d3460e;
	    }
	    .footer-preus {
	    	margin-top: 13%;
	    }
    </style>

<body style="background: white" id="print">

<div class="container content_wrapper imprimir_wrapper">
    <div class="row">
    	<a href="{{ route('carteles') }}" style="position:absolute;top:0;left:30px;" class="hidden-print">Tornar</a>
		<button style="position:absolute;top:0;right:30px;padding:10px" class="hidden-print" onclick="window.print()">Imprimir</button>
			
		<!-- Inici document -->
		<div class="col-xs-12 titular">
			<div id="documentTitle" v-on="click: changeTitle('documentTitle')" v-text="documentTitle" style="font-size:40px"></div>
			<input id="documentTitleInput" type="text" v-model="documentTitle" v-on="keyup:changeTitle('documentTitle') | key 'enter'" value="{{ $immoble->titol }}" style="display:none; width:100%">
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div class="contingut-superior">
		            <h1 class="adreca pull-left" id="title" v-on="click: changeTitle('title')" v-text="title"></h1>
		            <input type="text" id='titleInput' value="{{ $immoble->adreca_publica }} - {{ $immoble->ciutat->nom }}" v-model="title" v-on="keyup:changeTitle('title') | key 'enter'" style="display:none">
		        	<div class="clearfix"></div>
					<p class="referencia">REF. {{ $immoble->num_expedient }}</p>

		    		<span id="descripcio" v-text="descripcio" v-on="click: changeTitle('descripcio')"></span>
		        	<textarea id='descripcioInput' rows="7" v-model="descripcio" style="display:none">{{ nl2br($immoble->descripcio) }}</textarea>
					<button class="descripcioInput" v-on="click:changeTitle('descripcio')" style="display:none">OK</button>
				</div>
			
				<div class="footer-preus">
					<hr>
					<p style="font-size:16px" style="text-transform: uppercase"><b>QUALIFICACIÓ ENERGÈTICA:</b> {{ $immoble->certificat_eficiencia }}</p>
					<hr>
					<div class="preus pull-left" style="margin-top: 0">
						<span id="preu" class="preu" v-on="click: changeTitle('preu')" v-text="preu"></span>
			            <input type="text" id='preuInput' value="{{ $immoble->getNumberFormat('preu') }} €" v-model="preu" v-on="keyup:changeTitle('preu') | key 'enter'" style="display:none">
						<br>
						<span id="quota" class="quota" v-on="click: changeTitle('quota')" v-text="quota"></span>
			            <input type="text" id='quotaInput' value="@if($immoble->quota > 0) {{ $immoble->getNumberFormat('quota') }} €/mes @endif" v-model="quota" v-on="keyup:changeTitle('quota') | key 'enter'" style="display:none">
		            </div>
		            <div class="topos pull-right">  
		            	@if ($immoble->financament ) 
			     			<div class="topo financament" style="background: #424242">
			     				<div class="contingut"><b>100%</b> FINAN.</div>
			 				</div>      
		 				@endif
		 				@if ($immoble->estat_id == 1)
		 					<div class="topo obra-nova">
		 						<div class="contingut">Obra nova</div>
							</div> 
						@endif
					</div>
					<div class="clearfix"></div>
					<hr>
				</div>
			</div>
		
			<div class="col-xs-6">
				<div class="col-xs-12 immoble-partial">
					<div class="imatge-destacada" id="img1" v-on="click : setPosition(1)" style="background-image: url('http://codolgestio.net/content/{{ $immoble->imatgeDestacada->nom or $immoble->imatges->get(0)->nom }}.jpg');">
						@if ($immoble->novetat || $immoble->oportunitat)
							<div class="etiqueta-immoble">
								{{ $immoble->oportunitat ? 'Oh!portunitat' : ''}}
								{{ ($immoble->novetat && $immoble->oportunitat) ? '|' : '' }} 	
								{{ $immoble->novetat ? 'Novetat' : '' }}
							</div>
		            	@endif
	            	</div>
					<div class="col-xs-6" style="padding: 0">
						<div class="imatge-2" id="img2" v-on="click : setPosition(2)" style="background-image: url('http://codolgestio.net/content/{{ $immoble->imatges->get(0)->nom }}.jpg');"></div>
					</div>
					<div class="col-xs-6" style="padding:0">
						<div class="imatge-3" id="img3" v-on="click : setPosition(3)" style="background-image: url('http://codolgestio.net/content/{{ $immoble->imatges->get(1)->nom }}.jpg');"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<div id="photoSelector" class="hidden-print">
	@foreach($immoble->imatges as $imatge)
		<img src="http://codolgestio.net/content/{{ $imatge->nom }}.jpg" v-on="click : setImage('http://codolgestio.net/content/{{ $imatge->nom }}.jpg')" class="img-responsive">
	@endforeach
</div>

<script>
	
new Vue({
  el: '#print',
  	data: {
  		position: 0
  	},
  	methods: {
	  	changeTitle: function(nomCamp)
	  	{
	  		$('#' + nomCamp + 'Input').toggle().focus;
	  		$('.' + nomCamp + 'Input').toggle();
	  		$('#' + nomCamp).toggle();
	  	},
	  	setPosition: function(n)
		{
			this.position = n;
		},
		setImage:function(url)
		{
			$('#img' + this.position).css('background-image', 'url(' + url + ')')
		}
	}	
});

</script>

</body>
</html>