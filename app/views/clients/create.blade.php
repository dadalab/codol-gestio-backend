@extends('layouts.layout')

@section('content')

	{{ Form::open() }}
		<div class="page-header">
			<h1>Afegir nou client <small><button type="submit" class="btn btn-primary btn-lg pull-right">Guardar canvis</button></small></h1>
		</div>

		<div class="clearfix"></div>

		@include('common.errors')

        <div class="row">
		<div class="col-md-6">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="nom">Nom</label>
                    {{ Form::text('nom', null,  array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-md-6">
                    <label for="cognoms">Cognoms</label>
                    {{ Form::text('cognoms', null,  array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-md-6">
                    <label for="nom_altre">Nom 2</label>
                    {{ Form::text('nom_altre', null,  array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-md-6">
                    <label for="cognoms_altre">Cognoms 2</label>
                    {{ Form::text('cognoms_altre', null,  array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-md-12">
                    {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                    {{ Form::text('email', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('telefon_1', 'Telèfon 1', ['class' => 'control-label']) }}
                    {{ Form::text('telefon_1', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('telefon_2', 'Telèfon 2', ['class' => 'control-label']) }}
                    {{ Form::text('telefon_2', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('nif', 'NIF') }}
                    {{ Form::text('nif', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('dades_economiques', 'Dades econòmiques') }}
                    {{ Form::select('dades_economiques', withEmpty(array('Atur' => 'Atur', 'Autònom' => 'Autònom', 'Assalariat' => 'Assalariat', 'Jubilat' => 'Jubilat', 'Altres' => 'Altres')), null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('ciutat_id', 'Ciutat') }}
                    {{ Form::select('ciutat_id', withEmpty($ciutats), null, array('class' => 'form-control', 'data-live-search', 'true')) }}
                </div>


                <div class="form-group col-md-6">
                    {{ Form::label('ciutat_id', 'Ciutat') }}  <span class="label label-default pull-right afegir_ciutat"
                                                                    style="cursor: pointer"> + Afegir ciutat</span>
                    {{ Form::select('ciutat_id', withEmpty($ciutats), null, ['class' => 'form-control ciutat_id', 'data-live-search', 'true']) }}
                    {{ Form::text('ciutat_name', null, ['class' => 'form-control ciutat_name', 'style' => 'display:none']) }}
                </div>


                <div class="form-group col-md-6">
                    {{ Form::label('valoracio', 'Valoració del client') }}
                       <input type="number" name="valoracio" id="valoracio" class="rating" data-clearable="remove"/>
                </div>

                <div class="form-group col-md-12">
                    {{ Form::label('comentari', 'Comentari') }}
                    {{ Form::textarea('comentari', null, ['class' => 'form-control']) }}
                </div>
            </div>
		</div>

		<div class="col-md-6">
		    <div class="row">
                <div class="form-group col-md-12">
                    {{ Form::label('origen', 'Origen', ['class' => 'control-label']) }}
                    {{ Form::select('origen', withEmpty(
                        ['Fotocasa' => 'Fotocasa', 'Lead Anida' => 'Lead Anida', 'Lead Servihabitat' => 'Lead Servihabitat', 'Altres Portals' => 'Altres Portals', 'Oficina' => 'Oficina', 'Web' => 'Web', 'Telèfon' => 'Telèfon']
                    ), null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('comercial', 'Comercial', ['class' => 'control-label']) }}
                    {{ Form::select('comercial', ['Altres' => 'Altres', 'Josep' => 'Josep', 'Maria' => 'Maria', 'Jordi M.' => 'Jordi M.', 'Jordi C.' => 'Jordi C.', 'Carles' => 'Carles', 'Assumpta' => 'Assumpta'], null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('data_contacte', 'Data contacte', ['class' => 'control-label']) }}
                    {{ Form::text('data_contacte', \Carbon::now()->format('d-m-Y'), ['class' => 'form-control datepicker', "data-date-format" => "dd-mm-yyyy"]) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('poblacio_id', 'Població', ['class' => 'control-label']) }}
                    {{ Form::select('poblacio_id', withEmpty($ciutats), null, array('class' => 'form-control', 'data-live-search', 'true')) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('preu_maxim', 'Preu màxim', ['class' => 'control-label']) }}
                    {{ Form::text('preu_maxim', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('tipus_id', 'Tipus', ['class' => 'control-label']) }}
                    {{ Form::select('tipus_id', withEmpty($tipus), null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('regim_id', 'Règim', ['class' => 'control-label']) }}
                    {{ Form::select('regim_id', withEmpty($regims), null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('moblat', 'Moblat') }}
                    {{ Form::select('moblat', withEmpty(array('Sí' => 'Sí', 'No' => 'No')), null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-md-6">
                    {{ Form::label('habitacions', 'Habitacions', ['class' => 'control-label']) }}
                    {{ Form::number('habitacions', null, ['class' => 'form-control']) }}
                </div>
            </div>
		</div>

        </div>


	{{ Form::close() }}

@stop
