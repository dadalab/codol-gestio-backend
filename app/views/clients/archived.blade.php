@extends('layouts.layout')

@section('content')

    <table class="table" id="arxivats">
        <thead>
        <th>Nom</th>
        <th>Nom 2</th>
        <th>Telèfon</th>
        <th>Telèfon 2</th>
        <th>Motiu</th>
        <th></th>
        <th></th>
        </thead>
        <tbody>
        @foreach($clients as $client)
            <tr>
                <td>{{ $client->nom }} {{ $client->cognoms }}</td>
                <td>{{ $client->nom_altre }} {{ $client->cognoms_altre }}</td>
                <td>{{ $client->telefon_1 }}</td>
                <td>{{ $client->telefon_2 }}</td>
                <td>{{ $client->motiu_arxivar }}</td>
                <td><a href="{{ URL::route('clients.recuperar', $client->id) }}"><span
                                class="glyphicon glyphicon-upload"></span> Recuperar</a></td>
                <td><a href="{{ URL::route('clients.eliminar_definitivament', $client->id) }}"><span
                                class="glyphicon glyphicon-remove"></span> Eliminar definitivament</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <script>
        $(document).ready(function () {
            $('#arxivats').dataTable({
                "iDisplayLength": 50
            });
        });
    </script>

@stop

