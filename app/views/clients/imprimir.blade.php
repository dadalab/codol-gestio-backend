<html>
<head>

    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <style type="text/css">
        @media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style {
            font-family: Roboto, Arial, sans-serif;
            font-size: 11px;
            font-weight: 400;
            text-decoration: none
        }

        .gm-style img {
            max-width: none
        }</style>
    <title></title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" id="open-sans-css" href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=4.0" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="{{ url() }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/style.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/custom.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/print.css">
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/map.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/marker.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/onion.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/stats.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/controls.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://mt1.googleapis.com/vt?pb=!1m4!1m3!1i17!2i66356!3i48689!1m4!1m3!1i17!2i66357!3i48689!1m4!1m3!1i17!2i66356!3i48690!1m4!1m3!1i17!2i66357!3i48690!1m4!1m3!1i17!2i66358!3i48689!1m4!1m3!1i17!2i66358!3i48690!2m3!1e0!2sm!3i302170446!3m9!2sca-ES!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e3&amp;callback=_xdc_._r4n0l&amp;token=91869"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps/api/js/AuthenticationService.Authenticate?1shttp%3A%2F%2Fcodolgestio.net%2Fdetall%2Fpis-venda-vic-523%2F%3Fimprimir%3D1&amp;5e1&amp;callback=_xdc_._s8d9k8&amp;token=42708"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps/api/js/QuotaService.RecordEvent?1shttp%3A%2F%2Fcodolgestio.net%2Fdetall%2Fpis-venda-vic-523%2F%3Fimprimir%3D1&amp;4e1&amp;5e0&amp;6u1&amp;7s5wrdxt&amp;callback=_xdc_._6hh7kg&amp;token=66839"></script>
</head>
<body>

<div class="container content_wrapper imprimir_wrapper">
    <div class="row immoble">
        <div class="col-xs-12"><img src="http://codolgestio.net/wp-content/uploads/2014/09/codol-gestio-logotip.png"></div>

        <div class="col-xs-6" style="margin-top: 30px">
            <h2>Dades del client</h2>
            <table class="table">
            <tr>
                <td>Nom</td>
                <td>{{ $client->nom }}</td>
            </tr>
            <tr>
                <td>Cognoms</td>
                <td>{{ $client->cognoms }}</td>
            </tr>
            <tr>
                <td>Nom 2</td>
                <td>{{ $client->nom_altre }}</td>
            </tr>
            <tr>
                <td>Cognoms 2</td>
                <td>{{ $client->cognoms_altre }}</td>
            </tr>
            <tr>
                <td>NIF</td>
                <td>{{ $client->nif }}</td>
            </tr>
            <tr>
                <td>Ciutat</td>
                <td>{{ isset($client->poblacioClient->nom) ? $client->poblacioClient->nom : '' }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $client->email }}</td>
            </tr>
            <tr>
                <td>Telèfon 1</td>
                <td>{{ $client->telefon_1 }}</td>
            </tr>
            <tr>
                <td>Telèfon 2</td>
                <td>{{ $client->telefon_2 }}</td>
            </tr>
            <tr>
                <td>Valoració</td>
                <td>
                    @for ($i = 0; $i < $client->valoracio; $i++)
                        <span class="glyphicon glyphicon-star"></span>
                    @endfor
                    @for ($j = 0; $i < 5; $i++)
                        <span class="glyphicon glyphicon-star-empty"></span>
                    @endfor
                </td>
            </tr>
            <tr>
                <td>Comentari</td>
                <td>{{ $client->comentari }}</td>
            </tr>

            <tr>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td>Comercial</td>
                <td>{{ $client->comercial }}</td>
            </tr>
            <tr>
                <td>Data contacte</td>
                <td>{{ $client->data_contacte }}</td>
            </tr>
            <tr>
                <td>Dades econòmiques</td>
                <td>{{ $client->dades_economiques }}</td>
            </tr>
            <tr>
                <td>Preu màxim</td>
                <td>{{ $client->getNumberFormat('preu_maxim') }}€</td>
            </tr>
            <tr>
                <td>Població</td>
                <td>{{ isset($client->poblacioImmoble->nom) ? $client->poblacioImmoble->nom : 'No triat' }}</td>
            </tr>
            <tr>
                <td>Règim</td>
                <td>{{ isset($client->regim->nom) ? $client->regim->nom : 'No triat' }}</td>
            </tr>
            <tr>
                <td>Tipus</td>
                <td>{{ isset($client->tipus->nom) ? $client->tipus->nom : 'No triat' }}</td>
            </tr>
            <tr>
                <td>Moblat</td>
                <td>{{ $client->moblat }}</td>
            </tr>
            <tr>
                <td>Habitacions</td>
                <td>{{ $client->habitacions }}</td>
            </tr>
            </table>
        </div>
        <!-- end 6col container-->

        <div class="col-xs-6" style="margin-top: 30px">
            <h2>Visites del client</h2>
            @foreach($visites as $visita)
                <p>{{ $visita->immoble->adreca_immoble }}</p>
                <p>{{ $visita->data }}</p>
                <p>{{ $visita->observacions }}</p>
                <hr>
            @endforeach



        </div>
        <!-- end 6col container-->

    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/main.js"></script>


</body>
</html>