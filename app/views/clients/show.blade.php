@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Visualitzant client
            <small>
                <a href="{{ URL::route('clients')}}" style="margin-left: 10px" class="btn btn-default pull-right">Tornar al llistat</a>
                <button type="button" style="margin-left: 10px" class="btn btn-primary pull-right" data-toggle="modal" data-target="#visita">Afegir visita
                </button>
                <a href="{{ URL::route('clients.modificar', $client->id)}}" style="margin-left: 10px" class="btn btn-primary pull-right">Modificar client</a>
                <a href="{{ URL::route('client.print', $client->id) }}" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-print"></i> </a>
            </small>
        </h1>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Característiques del client</h4></div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>Nom</td>
                            <td>{{ $client->nom }}</td>
                        </tr>
                        <tr>
                            <td>Cognoms</td>
                            <td>{{ $client->cognoms }}</td>
                        </tr>
                        <tr>
                            <td>Nom 2</td>
                            <td>{{ $client->nom_altre }}</td>
                        </tr>
                        <tr>
                            <td>Cognoms 2</td>
                            <td>{{ $client->cognoms_altre }}</td>
                        </tr>
                        <tr>
                            <td>NIF</td>
                            <td>{{ $client->nif }}</td>
                        </tr>
                        <tr>
                            <td>Ciutat</td>
                            <td>{{ isset($client->poblacioClient->nom) ? $client->poblacioClient->nom : '' }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $client->email }}</td>
                        </tr>
                        <tr>
                            <td>Telèfon 1</td>
                            <td>{{ $client->telefon_1 }}</td>
                        </tr>
                        <tr>
                            <td>Telèfon 2</td>
                            <td>{{ $client->telefon_2 }}</td>
                        </tr>
                        <tr>
                            <td>Valoració</td>
                            <td>
                                @for ($i = 0; $i < $client->valoracio; $i++)
                                    <span class="glyphicon glyphicon-star"></span>
                                @endfor
                                @for ($j = 0; $i < 5; $i++)
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <td>Comentari</td>
                            <td>{{ $client->comentari }}</td>
                        </tr>

                        <tr>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>Comercial</td>
                            <td>{{ $client->comercial }}</td>
                        </tr>
                        <tr>
                            <td>Data contacte</td>
                            <td>{{ $client->data_contacte }}</td>
                        </tr>
                        <tr>
                            <td>Dades econòmiques</td>
                            <td>{{ $client->dades_economiques }}</td>
                        </tr>
                        <tr>
                            <td>Preu màxim</td>
                            <td>{{ $client->getNumberFormat('preu_maxim') }}€</td>
                        </tr>
                        <tr>
                            <td>Població</td>
                            <td>{{ isset($client->poblacioImmoble->nom) ? $client->poblacioImmoble->nom : 'No triat' }}</td>
                        </tr>
                        <tr>
                            <td>Règim</td>
                            <td>{{ isset($client->regim->nom) ? $client->regim->nom : 'No triat' }}</td>
                        </tr>
                        <tr>
                            <td>Tipus</td>
                            <td>{{ isset($client->tipus->nom) ? $client->tipus->nom : 'No triat' }}</td>
                        </tr>
                        <tr>
                            <td>Moblat</td>
                            <td>{{ $client->moblat }}</td>
                        </tr>
                        <tr>
                            <td>Habitacions</td>
                            <td>{{ $client->habitacions }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button type="button" class="btn btn-danger btn-sm pull-right" data-toggle="modal"
                                        data-target="#myModal">Arxivar client
                                </button>
                            </td>
                        </tr>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span
                                                    aria-hidden="true">&times;</span><span class="sr-only">Tancar</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Arxivar client</h4>
                                    </div>
                                    {{ Form::open(array('route' => array('clients.eliminar', $client->id))) }}
                                    <div class="modal-body">
                                        Estàs segur que vols arxivar aquest client?
                                        {{ Form::text('motiu_arxivar', null, ['class' => 'form-control', 'placeholder' => 'Motiu arxivar', 'style' => 'margin-top: 15px']) }}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar
                                        </button>
                                        <button type="submit" class="btn btn-danger">Arxivar</button>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                        </tr>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <ul class="list-group">
                @foreach($visites as $visita)
                    <li class="list-group-item" style="min-height: 105px">
                        <span class="badge">{{ $visita->data }}</span>
                        @if (isset($visita->immoble->id)) <a href="{{ URL::route('immoble.visualitzar', $visita->immoble->id) }}">{{ $visita->immoble->adreca_immoble }}</a>
                        @else No immoble @endif
                        <br>
                        <hr>
                        {{ $visita->observacions }}

                        @if ($client->oficina_id == Auth::user()->oficina_id)
                            <a href="#" class="modificar-visita pull-right" data-toggle="modal" data-id="{{ $visita->id }}" data-content="{{ $visita->observacions }}" data-target="#modificar-visita">modificar</a>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
            <h3>Documents associats</h3>

            <div class="row">
                @forelse($client->documents as $document)

                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="{{ url() }}/documents/clients/{{ $document->nom }}" target="_blank"><span class="glyphicon glyphicon-file"></span>{{ $document->nom }}</a></div>
                        </div>
                    </div>

                @empty
                    <div class="col-md-12"><p>Encara no hi ha cap document per aquest immoble!</p></div>
                @endforelse
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="visita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tancar</span></button>
                    <h4 class="modal-title" id="myModalLabel">Afegir visita</h4>
                </div>
                {{ Form::open(array('url' => URL::route('visita.afegir', $client->id) ) ) }}
                <div class="modal-body" style="overflow: hidden">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('data', 'Data') }}
                            {{ Form::text('data', \Carbon::now()->format("d-m-Y"), array('class' => 'form-control datepicker', "data-date-format" => "dd-mm-yyyy")) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('immoble_id', 'Immoble') }}
                            {{ Form::select('immoble_id', withEmpty($immobles), null, array('class' => 'form-control', 'data-live-search', 'true')) }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('observacions', 'Observacions') }}
                            {{ Form::textarea('observacions', null, array('class' => 'form-control')) }} <br>
                            <label style="font-weight:500">{{ Form::checkbox('notificacions') }} El client vol rebre notificacions?</label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                    <button type="submit" id="store" class="btn btn-primary">Guardar visita</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modificar-visita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Tancar</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modificar visita</h4>
                </div>
                {{ Form::open(array('url' => URL::route('visita.modificar', $client->id) ) ) }}
                <div class="modal-body" style="overflow: hidden">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('observacions', 'Observacions') }}
                            {{ Form::textarea('observacions', null, array('class' => 'form-control', 'id' => 'observacions-modificar')) }}
                            {{ Form::hidden('visita_id', null, ['id' => 'visita_id']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <label class="pull-left">{{ Form::checkbox('eliminar', '1',  null, ['id' => 'eliminar_visita']) }}
                        Eliminar visita?</label>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                    <button type="submit" class="btn btn-primary">Guardar canvis visita</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <script>
        $('.datepicker').datepicker({
            'weekStart': 1
        });

        $(document).on('click', '.modificar-visita', function () {
            $('#observacions-modificar').val($(this).attr('data-content'));
            $('#visita_id').val($(this).attr('data-id'));
        });
    </script>

    <script>
        $(document).on('click', 'ul.selectpicker li', function () {
            var selectedValue = $('.dropdown-toggle.selectpicker').attr('title');
            if (selectedValue == '-- Escull opció --') {
                $('#store').hide()
                $('#nopots').show()

            }
            else {
                $('#store').show()
                $('#nopots').hide()
            }
        });
    </script>
@stop