@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Clients<small>
                <a href="{{ URL::route('clients.afegir') }}" class="btn btn-primary btn-lg pull-right">Afegir client</a>
                <a href="{{ URL::route('clients.arxivats') }}" class="btn btn-primary btn-lg pull-right" style="margin-right:10px">Arxivats</a>
            </small></h1>
    </div>

    <table id="clients" class="table table-striped">
        <thead>
            <tr>
                <th>Nom 1</th>
                <th>Nom 2</th>
                <th>Telèfon 1</th>
                <th>Tipus</th>
                <th>Règim</th>
                <th>Comercial</th>
                <th>Data alta</th>
                <th></th>
            </tr>
        </thead>
        @forelse($clients as $client)
            <tr>
                <td>{{ $client->nom }} {{ $client->cognoms }}</td>
                <td>{{ $client->nom_altre }} {{ $client->cognoms_altre }}</td>
                <td>{{ $client->telefon_1 }}</td>
                <td>{{ $client->tipus->nom or ''}}</td>
                <td>{{ $client->regim->nom or '' }}</td>
                <td>{{ $client->comercial }}</td>
                <td>{{ $client->created_at->format('d-m-Y') }}</td>
                <td class="text-right">
                    <a href="{{ URL::route('client.visualitzar', $client->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a>
                </td>
            </tr>
        @empty

        @endforelse
    </table>


    <script>
        $(document).ready(function() {
            $.fn.dataTable.moment( 'DD-MM-YYYY' );
            $('#clients').dataTable({
                "iDisplayLength": 50,
            });
        } );
    </script>
@stop