@extends('layouts.layout')

@section('content')
    {{ Form::open(['id' => 'formulari']) }}
    <div class="col-md-4">
        <label for="taula">Què vols exportar?</label>
        <select id="taula" name="taula">
            <option value=""></option>
            <option value="immoble">Immobles</option>
            <option value="client">Clients</option>
            <option value="propietari">Propietaris</option>
            <option value="visites">Visites</option>
        </select>

        <button type="submit" style="margin-top:20px" class="btn btn-primary">Descarregar Excel</button>
    </div>
    <div class="col-md-8">
        <div id="fields"></div>
    </div>
    {{ Form::close() }}


    <script>
        var immoble = ['propietari_id', 'num_expedient', 'claus', 'titol', 'descripcio', 'tipus_id', 'regim_id', 'estat_id', 'ciutat_id', 'preu', 'quota', 'adreca_immoble', 'superficie', 'superficie_total', 'dormitoris', 'banys', 'certificat_eficiencia', 'financament', 'oportunitat', 'destacat', 'novetat', 'cedula_habitabilitat', 'retol_penjat', 'tipus_via', 'publicitat', 'url', 'places', 'preu_lloguer'];
        //var client = ['regim_id', 'tipus_id', 'preu_maxim', 'habitacions', 'nom', 'cognoms', 'telefon_1', 'comentari', 'nom_altre', 'cognoms_altre', 'ciutat_id', 'comercial', 'poblacio', 'telefon_2', 'email', 'valoracio', 'origen', 'nif', 'dades_economiques', 'poblacio_id', 'moblat', 'created_at'];
        var client = ['nom', 'cognoms', 'nom_altre', 'cognoms_altre', 'ciutat_id', 'comercial', 'data_contacte', 'poblacio', 'telefon_1', 'telefon_2', 'email', 'comentari', 'valoracio', 'origen', 'nif', 'dades_economiques', 'regim_id', 'preu_maxim', 'poblacio_id', 'tipus_id', 'moblat', 'habitacions', 'created_at'];
        var propietari = ['nom', 'cognoms', 'nom2', 'cognoms2', 'nif', 'adreca', 'telefon', 'telefon2', 'email'];
        var visites = ['client_id', 'data', 'observacions', 'num_expedient', 'adreca_immoble'];

        $( '#taula' ).change( function() {
            var taula = $(this).val();
            var fields = window[taula];

            $('#fields').html('');
            if (taula != "") {
                var data_inici = "{{ Carbon\Carbon::today()->startOfMonth()->format('d-m-Y') }}";
                var data_fi = "{{ Carbon\Carbon::today()->endOfMonth()->format('d-m-Y') }}";
                var html = '<div class="col-md-6" style="margin-bottom: 20px"><label>Data Inici</label><br><input type="text" name="data_inici" value="' + data_inici + '"></div>'
                            + '<div class="col-md-6"><label>Data Fi</label><br><input type="text" name="data_fi" value="' + data_fi + '"></div><div class="clearfix"></div>';
                $('#fields').append('<h2>Camps  <label style="font-size: 12px"><input type="checkbox" id="marcar-tot"> Marcar tot</label></h2>');
                if (taula == 'visites') {
                    $('#fields').append(html);
                       
                }
                $.each(fields, function (index, value) {
                    $('#fields').append('<label class="col-md-4"><input type="checkbox" value="' + value + '" name="checks[]"> ' + value + '</label>');
                });
                //$('#fields').append('<h2>Opcions</h2>');
            }
        });

        $( '#formulari').submit( function (e) {
             if ($('#taula').val() == "") e.preventDefault();
        });

        $(document).on('click', '#marcar-tot', function(e) {
            $('input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        })
    </script>
@stop

