<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</html>
<body>
<table>
    <thead>
    <tr>
        @foreach($fields as $field)
            <td>{{ ucfirst($field) }}</td>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($resultats as $line)
        <tr>
            @foreach($line as $index => $field)

                @if (in_array($index, $fields))
                    <td @if($index == 'observacions') width="500" @endif>{{ $field }}</td>
                @endif

            @endforeach
        </tr>
    @endforeach
    
    </tbody>
</table>
</body>