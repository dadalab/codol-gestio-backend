<?php
echo "<?xml version='1.0' encoding='UTF-8'?>";
$municipis = array(
    1 => array('nom'=>'Vic', 'codi_fotocasa' => 8298, 'codi_provincia' => 8, 'codi_postal'=>'08500'),
    2 => array('nom'=>'Manlleu', 'codi_fotocasa' => 8112, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    3 => array('nom'=>'Torelló', 'codi_fotocasa' => 8285, 'codi_provincia' => 8, 'codi_postal'=>'08570'),
    4 => array('nom'=>'Barcelona', 'codi_fotocasa' => 8019, 'codi_provincia' => 8, 'codi_postal'=>'08070'),
    5 => array('nom'=>'Sant Pere de Torelló', 'codi_fotocasa' => 8233, 'codi_provincia' => 8, 'codi_postal'=>'08572'),
    6 => array('nom'=>'Queralbs', 'codi_fotocasa' => 17043, 'codi_provincia' => 17, 'codi_postal'=>'17534'),
    7 => array('nom'=>'Sant Vicenç de Torelló', 'codi_fotocasa' => 8265, 'codi_provincia' => 8, 'codi_postal'=>'08571'),
    8 => array('nom'=>'Montesquiu', 'codi_fotocasa' => 8131, 'codi_provincia' => 8, 'codi_postal'=>'08585'),
    9 => array('nom'=>'Sant Quirze de Besora', 'codi_fotocasa' => 8237, 'codi_provincia' => 8, 'codi_postal'=>'08580'),
    10 => array('nom'=>'Sant Hipòlit de Voltregà', 'codi_fotocasa' => 8215, 'codi_provincia' => 8, 'codi_postal'=>'08512'),
    11 => array('nom'=>'Santa Maria de Corcó', 'codi_fotocasa' => 8254, 'codi_provincia' => 8, 'codi_postal'=>'08511'),
    12 => array('nom'=>'Folgueroles', 'codi_fotocasa' => 8083, 'codi_provincia' => 8, 'codi_postal'=>'08519'),
    13 => array('nom'=>'Oris', 'codi_fotocasa' => 8150, 'codi_provincia' => 8, 'codi_postal'=>'08573'),
    14 => array('nom'=>'Les Masies de Voltregà', 'codi_fotocasa' => 8117, 'codi_provincia' => 8, 'codi_postal'=>'08508'),
    15 => array('nom'=>'Roda de Ter', 'codi_fotocasa' => 8183, 'codi_provincia' => 8, 'codi_postal'=>'08510'),
    16 => array('nom'=>'Tona', 'codi_fotocasa' => 8283, 'codi_provincia' => 8, 'codi_postal'=>'08551'),
    17 => array('nom'=>'Ripoll', 'codi_fotocasa' => 17147, 'codi_provincia' => 17, 'codi_postal'=>'17500'),
    18 => array('nom'=>'Sant Julià de Vilatorta', 'codi_fotocasa' => 8220, 'codi_provincia' => 8, 'codi_postal'=>'08504'),
    19 => array('nom'=>'Calldetenes', 'codi_fotocasa' => 8037, 'codi_provincia' => 8, 'codi_postal'=>'08506'),
    20 => array('nom'=>'Brull', 'codi_fotocasa' => 8026, 'codi_provincia' => 8, 'codi_postal'=>'08553'),
    21 => array('nom'=>'Esquirol', 'codi_fotocasa' => 8254, 'codi_provincia' => 8, 'codi_postal'=>'08511'),
    22 => array('nom'=>'Queralbs', 'codi_fotocasa' => 17043, 'codi_provincia' => 17, 'codi_postal'=>'17534'),
    23 => array('nom'=>'SENTFORES', 'codi_fotocasa' => 8298, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
    24 => array('nom'=>'Sant Bartomeu del Grau', 'codi_fotocasa' => 8199, 'codi_provincia' => 8, 'codi_postal'=>'08503'),
    25 => array('nom'=>'El Brull', 'codi_fotocasa' => 8026, 'codi_provincia' => 8, 'codi_postal'=>'08553'),
    26 => array('nom'=>'Sora', 'codi_fotocasa' => 8272, 'codi_provincia' => 8, 'codi_postal'=>'08588'),
    27 => array('nom'=>'Sentfores', 'codi_fotocasa' => 8298, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
    28 => array('nom'=>'Sentfores (La Guixa)', 'codi_fotocasa' => 8298, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
    29 => array('nom'=>'Manlleu', 'codi_fotocasa' => 8112, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
);
$extres = array(
    1 => array('nom'=>'terrassa', 'codi_fotocasa' => 10),
    2 => array('nom'=>'jardí', 'codi_fotocasa' => 7),
    3 => array('nom'=>'balcó', 'codi_fotocasa' => 32),
    4 => array('nom'=>'amb mobles', 'codi_fotocasa' => 19),
    5 => array('nom'=>'ascensor', 'codi_fotocasa' => 13),
    6 => array('nom'=>'calefacció gas natural', 'codi_fotocasa' => 3),
    7 => array('nom'=>'piscina', 'codi_fotocasa' => 17),
    8 => array('nom'=>'video porter', 'codi_fotocasa' => 81),
    10 => array('nom'=>'zona infantil', 'codi_fotocasa' => 83),
    11 => array('nom'=>'cuina amb electrodomèstics', 'codi_fotocasa' => 131),
    12 => array('nom'=>'armaris encastats', 'codi_fotocasa' => 2),
    14 => array('nom'=>'pàrquin inclòs', 'codi_fotocasa' => 26),
    15 => array('nom'=>'aire condicionat', 'codi_fotocasa' => 1),
    17 => array('nom'=>'garatge privat', 'codi_fotocasa' => 5),
    18 => array('nom'=>'traster', 'codi_fotocasa' => 11),
    19 => array('nom'=>'cuina office', 'codi_fotocasa' => 15),
);
$certificats_eficiencia = array (
    'A' => 139,
    'B' => 140,
    'C' => 141,
    'D' => 142,
    'E' => 143,
    'F' => 144,
    'G' => 145
);
?>

<AdRealestate xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    @foreach($immobles as $immoble)
    <RealEstate>
        <TitleAd>{{ $immoble->titol }}</TitleAd>
        <DescriptionAd>{{ $immoble->descripcio }}</DescriptionAd>
        <CustomerRef>{{ $immoble->num_expedient }}</CustomerRef>
        <CustomerId>{{ $immoble->oficina->id }}</CustomerId>
        <LanguageId>1</LanguageId>
        <MediaList>
            @foreach($immoble->imatges as $imatge)
            <Media>
                <Name>{{ $imatge->nom }}.jpg</Name>
                <Url>http://codolgestio.net/content/{{ $imatge->nom }}.jpg</Url>
                <DictionaryId>7</DictionaryId>
                @if ($imatge->ordre == 0)
                <Order>1</Order>
                @else
                <Order>{{ $imatge->ordre }}</Order>
                @endif
            </Media>
            @endforeach
        </MediaList>
        <ExternalURL></ExternalURL>
        <PriceList>
            <Price>
                {{-- REGIM --}}
                @if ($immoble->regim_id == 1)
                    <TransactionId>3</TransactionId>
                @elseif ($immoble->regim_id == 2)
                    <TransactionId>1</TransactionId>
                @elseif ($immoble->regim_id == 3)
                    <TransactionId>9</TransactionId>
                @else
                    <TransactionId></TransactionId>
                @endif
                <CurrencyId>1</CurrencyId>
                <PriceValue>{{ $immoble->preu }}</PriceValue>
            </Price>
        </PriceList>
        <Locations>
            <?php if( array_key_exists($immoble->ciutat_id, $municipis) ): ?>
            <ZIPCode><?php echo $municipis[$immoble->ciutat_id]['codi_postal']; ?></ZIPCode>
            <?php endif; ?>
            <LocationList>
                <Location>
                    {{-- PAIS --}}
                    <Value>724</Value>
                    <TypeId>1</TypeId>
                </Location>
                <?php if( array_key_exists($immoble->ciutat_id, $municipis) ): ?>
                <Location>
                    {{-- PROVINCIA --}}
                    <Value><?php echo $municipis[$immoble->ciutat_id]['codi_provincia']; ?></Value>
                    <TypeId>2</TypeId>
                </Location>
                <Location>
                    {{-- MUNICIPI --}}
                    <Value><?php echo $municipis[$immoble->ciutat_id]['codi_fotocasa']; ?></Value>
                    <TypeId>3</TypeId>
                </Location>
                <?php endif; ?>
            </LocationList>
        </Locations>
        <NRooms>{{ $immoble->dormitoris }}</NRooms>
        <NBathrooms>{{ $immoble->banys }}</NBathrooms>
        {{-- TIPUS IMMOBLE --}}
        @if ($immoble->tipus_id == 1)
            <BuildingTypeId>2</BuildingTypeId>
        @elseif ($immoble->tipus_id == 2)
            <BuildingTypeId>2</BuildingTypeId>
        @elseif ($immoble->tipus_id == 3)
            <BuildingTypeId>5</BuildingTypeId>
        @elseif ($immoble->tipus_id == 4)
            <BuildingTypeId>7</BuildingTypeId>
        @elseif ($immoble->tipus_id == 5)
            <BuildingTypeId>3</BuildingTypeId>
        @elseif ($immoble->tipus_id == 6)
            <BuildingTypeId>4</BuildingTypeId>
        @elseif ($immoble->tipus_id == 7)
            <BuildingTypeId>14</BuildingTypeId>
        @else
            <BuildingTypeId></BuildingTypeId>
        @endif
        {{-- TIPUS HABITATGE --}}
        @if ($immoble->tipus_id == 1)
            <HouseTypeId>1</HouseTypeId>
        @elseif ($immoble->tipus_id == 2)
            @if ($immoble->tipus_reclam == 6)
                <HouseTypeId>5</HouseTypeId>
            @else
                <HouseTypeId>3</HouseTypeId>
            @endif
        @elseif ($immoble->tipus_id == 3)
            <HouseTypeId>41</HouseTypeId>
        @elseif ($immoble->tipus_id == 4)
            <HouseTypeId></HouseTypeId>
        @elseif ($immoble->tipus_id == 5)
            <HouseTypeId>40</HouseTypeId>
        @elseif ($immoble->tipus_id == 6)
            <HouseTypeId></HouseTypeId>
        @elseif ($immoble->tipus_id == 7)
            <HouseTypeId>57</HouseTypeId>
        @else
            <HouseTypeId></HouseTypeId>
        @endif
        {{-- EXTRES --}}
        <Extras>
		@foreach($immoble->tags as $tag)
            <?php if( array_key_exists($tag->id, $extres) ): ?>
        	<Extra>
        		<AccessoryId><?php echo $extres[$tag->id]['codi_fotocasa']; ?></AccessoryId>
			</Extra>
            <?php endif; ?>
		@endforeach
        {{-- Certificat eficiencia --}}
        <?php if( array_key_exists($immoble->certificat_eficiencia, $certificats_eficiencia) ): ?>
            <Extra>
                <AccessoryId><?php echo $certificats_eficiencia[$immoble->certificat_eficiencia]; ?></AccessoryId>
            </Extra>
        <?php endif; ?>
		</Extras>
        {{-- ESTAT --}}
        @if ($immoble->tipus_id == 1)
            <BuildingStatus>1</BuildingStatus>
        @elseif ($immoble->tipus_id == 2)
            <BuildingStatus>2</BuildingStatus>
        @elseif ($immoble->tipus_id == 3)
            <BuildingStatus>3</BuildingStatus>
        @elseif ($immoble->tipus_id == 4)
            <BuildingStatus>1</BuildingStatus>
        @elseif ($immoble->tipus_id == 5)
            <BuildingStatus>5</BuildingStatus>
        @elseif ($immoble->tipus_id == 6)
            <BuildingStatus>4</BuildingStatus>
        @else
            <BuildingStatus>0</BuildingStatus>
        @endif
        <Area>{{ $immoble->superficie_total }}</Area>
        <LivingArea>{{ $immoble->superficie }}</LivingArea>
        <ParkingSpaceId>{{ $immoble->places }}</ParkingSpaceId>
        {{-- OFICINA --}}
        @if ($immoble->oficina->id == 1)
            <Name>Còdol Gestió - Vic</Name>
            <Email>vic@codolgestio.net</Email>
            <Contact>93 886 36 38</Contact>
        @elseif ($immoble->oficina->id == 2)
            <Name>Còdol Gestió - Manlleu</Name>
            <Email>manlleu@codolgestio.net</Email>
            <Contact>93 850 68 00</Contact>
        @else
            <Name>Còdol Gestió - Torelló</Name>
            <Email>torello@codolgestio.net</Email>
            <Contact>93 859 02 00</Contact>
        @endif
        <Street>{{ $immoble->adreca_publica }}</Street>
        {{-- VIA --}}
            @if ($immoble->tipus_via == "Avinguda")
                <StreetTypeId>29</StreetTypeId>
            @elseif ($immoble->tipus_via == "Carrer")
                <StreetTypeId>53</StreetTypeId>
            @elseif ($immoble->tipus_via == "Carretera")
                <StreetTypeId>94</StreetTypeId>
            @elseif ($immoble->tipus_via == "Passeig")
                <StreetTypeId>253</StreetTypeId>
            @elseif ($immoble->tipus_via == "Plaça")
                <StreetTypeId>273</StreetTypeId>
            @elseif ($immoble->tipus_via == "Rambla")
                <StreetTypeId>279</StreetTypeId>
            @elseif ($immoble->tipus_via == "Ronda")
                <StreetTypeId>282</StreetTypeId>
            @else
                <StreetTypeId></StreetTypeId>
            @endif
        <HideEmail></HideEmail>
        <HideNumber></HideNumber>
        <ConservationState>{{ $immoble->estat->nom }}</ConservationState>
        <X>{{ $immoble->coordx }}</X>
        <Y>{{ $immoble->coordy }}</Y>
        <ShowPoi>2</ShowPoi>
        <VisualizationTypeId>2</VisualizationTypeId>
    </RealEstate>
    @endforeach
</AdRealestate>
