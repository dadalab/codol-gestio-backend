<?php
echo "<?xml version='1.0' encoding='UTF-8'?>";
$municipis = array(
    1 => array('nom'=>'Vic', 'codi_reclam' => 826, 'codi_provincia' => 8, 'codi_postal'=>'08500'),
    2 => array('nom'=>'Manlleu', 'codi_reclam' => 791, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    3 => array('nom'=>'Torelló', 'codi_reclam' => 825, 'codi_provincia' => 8, 'codi_postal'=>'08570'),
    4 => array('nom'=>'Barcelona', 'codi_reclam' => 344, 'codi_provincia' => 8, 'codi_postal'=>'08070'),
    5 => array('nom'=>'Sant Pere de Torelló', 'codi_reclam' => 812, 'codi_provincia' => 8, 'codi_postal'=>'08572'),
    6 => array('nom'=>'Queralbs', 'codi_reclam' => 867, 'codi_provincia' => 17, 'codi_postal'=>'17534'),
    7 => array('nom'=>'Sant Vicenç de Torelló', 'codi_reclam' => 815, 'codi_provincia' => 8, 'codi_postal'=>'08571'),
    8 => array('nom'=>'Montesquiu', 'codi_reclam' => 794, 'codi_provincia' => 8, 'codi_postal'=>'08585'),
    9 => array('nom'=>'Sant Quirze de Besora', 'codi_reclam' => 813, 'codi_provincia' => 8, 'codi_postal'=>'08580'),
    10 => array('nom'=>'Sant Hipòlit de Voltregà', 'codi_reclam' => 808, 'codi_provincia' => 8, 'codi_postal'=>'08512'),
    11 => array('nom'=>'Santa Maria de Corcó', 'codi_reclam' => 820, 'codi_provincia' => 8, 'codi_postal'=>'08511'),
    12 => array('nom'=>'Folgueroles', 'codi_reclam' => 787, 'codi_provincia' => 8, 'codi_postal'=>'08519'),
    13 => array('nom'=>'Oris', 'codi_reclam' => 797, 'codi_provincia' => 8, 'codi_postal'=>'08573'),
    14 => array('nom'=>'Les Masies de Voltregà', 'codi_reclam' => 793, 'codi_provincia' => 8, 'codi_postal'=>'08508'),
    15 => array('nom'=>'Roda de Ter', 'codi_reclam' => 802, 'codi_provincia' => 8, 'codi_postal'=>'08510'),
    16 => array('nom'=>'Tona', 'codi_reclam' => 824, 'codi_provincia' => 8, 'codi_postal'=>'08551'),
    17 => array('nom'=>'Ripoll', 'codi_reclam' => 869, 'codi_provincia' => 17, 'codi_postal'=>'17500'),
    18 => array('nom'=>'Sant Julià de Vilatorta', 'codi_reclam' => 809, 'codi_provincia' => 8, 'codi_postal'=>'08504'),
    19 => array('nom'=>'Calldetenes', 'codi_reclam' => 781, 'codi_provincia' => 8, 'codi_postal'=>'08506'),
    20 => array('nom'=>'Brull', 'codi_reclam' => 780, 'codi_provincia' => 8, 'codi_postal'=>'08553'),
    21 => array('nom'=>'Esquirol', 'codi_reclam' => 1017, 'codi_provincia' => 8, 'codi_postal'=>'08511'),
    22 => array('nom'=>'Queralbs', 'codi_reclam' => 867, 'codi_provincia' => 17, 'codi_postal'=>'17534'),
    23 => array('nom'=>'SENTFORES', 'codi_reclam' => 826, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
    24 => array('nom'=>'Sant Bartomeu del Grau', 'codi_reclam' => 8199, 'codi_provincia' => 8, 'codi_postal'=>'08503'),
    25 => array('nom'=>'El Brull', 'codi_reclam' => 780, 'codi_provincia' => 8, 'codi_postal'=>'08553'),
    26 => array('nom'=>'Sora', 'codi_reclam' => 806, 'codi_provincia' => 8, 'codi_postal'=>'08588'),
    27 => array('nom'=>'Sentfores', 'codi_reclam' => 826, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
    28 => array('nom'=>'Sentfores (La Guixa)', 'codi_reclam' => 826, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
    29 => array('nom'=>'Manlleu', 'codi_reclam' => 791, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
);
$extres = array(
    1 => array('nom'=>'terrassa', 'codi_reclam' => 10),
    2 => array('nom'=>'jardí', 'codi_reclam' => 7),
    3 => array('nom'=>'balcó', 'codi_reclam' => 32),
    4 => array('nom'=>'amb mobles', 'codi_reclam' => 19),
    5 => array('nom'=>'ascensor', 'codi_reclam' => 13),
    6 => array('nom'=>'calefacció gas natural', 'codi_reclam' => 3),
    7 => array('nom'=>'piscina', 'codi_reclam' => 17),
    8 => array('nom'=>'video porter', 'codi_reclam' => 81),
    10 => array('nom'=>'zona infantil', 'codi_reclam' => 83),
    11 => array('nom'=>'cuina amb electrodomèstics', 'codi_reclam' => 131),
    12 => array('nom'=>'armaris encastats', 'codi_reclam' => 2),
    14 => array('nom'=>'pàrquin inclòs', 'codi_reclam' => 26),
    15 => array('nom'=>'aire condicionat', 'codi_reclam' => 1),
    17 => array('nom'=>'garatge privat', 'codi_reclam' => 5),
    18 => array('nom'=>'traster', 'codi_reclam' => 11),
    19 => array('nom'=>'cuina office', 'codi_reclam' => 15),
);
$certificats_eficiencia = array (
    'A' => 139,
    'B' => 140,
    'C' => 141,
    'D' => 142,
    'E' => 143,
    'F' => 144,
    'G' => 145
);
?>

<AdRealestate xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    @foreach($immobles as $immoble)
    <RealEstate>
        <TitleAd>{{ $immoble->titol }}</TitleAd>
        <DescriptionAd>{{ $immoble->descripcio }}</DescriptionAd>
        <CustomerRef>{{ $immoble->num_expedient }}</CustomerRef>
        <CustomerId>{{ $immoble->oficina->id }}</CustomerId>
        <LanguageId>1</LanguageId>
        <MediaList>
            @foreach($immoble->imatges as $imatge)
            <Media>
                <Name>{{ $imatge->nom }}.jpg</Name>
                <Url>http://codolgestio.net/content/{{ $imatge->nom }}.jpg</Url>
                <DictionaryId>7</DictionaryId>
                @if ($imatge->ordre == 0)
                <Order>1</Order>
                @else
                <Order>{{ $imatge->ordre }}</Order>
                @endif
            </Media>
            @endforeach
        </MediaList>
        <ExternalURL></ExternalURL>
        <PriceList>
            <Price>
                {{-- REGIM --}}
                @if ($immoble->regim_id == 1)
                    <TransactionId>3</TransactionId>
                @elseif ($immoble->regim_id == 2)
                    <TransactionId>1</TransactionId>
                @elseif ($immoble->regim_id == 3)
                    <TransactionId>9</TransactionId>
                @else
                    <TransactionId></TransactionId>
                @endif
                <CurrencyId>1</CurrencyId>
                <PriceValue>{{ $immoble->preu }}</PriceValue>
            </Price>
        </PriceList>
        <Locations>
            <?php if( array_key_exists($immoble->ciutat_id, $municipis) ): ?>
            <ZIPCode><?php echo $municipis[$immoble->ciutat_id]['codi_postal']; ?></ZIPCode>
            <?php endif; ?>
            <LocationList>
                <Location>
                    {{-- PAIS --}}
                    <Value>724</Value>
                    <TypeId>1</TypeId>
                </Location>
                <?php if( array_key_exists($immoble->ciutat_id, $municipis) ): ?>
                <Location>
                    {{-- PROVINCIA --}}
                    <Value><?php echo $municipis[$immoble->ciutat_id]['codi_provincia']; ?></Value>
                    <TypeId>2</TypeId>
                </Location>
                <Location>
                    {{-- MUNICIPI --}}
                    <Value><?php echo $municipis[$immoble->ciutat_id]['codi_reclam']; ?></Value>
                    <TypeId>3</TypeId>
                </Location>
                <?php endif; ?>
            </LocationList>
        </Locations>
        
        <NRooms>{{ $immoble->dormitoris }}</NRooms>
        <NBathrooms>{{ $immoble->banys }}</NBathrooms>
        
        {{-- TIPUS IMMOBLE --}}
        <BuildingTypeId>{{ $immoble->tipus_reclam }}</BuildingTypeId>
        
        {{-- EXTRES --}}
        <Extras>
		@foreach($immoble->tags as $tag)
            <?php if( array_key_exists($tag->id, $extres) ): ?>
        	<Extra>
        		<AccessoryId><?php echo $extres[$tag->id]['codi_reclam']; ?></AccessoryId>
			</Extra>
            <?php endif; ?>
		@endforeach
        
        {{-- Certificat eficiencia --}}
        <?php if( array_key_exists($immoble->certificat_eficiencia, $certificats_eficiencia) ): ?>
            <Extra>
                <AccessoryId><?php echo $certificats_eficiencia[$immoble->certificat_eficiencia]; ?></AccessoryId>
            </Extra>
        <?php endif; ?>
		</Extras>
        
        <Area>{{ $immoble->superficie_total }}</Area>
        <LivingArea>{{ $immoble->superficie }}</LivingArea>
        <ParkingSpaceId>{{ $immoble->places }}</ParkingSpaceId>
        
        {{-- OFICINA --}}
        @if ($immoble->oficina->id == 1)
            <Name>Còdol Gestió - Vic</Name>
            <Email>vic@codolgestio.net</Email>
            <Contact>93 886 36 38</Contact>
        @elseif ($immoble->oficina->id == 2)
            <Name>Còdol Gestió - Manlleu</Name>
            <Email>manlleu@codolgestio.net</Email>
            <Contact>93 850 68 00</Contact>
        @else
            <Name>Còdol Gestió - Torelló</Name>
            <Email>torello@codolgestio.net</Email>
            <Contact>93 859 02 00</Contact>
        @endif
        <Street>{{ $immoble->adreca_publica }}</Street>
        
        {{-- VIA --}}
        @if ($immoble->tipus_via)
            <StreetTypeId>$immoble->tipus_via</StreetTypeId>
        @endif
    
        <HideEmail></HideEmail>
        <HideNumber></HideNumber>
        <ConservationState>{{ $immoble->estat->nom }}</ConservationState>
        <X>{{ $immoble->coordx }}</X>
        <Y>{{ $immoble->coordy }}</Y>
        <ShowPoi>2</ShowPoi>
        <VisualizationTypeId>2</VisualizationTypeId>
    </RealEstate>
    @endforeach
</AdRealestate>
