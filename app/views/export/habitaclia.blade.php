 <?php
echo "<?xml version='1.0' encoding='ISO-8859-1' ?>";
$municipis = array(
    1 => array('nom'=>'Vic', 'codi_habitaclia' => 29810007, 'codi_provincia' => 8, 'codi_postal'=>'08500'),
    2 => array('nom'=>'Manlleu', 'codi_habitaclia' => 11200000, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    3 => array('nom'=>'Torelló', 'codi_habitaclia' => 28580001, 'codi_provincia' => 8, 'codi_postal'=>'08570'),
    4 => array('nom'=>'Barcelona', 'codi_habitaclia' => 1930008, 'codi_provincia' => 8, 'codi_postal'=>'08070'),
    5 => array('nom'=>'Sant Pere de Torelló', 'codi_habitaclia' => 23310007, 'codi_provincia' => 8, 'codi_postal'=>'08572'),
    6 => array('nom'=>'Queralbs', 'codi_habitaclia' => 4330008, 'codi_provincia' => 17, 'codi_postal'=>'17534'),
    7 => array('nom'=>'Sant Vicenç de Torelló', 'codi_habitaclia' => 26520002, 'codi_provincia' => 8, 'codi_postal'=>'08571'),
    8 => array('nom'=>'Montesquiu', 'codi_habitaclia' => 13110007, 'codi_provincia' => 8, 'codi_postal'=>'08585'),
    9 => array('nom'=>'Sant Quirze de Besora', 'codi_habitaclia' => 23780001, 'codi_provincia' => 8, 'codi_postal'=>'08580'),
    10 => array('nom'=>'Sant Hipòlit de Voltregà', 'codi_habitaclia' => 21530008, 'codi_provincia' => 8, 'codi_postal'=>'08512'),
    11 => array('nom'=>'Santa Maria de Corcó', 'codi_habitaclia' => 25410007, 'codi_provincia' => 8, 'codi_postal'=>'08511'),
    12 => array('nom'=>'Folgueroles', 'codi_habitaclia' => 8320002, 'codi_provincia' => 8, 'codi_postal'=>'08519'),
    13 => array('nom'=>'Oris', 'codi_habitaclia' => 15090004, 'codi_provincia' => 8, 'codi_postal'=>'08573'),
    14 => array('nom'=>'Les Masies de Voltregà', 'codi_habitaclia' => 11730008, 'codi_provincia' => 8, 'codi_postal'=>'08508'),
    15 => array('nom'=>'Roda de Ter', 'codi_habitaclia' => 18310007, 'codi_provincia' => 8, 'codi_postal'=>'08510'),
    16 => array('nom'=>'Tona', 'codi_habitaclia' => 28300000, 'codi_provincia' => 8, 'codi_postal'=>'08551'),
    17 => array('nom'=>'Ripoll', 'codi_habitaclia' => 14790004, 'codi_provincia' => 17, 'codi_postal'=>'17500'),
    18 => array('nom'=>'Sant Julià de Vilatorta', 'codi_habitaclia' => 22050006, 'codi_provincia' => 8, 'codi_postal'=>'08504'),
    19 => array('nom'=>'Calldetenes', 'codi_habitaclia' => 3700000, 'codi_provincia' => 8, 'codi_postal'=>'08506'),
    20 => array('nom'=>'Brull', 'codi_habitaclia' => 2660009, 'codi_provincia' => 8, 'codi_postal'=>'08553'),
    21 => array('nom'=>'Esquirol', 'codi_habitaclia' => 8254, 'codi_provincia' => 8, 'codi_postal'=>'08511'),
    22 => array('nom'=>'Queralbs', 'codi_habitaclia' => 4330008, 'codi_provincia' => 17, 'codi_postal'=>'17534'),
    23 => array('nom'=>'SENTFORES', 'codi_habitaclia' => 29810007, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
    24 => array('nom'=>'Sant Bartomeu del Grau', 'codi_habitaclia' => 19950006, 'codi_provincia' => 8, 'codi_postal'=>'08503'),
    25 => array('nom'=>'El Brull', 'codi_habitaclia' => 2660009, 'codi_provincia' => 8, 'codi_postal'=>'08553'),
    26 => array('nom'=>'Sora', 'codi_habitaclia' => 27260009, 'codi_provincia' => 8, 'codi_postal'=>'08588'),
  //  27 => array('nom'=>'Sentfores', 'codi_habitaclia' => 29810007, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
  //  28 => array('nom'=>'Sentfores (La Guixa)', 'codi_habitaclia' => 29810007, 'codi_provincia' => 8, 'codi_postal'=>'08505'),
  //  29 => array('nom'=>'Manlleu', 'codi_habitaclia' => 11200000, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    27 => array('nom'=>'Olot', 'codi_habitaclia' => 11430008, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    28 => array('nom'=>'Gurb', 'codi_habitaclia' => 10000000, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    31 => array('nom'=>'Olot', 'codi_habitaclia' => 11430008, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    32 => array('nom'=>'Centelles', 'codi_habitaclia' => 6730008, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    33 => array('nom'=>'Taradell', 'codi_habitaclia' => 27850006, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    34 => array('nom'=>'Seva (Montanyà)', 'codi_habitaclia' => 26900000, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    35 => array('nom'=>'Campdevanol', 'codi_habitaclia' => 3660009, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    36 => array('nom'=>'Girona', 'codi_habitaclia' => 7920002, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    37 => array('nom'=>'Sant Miquel de Balenyà', 'codi_habitaclia' => 1740003, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    38 => array('nom'=>'Balenyà', 'codi_habitaclia' => 1740003, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    41 => array('nom'=>'Ametlla del Vallès', 'codi_habitaclia' => 570005, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    42 => array('nom'=>'Puig-reig (Berga)', 'codi_habitaclia' => 17510007, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    43 => array('nom'=>'Muntanyola', 'codi_habitaclia' => 12900000, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    44 => array('nom'=>'Santa Eulàlia de Riuprimer', 'codi_habitaclia' => 24760009, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    48 => array('nom'=>'Santa Eugènia de Berga', 'codi_habitaclia' => 24600000, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
    49 => array('nom'=>'La Garriga', 'codi_habitaclia' => 8850006, 'codi_provincia' => 8, 'codi_postal'=>'08560'),
);

$provinciaGirona = ['Ripoll', 'Queralbs', 'Olot', 'Campdevanol', 'Girona'];

$extres = array(
    1 => array('nom'=>'terrassa', 'codi_fotocasa' => 10),
    2 => array('nom'=>'jardí', 'codi_fotocasa' => 7),
    3 => array('nom'=>'balcó', 'codi_fotocasa' => 32),
    4 => array('nom'=>'amb mobles', 'codi_fotocasa' => 19),
    5 => array('nom'=>'ascensor', 'codi_fotocasa' => 13),
    6 => array('nom'=>'calefacció gas natural', 'codi_fotocasa' => 3),
    7 => array('nom'=>'piscina', 'codi_fotocasa' => 17),
    8 => array('nom'=>'video porter', 'codi_fotocasa' => 81),
    10 => array('nom'=>'zona infantil', 'codi_fotocasa' => 83),
    11 => array('nom'=>'cuina amb electrodomèstics', 'codi_fotocasa' => 131),
    12 => array('nom'=>'armaris encastats', 'codi_fotocasa' => 2),
    14 => array('nom'=>'pàrquin inclòs', 'codi_fotocasa' => 26),
    15 => array('nom'=>'aire condicionat', 'codi_fotocasa' => 1),
    17 => array('nom'=>'garatge privat', 'codi_fotocasa' => 5),
    18 => array('nom'=>'traster', 'codi_fotocasa' => 11),
    19 => array('nom'=>'cuina office', 'codi_fotocasa' => 15),
);

$tipos = [
    'Pis' => 'Habitatge',
    'Casa' => 'Habitatge',
    'Local' => 'Local',
    'Nau' => 'Industrial',
    'Pàrquing' => 'Parking',
    'Solar' => 'Terrenos y solares',
    'Traster' => '',
    'Oficina' => 'Oficina',
];

$subtipus = [
    1 => [1, 4, 'Apartament'],
    2 => [1, 7, 'Àtic'],
    3 => [1, 1, 'Pis'],
    4 => [1, 2, 'Dúplex'],
    5 => [1, 13, 'Planta baixa'],
    6 => [1, 3, 'Casa'],
    7 => [1, 3, 'Casa'],
    8 => [1, 3, 'Casa'],
    9 => [1, 9, 'Loft'],
    10 => [5, 3, 'Solar industrial'],
    11 => [5, 5, 'Solar urbà'],
    12 => [5, 1, 'Terreny'],
    13 => [5, 2, 'Finca rústica'],
    14 => [1, 8, 'Masia'],
    15 => [3, 1, 'Local comercial'],
    16 => [3, 1, 'Traspàs'],
    17 => [8, 3, 'Edifici'],
    18 => [4, 1, 'Nau industrial'],
    19 => [6, 1, 'Plaça pàrquing'],
    20 => [1, 6, 'Torre'],
    21 => [2, 1, 'Oficina'],
    22 => [2, 1, 'Oficina']
];
?>

<producto xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    @foreach($immobles as $immoble)
    <inmueble>
        <descripcion>{{ $immoble->descripcio }}</descripcion>
        <referencia>{{ $immoble->num_expedient }}</referencia>
        
        <id_sucursal>{{ $immoble->oficina->id }}</id_sucursal>
        @if ($immoble->oficina->id == 1)
            <email_sucursal>vic@codolgestio.net</email_sucursal>
        @elseif ($immoble->oficina->id == 2)
            <email_sucursal>manlleu@codolgestio.net</email_sucursal>
        @else
            <email_sucursal>torello@codolgestio.net</email_sucursal>
        @endif

        <venta_01>{{ $immoble->regim->id == 2 ? 1 : 0 }}</venta_01>
        <alquiler_01>{{ $immoble->regim->id == 1 ? 1 : 0 }}</alquiler_01>
        <alquiler_opcion_venta_01>0</alquiler_opcion_venta_01>
        <traspaso_01>0</traspaso_01>
        <alquiler_temporada_01>0</alquiler_temporada_01>

        <precio_venta>{{ $immoble->preu }}</precio_venta>
        <precio_alquiler>{{ $immoble->preu }}</precio_alquiler>

        <tipo>{{ $tipos[$immoble->tipus->nom] }}</tipo>
        @if($immoble->tipus_reclam) <tip_inm>{{ $subtipus[$immoble->tipus_reclam][0] }}</tip_inm> 
        @else <tip_inm>1</tip_inm>
        @endif
        @if(isset($subtipus[$immoble->tipus_reclam][2]))<subtipo>{{ $subtipus[$immoble->tipus_reclam][2] ?: $tipos[$immoble->tipus->nom] }}</subtipo>
        @else <subtipo>Apartament</subtipo>
        @endif
        @if($immoble->tipus_reclam)<tip_inm2>{{ $subtipus[$immoble->tipus_reclam][1] }}</tip_inm2>
        @else <tip_inm2>4</tip_inm2>
        @endif
        <habitaciones>{{ $immoble->dormitoris }}</habitaciones>
        <banos>{{ $immoble->banys }}</banos>
        <m2construidos>{{ $immoble->superficie }}</m2construidos>
        
        @if ($immoble->text_destacat) <destacado>{{ $immoble->text_destacat }}</destacado>@endif

        <provincia>{{ in_array($immoble->ciutat->nom, $provinciaGirona) ? 'Girona' : 'Barcelona' }}</provincia>
        <cod_prov>{{ in_array($immoble->ciutat->nom, $provinciaGirona) ? 2 : 1 }}</cod_prov>
        <localidad>{{ $immoble->ciutat->nom }}</localidad>
        @if ( array_key_exists($immoble->ciutat_id, $municipis) )
            <cod_pob><?php echo $municipis[$immoble->ciutat_id]['codi_habitaclia']; ?></cod_pob>
        @endif
        <ubicacion>{{ $immoble->tipus_via . ' ' . $immoble->adreca_publica }}</ubicacion>
        @if (isset($municipis[$immoble->ciutat_id]['codi_postal']))<cp>{{ $municipis[$immoble->ciutat_id]['codi_postal'] }}</cp>@endif

        <photos>
            @foreach($immoble->imatges as $imatge)
            <photo>
                <url>http://codolgestio.net/content/{{ $imatge->nom }}.jpg</url>
                @if ($imatge->ordre == 0)
                <numimagen>1</numimagen>
                @else
                <numimagen>{{ $imatge->ordre }}</numimagen>
                @endif
            </photo>
            @endforeach
        </photos>

        <mapa>
            <latitud>{{ $immoble->coordy }}</latitud>
            <longitud>{{ $immoble->coordx }}</longitud>
            <zoom>16</zoom>
            <puntero>0</puntero>
        </mapa>

        <calif_energetica>{{ $immoble->certificat_eficiencia == 'En tràmit' ? 'ZZ' : $immoble->certificat_eficiencia }}</calif_energetica>

        <garaje_01>{{ $immoble->tags->contains(14) ? 1 : 0 }}</garaje_01>
        <terraza_01>{{ $immoble->tags->contains(1) ? 1 : 0 }}</terraza_01>
        <ascensor_01>{{ $immoble->tags->contains(5) ? 1 : 0 }}</ascensor_01>
        <trastero_01>{{ $immoble->tags->contains(18) ? 1 : 0 }}</trastero_01>
        <piscina_01>{{ $immoble->tags->contains(7) ? 1 : 0 }}</piscina_01>
        <buhardilla_01>0</buhardilla_01>
        <lavadero_01>{{ $immoble->tags->contains(23) ? 1 : 0 }}</lavadero_01>
        <jardin_01>{{ $immoble->tags->contains(2) ? 1 : 0 }}</jardin_01>
        <eqdeportivos_01>0</eqdeportivos_01>
        <vistasalmar_01>{{ $immoble->tags->contains(20) ? 1 : 0 }}</vistasalmar_01>
        <vistasalamontana_01>{{ $immoble->tags->contains(21) ? 1 : 0 }}</vistasalamontana_01>
        <vistasalaciudad_01>{{ $immoble->tags->contains(22) ? 1 : 0 }}</vistasalaciudad_01>
        <cercatransportepub_01>1</cercatransportepub_01>
        <aireacondicionado_01>{{ $immoble->tags->contains(15) ? 1 : 0 }}</aireacondicionado_01>
        <calefaccion_01>{{ $immoble->tags->contains(6) ? 1 : 0 }}</calefaccion_01>
        <chimenea_01>{{ $immoble->tags->contains(16) ? 1 : 0 }}</chimenea_01>
        <cocina_office>{{ $immoble->tags->contains(19) ? 1 : 0 }}</cocina_office>
        <despacho>0</despacho>
        <amueblado>{{ $immoble->tags->contains(4) ? 1 : 0 }}</amueblado>
        <vigilancia>0</vigilancia>
        <de_banco>{{ $immoble->tags->contains(24) ? 1 : 0 }}</de_banco>
        <estadococina>Equipada</estadococina>
		
    </inmueble>
    @endforeach
</producto>