@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Llistat immobles</h1>
    </div>

    <table class="table no-selectpicker" id="export">
        <thead>
        <th>Expedient</th>
        <th>Adreça</th>
        <th>Ciutat</th>
        <th>Tipus</th>
        <th>Règim</th>
        <th></th>
        <th id="web_count" class="text-center"></th>
        <th class="text-center" id="fotocasa_count"></th>
        <th id="habitaclia_count"></th>
        <th id="reclam_count"></th>
        <th>Tipus Reclam</th>
        </thead>
        <tbody>
        @foreach($immobles as $immoble)
            <tr data-immoble="{{ $immoble->id }}">
                <td>{{ $immoble->num_expedient }}</td>
                <td>{{ \Str::limit($immoble->adreca_immoble, 25) }}</td>
                <td>{{ \Str::limit($immoble->ciutat->nom, 15) }}</td>
                <td>{{ $immoble->tipus->nom }}</td>
                <td>{{ $immoble->regim->nom }}</td>
                <td>
                    <span data-toggle="tooltip" data-nom="destacat" data-val="{{ $immoble->destacat }}" title="destacat" class="glyphicon glyphicon-pushpin @if ($immoble->destacat) text-primary @endif"></span>
                    <span data-toggle="tooltip" data-nom="oportunitat" data-val="{{ $immoble->oportunitat }}" title="oportunitat" class="glyphicon glyphicon-star @if ($immoble->oportunitat) text-primary @endif"></span>
                    <span data-toggle="tooltip" data-nom="novetat" data-val="{{ $immoble->novetat }}" title="novetat" class="glyphicon glyphicon-certificate @if ($immoble->novetat) text-primary @endif"></span>
                    <span data-toggle="tooltip" data-nom="fet_a_mida" data-val="{{ $immoble->fet_a_mida }}" title="fet a mida" class="glyphicon glyphicon-ok-circle @if ($immoble->fet_a_mida) text-primary @endif"></span>
                </td>
                <td class="text-center">                    
                    <span data-toggle="tooltip" data-nom="publicat" data-val="{{ $immoble->publicat }}" title="publicat" class="glyphicon glyphicon-globe @if ($immoble->publicat) text-primary @endif"></span>
                </td>
                <td class="text-center">
                    <span data-toggle="tooltip" data-nom="fotocasa" data-val="{{ $immoble->fotocasa }}" title="fotocasa" class="icon-portal @if ($immoble->fotocasa) text-primary @endif">fc</span>
                </td>
                <td class="text-center">
                    <span data-toggle="tooltip" data-nom="habitaclia" data-val="{{ $immoble->habitaclia }}" title="habitaclia" class="icon-portal-habitaclia @if ($immoble->habitaclia) text-primary @endif">H</span>
                </td>
                <td class="text-center">
                    <span data-toggle="tooltip" data-nom="reclam" data-val="{{ $immoble->reclam }}" title="reclam" class="icon-reclam @if ($immoble->reclam) text-primary @endif">R</span>
                  </td><td>  <select name="tipus_reclam">
                        <option @if($immoble->tipus_reclam == 1) selected="true" @endif value="1">Apartament</option>
                        <option @if($immoble->tipus_reclam == 2) selected="true" @endif value="2">Àtic</option>
                        <option @if($immoble->tipus_reclam == 3) selected="true" @endif value="3">Pis</option>
                        <option @if($immoble->tipus_reclam == 4) selected="true" @endif value="4">Dúplex</option>
                        <option @if($immoble->tipus_reclam == 5) selected="true" @endif value="5">Planta baixa</option>
                        <option @if($immoble->tipus_reclam == 6) selected="true" @endif value="6">Casa adossada</option>
                        <option @if($immoble->tipus_reclam == 7) selected="true" @endif value="7">Casa-xalet</option>
                        <option @if($immoble->tipus_reclam == 8) selected="true" @endif value="8">Finca rústica</option>
                        <option @if($immoble->tipus_reclam == 9) selected="true" @endif value="9">Loft</option>
                        <option @if($immoble->tipus_reclam == 10) selected="true" @endif value="10">Solar industrial</option>
                        <option @if($immoble->tipus_reclam == 11) selected="true" @endif value="11">Solar vivienda</option>
                        <option @if($immoble->tipus_reclam == 12) selected="true" @endif value="12">Terrenys</option>
                        <option @if($immoble->tipus_reclam == 13) selected="true" @endif value="13">Finques rústiques</option>
                        <option @if($immoble->tipus_reclam == 14) selected="true" @endif value="14">Masies</option>
                        <option @if($immoble->tipus_reclam == 15) selected="true" @endif value="15">Local comercial</option>
                        <option @if($immoble->tipus_reclam == 16) selected="true" @endif value="16">Traspàs</option>
                        <option @if($immoble->tipus_reclam == 17) selected="true" @endif value="17">Edifici</option>
                        <option @if($immoble->tipus_reclam == 18) selected="true" @endif value="18">Nau industrial</option>
                        <option @if($immoble->tipus_reclam == 19) selected="true" @endif value="19">Plaça pàrquing</option>
                        <option @if($immoble->tipus_reclam == 20) selected="true" @endif value="20">Torre</option>
                        <option @if($immoble->tipus_reclam == 21) selected="true" @endif value="21">Despatxos</option>
                        <option @if($immoble->tipus_reclam == 22) selected="true" @endif value="22">Oficines</option>
                    </select>
                </td>


            </tr>
        @endforeach
        </tbody>
    </table>


    <script>
        $(document).ready(function () {

            var table = $('#export').DataTable({
                "iDisplayLength": 9999
            });
        });

    </script>

@stop
