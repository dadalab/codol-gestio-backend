<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<publicacion oficina="03004">
    <referencias>
    @foreach($immobles as $immoble)
      <referencia id="{{ $immoble->id }}" timestamp="<?php echo strtotime($immoble->updated_at); ?>">
        {{-- REGIM --}}
        @if ($immoble->regim_id == 1)
            <operacion id="2">alquiler</operacion>
        @elseif ($immoble->regim_id == 2)
            <operacion id="1">compra</operacion>
        @else
            <operacion id="7">alquiler opción compra</operacion>
        @endif
        {{-- TIPUS --}}
        @if ($immoble->tipus_id == 1)
            <tipo id="11">piso</tipo>
        @elseif ($immoble->tipus_id == 2)
            <tipo id="16">casa</tipo>
        @elseif ($immoble->tipus_id == 3)
            <tipo id="28">local</tipo>
        @elseif ($immoble->tipus_id == 4)
            <tipo id="43">nave</tipo>
        @elseif ($immoble->tipus_id == 5)
            <tipo id="36">garaje</tipo>
        @elseif ($immoble->tipus_id == 6)
            <tipo id="39">solar</tipo>
        @else
            <tipo id="37">trastero</tipo>
        @endif
        <ubicacion>
            <pais>ES</pais>
            <provincia id="08"><![CDATA[Barcelona]]></provincia>
<?php
$array_ciutats = array('Torelló'=>303, 'Manlleu'=>169, 'Vic'=>314);
$nom_ciutat = $immoble->ciutat->nom;
$ciutat_id = 0;
if ( array_key_exists($nom_ciutat, $array_ciutats) ) {
    $ciutat_id = $array_ciutats[$nom_ciutat];
}
else {
    $xml = simplexml_load_file('http://atlas.yaencontre.com/api/getCities?stateId=8');
    $ciutat = $xml->xpath("//city[name='$nom_ciutat']");
    if ( count($ciutat) ) {
        $ciutat_id = $ciutat[0]->attributes()->id;
        $array_ciutats[$nom_ciutat] = $ciutat_id;
    }
}
?>
            <poblacion id="{{ $ciutat_id[0] }}"><![CDATA[{{ $immoble->ciutat->nom }}]]></poblacion>
            <zona></zona>
            <zona_libre></zona_libre>
            <cod_postal></cod_postal>
            {{-- VIA --}}
            @if ($immoble->tipus_via == "Avinguda")
                <tipo_via id="0"><![CDATA[avenida]]></tipo_via>
            @elseif ($immoble->tipus_via == "Carrer")
                <tipo_via id="2"><![CDATA[calle]]></tipo_via>
            @elseif ($immoble->tipus_via == "Carretera")
                <tipo_via id="4"><![CDATA[carretera]]></tipo_via>
            @elseif ($immoble->tipus_via == "Passeig")
                <tipo_via id="16"><![CDATA[paseo]]></tipo_via>
            @elseif ($immoble->tipus_via == "Plaça")
                <tipo_via id="10"><![CDATA[plaza]]></tipo_via>
            @elseif ($immoble->tipus_via == "Polígon")
                <tipo_via id="8"><![CDATA[polígono]]></tipo_via>
            @elseif ($immoble->tipus_via == "Rambla")
                <tipo_via id="15"><![CDATA[rambla]]></tipo_via>
            @elseif ($immoble->tipus_via == "Ronda")
                <tipo_via id="11"><![CDATA[ronda]]></tipo_via>
            @else
                <tipo_via id="2"><![CDATA[calle]]></tipo_via>
            @endif

            <direccion><![CDATA[{{ $immoble->adreca_publica }}]]></direccion>
            <direccion_num></direccion_num>
            <direccion_otra_info></direccion_otra_info>
            <direccion_privada></direccion_privada>
            <latitud>{{ $immoble->coordy }}</latitud>
            <longitud>{{ $immoble->coordx }}</longitud>
        </ubicacion>
        <precio>{{ $immoble->preu }}</precio>
        <disponibilidad></disponibilidad>
        <m2_construidos>{{ $immoble->superficie_total }}</m2_construidos>
        <m2_utiles>{{ $immoble->superficie }}</m2_utiles>
        <habitaciones>{{ $immoble->dormitoris }}</habitaciones>
        <banyos>{{ $immoble->banys }}</banyos>
        <nuevo></nuevo>
        <certificacion_energetica><![CDATA[{{ strtolower($immoble->certificat_eficiencia) }}]]></certificacion_energetica>
        <certificacion_energetica_etiqueta></certificacion_energetica_etiqueta>
        <titulos>
            <titulo idioma="ca"><![CDATA[{{ $immoble->titol }}]]></titulo>
        </titulos>
        <descripciones>
            <descripcion idioma="ca">
            <![CDATA[{{ $immoble->descripcio }}]]></descripcion>
        </descripciones>
        <caracteristicas>
            {{-- PLACES --}}
            @if ($immoble->places == 1)
                <extra id="parking_plazas">0</extra>
            @elseif ($immoble->places == 2)
                <extra id="parking_plazas">1</extra>
            @elseif ($immoble->places > 2)
                <extra id="parking_plazas">2</extra>
            @endif
            {{-- ESTAT --}}
            @if ($immoble->estat_id == 1)
                <extra id="estado">0</extra>
            @elseif ($immoble->estat_id == 3)
                <extra id="estado">1</extra>
            @elseif ($immoble->estat_id == 6)
                <extra id="estado">2</extra>
            @endif
        </caracteristicas>
        <adjuntos>
            @if (isset($immoble->imatgeDestacada->nom))
                 <adjunto tipo="foto" url="http://codolgestio.net/content/{{ $immoble->imatgeDestacada->nom }}-thumb.jpg"></adjunto>
            @endif
        </adjuntos>
      </referencia>
    @endforeach
    </referencias>
</publicacion>
