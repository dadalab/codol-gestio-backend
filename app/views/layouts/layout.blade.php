<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Còdol Gestió</title>

    <link rel="stylesheet" type="text/css" href="{{ url() }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url() }}/assets/css/custom.css?id=122">
    <link rel="stylesheet" type="text/css" href="{{ url() }}/assets/css/dropzone.css">
    <link rel="stylesheet" type="text/css" href="{{ url() }}/assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ url() }}/assets/css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ url() }}/assets/css/bootstrap-select.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ url() }}/assets/js/bootstrap.min.js"></script>
    <script src="{{ url() }}/assets/js/dropzone.js"></script>
    <script src="{{ url() }}/assets/js/custom.js?id=122"></script>
    <script src="{{ url() }}/assets/js/jquery.dataTables.min.js"></script>
    <script src="{{ url() }}/assets/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/plug-ins/f2c75b7247b/sorting/datetime-moment.js"></script>
    <script src="{{ url() }}/assets/js/bootstrap-datepicker.js"></script>
    <script src="{{ url() }}/assets/js/bootstrap-select.js"></script>
    <script src="{{ url() }}/assets/js/bootstrap-rating-input.js"></script>
    <script src="{{ url() }}/assets/js/vue.min.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

</head>
<body>
@include('common.top-menu')

@include('common.notification')

<div class="container home-content">
    <div class="col-md-12">
        @yield('content')
    </div>
</div>

<footer>
    <div class="container">
        <div class="col-md-12">CodolGestió <?php echo date('Y') ?> © - Gestors Immobiliaris</div>
    </div>
</footer>

<script>
    if (! $('.no-selectpicker').length )
        $('select').selectpicker();

    $('.datepicker').datepicker();


    $(document).ready(function () {

        var calcularF = function () {
            var L, P, n, c, dp;

            var valImport = $("#mcPrice").val();
            valImport = valImport.replace(/\./g, '');
            valImport = valImport.replace(/\,/g, '.');

            var valInteres = $("#mcRate").val();
            valInteres = valInteres.replace(/\,/g, '.');

            L = parseInt(valImport);
            n = parseInt($("#mcTerm").val()) * 12;
            c = parseFloat(valInteres) / 1200;
            //dp = 1 - parseFloat($("#mcDown").val())/100;
            //L = L * dp;
            P = (L * (c * Math.pow(1 + c, n))) / (Math.pow(1 + c, n) - 1);
            if (!isNaN(P)) {
                $("#mcPayment").val(P.toFixed(2));
            }
            else {
                $("#mcPayment").val('00.00');
            }
            return false;
        };

        //Cridem la funcio al carregar
        $("#mcPrice").keyup(function () {
            calcularF();
        });
        $("#mcTerm").keyup(function () {
            calcularF();
        });
        $("#mcRate").keyup(function () {
            calcularF();
        });

        $("#mcPrice").change(function () {
            calcularF();
        });
        $("#mcTerm").change(function () {
            calcularF();
        });
        $("#mcRate").change(function () {
            calcularF();
        });

    });

</script>
</body>
</html>
