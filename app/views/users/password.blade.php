@extends('layouts.layout')

@section('content')
    <div class="col-md-6 col-md-offset-3">

        <div class="page-header">
            <h1>Canvi de contrasenya</h1>
        </div>

        {{ Form::open() }}
        @if($errors->count())
            <div class="col-md-12">
                <div class="alert alert-danger">{{ $errors->first() }}</div>
            </div>
        @endif

        @if(Session::get('success'))
            <div class="col-md-12">
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            </div>
        @endif

        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('password', 'Contrasenya') }}
                {{ Form::password('password', ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('password_confirm', 'Repetir contrasenya') }}
                {{ Form::password('password_confirm', ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('master_password', 'Clau per canviar contrasenyes') }}
                {{ Form::password('master_password', ['class' => 'form-control']) }}
            </div>
            {{ Form::submit('Guardar contrasenya', ['class' => 'btn btn-primary form-control']) }}
        </div>
    </div>
    {{ Form::close() }}
@stop