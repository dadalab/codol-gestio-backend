@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Visites
            <small>
            </small>
        </h1>
    </div>

    <table id="visitas" class="table table-striped">
        <thead>
        <tr>
            <th>Immoble</th>
            <th>Client</th>
            <th>Comercial</th>
            <th>Data</th>
            <th>Observacions</th>
        </tr>
        </thead>
        @forelse($visites as $visita)
            <tr>
                <td>@if(isset($visita->immoble['id']))<a href="{{ URL::route('immoble.visualitzar', $visita->immoble['id']) }}">{{ $visita->immoble['adreca_immoble'] }}</a>@else - @endif</td>
                <td>@if(isset($visita->client->id)) <a href="{{ URL::route('client.visualitzar', $visita->client->id) }}">{{ $visita->client->nom }}</a>@else - @endif
                </td>
                <td>@if(isset($visita->client->id)) {{ $visita->client->comercial }} @endif</td>
                <td width="100">{{ $visita->data }}</td>
                <td>{{ $visita->observacions }}</td>
            </tr>
        @empty

        @endforelse
    </table>


    <script>
        $(document).ready(function () {
            $.fn.dataTable.moment('DD-MM-YYYY');
            $('#visitas').dataTable({
                "iDisplayLength": 50
            });
        });
    </script>
@stop