@extends('layouts.layout')

@section('content')

    {{ Form::model($immoble, array('route' => array('valoracions.actualitzar', $immoble->id))) }}
    <div class="page-header">
        <h1>Modificant valoració
            <small>
                <a href="{{ URL::route('valoracions.visualitzar', $immoble->id) }}" style="margin-left: 10px"
                   class="btn btn-default pull-right">No guardar</a>
                <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Guardar canvis</button>
                
            </small>
        </h1>
    </div>
    <div class="clearfix"></div>

    @include('common.errors')

    <div class="col-md-4">
        @if (isset($immoble->imatgeDestacada))
            <img src="http://codolgestio.net/content/{{ $immoble->imatgeDestacada->nom }}-thumb.jpg"
                 id="imatge-destacada" class="img-responsive img-rounded img-thumbnail">
        @endif
        <div class="clearfix"></div>
        <div class="col-md-12" style="font-size: 16px; margin: 10px 0;">
            <span data-name="destacat" data-value="{{ $immoble->destacat ? 1 : 0 }}" data-toggle="tooltip"
                  title="{{ $immoble->destacat ? 'destacat' : 'no destacat' }}"
                  class="glyphicon glyphicon-pushpin @if ($immoble->destacat) text-primary @endif edit-immoble-span"></span>
            <span data-name="publicat" data-value="{{ $immoble->publicat ? 1 : 0 }}" data-toggle="tooltip"
                  title="{{ $immoble->publicat ? 'publicat' : 'no publicat' }}"
                  class="glyphicon glyphicon-globe @if ($immoble->publicat) text-primary @endif edit-immoble-span"></span>
            <span data-name="oportunitat" data-value="{{ $immoble->oportunitat ? 1 : 0 }}" data-toggle="tooltip"
                  title="{{ $immoble->oportunitat ? 'oportunitat' : 'no oportunitat' }}"
                  class="glyphicon glyphicon-star @if ($immoble->oportunitat) text-primary @endif edit-immoble-span"></span>
             <span data-name="fet_a_mida" data-value="{{ $immoble->fet_a_mida ? 1 : 0 }}" data-toggle="tooltip"
                   title="{{ $immoble->fet_a_mida ? 'fet a mida' : 'no fet a mida' }}"
                   class="glyphicon glyphicon-ok-circle @if ($immoble->fet_a_mida) text-primary @endif edit-immoble-span"></span>
            <span data-name="novetat" data-value="{{ $immoble->novetat ? 1 : 0 }}" data-toggle="tooltip"
                  title="{{ $immoble->novetat ? 'novetat' : 'no novetat' }}"
                  class="glyphicon glyphicon-certificate @if ($immoble->novetat) text-primary @endif edit-immoble-span"></span>
        </div>
            {{ Form::hidden('oficina_id', Auth::user()->oficina_id) }}
        @include('immobles.form_parts.propietari')
        <div class="form-group">
            <label for="valoracio_data">Data Valoració</label>
            {{ Form::text('valoracio_data', null, array('class' => 'form-control datepicker', "data-date-format" => "dd-mm-yyyy")) }}
        </div>
        <div class="form-group">
                <label for="valoracio_codol">Valoració CòdolGestió</label>
                {{ Form::text('valoracio_codol', $immoble->valoracio_codol, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                <label for="valoracio_client">Valoració Client</label>
                {{ Form::text('valoracio_client', $immoble->valoracio_client, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                <label for="valoracio_hipoteca">Hipoteca</label>
                {{ Form::text('valoracio_hipoteca', $immoble->valoracio_hipoteca, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                <label for="valoracio_entitat">Entitat</label>
                {{ Form::text('valoracio_entitat', $immoble->valoracio_entitat, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                <label for="valoracio_comentaris">Comentaris</label>
                <textarea class="form-control" id="valoracio_comentaris" name="valoracio_comentaris">{{ $immoble->valoracio_comentaris }}</textarea>
            </div>
            <input type="hidden" name="es_valoracio" value="1">

    </div>

    <div class="col-md-8">

        @include('immobles.form_parts.principal')

        <div class="clearfix"></div>
        @include('immobles.form_parts.propietats')
    </div>

    {{ Form::close() }}

    <div class="clearfix"></div>

    {{ Form::hidden('immoble_id', $immoble->id, array('id' => 'immoble_id')) }}
    <div style="margin-top:50px" class="alert alert-danger">Recorda guardar canvis abans d'afegir imatges!</div>
    <h3>Galeria imatges de l'immoble <a href="{{ URL::route('immoble.pujar-imatges', $immoble->id) }}"
                                        class="btn btn-primary btn-lg">Afegir imatges</a></h3>

    <div class="imatges-per-triar">

        @forelse($immoble->imatges as $imatge)
            <div id="{{ $imatge->id }}"
                 class="col-xs-6 col-md-3 imatge-triar @if (isset($immoble->imatgeDestacada) && $imatge->id == $immoble->imatgeDestacada->id) destacada @endif">
                <img src="http://codolgestio.net/content/{{ $imatge->nom }}-thumb.jpg" data-id="{{ $imatge->id }}" class="img-responsive img-rounded img-thumbnail">

                <span class="glyphicon glyphicon-remove eliminar-imatge"></span>
                <span class="glyphicon glyphicon-eye-open visibilitat-imatge" data-id="{{ $imatge->es_visible }}"
                      style="color: {{ $imatge->es_visible ? 'green': 'red'}}"></span>
                <span class="glyphicon glyphicon-bookmark bookmark-destacada"></span>
            </div>
        @empty
            <p>Encara no hi ha cap imatge per aquest immoble!</p>
        @endforelse

    </div>

    <div class="clearfix"></div>
    <h3>Documents associats <a href="{{ URL::route('immoble.pujar-documents', $immoble->id) }}"
                               class="btn btn-primary btn-lg">Afegir document</a></h3>
    <div class="row">
        @forelse($immoble->documents as $document)
            <div class="col-xs-6 col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span data-id="{{ $document->id }}" class="glyphicon glyphicon-remove remove-file"></span>
                        <a href="{{ url() }}/documents/immobles/{{ $document->nom }}" target="_blank">
                            <span class="glyphicon glyphicon-file"></span>
                            {{ $document->nom }}
                        </a>
                    </div>

                </div>
            </div>
        @empty
            <p>Aquest immoble no té cap document associat.</p>
        @endforelse
    </div>


    <div class="clearfix"></div>

    <button type="button" class="btn btn-danger btn-sm pull-right" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-inbox"></span> 
        Eliminar valoració
    </button>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Tancar</span></button>
                    <h4 class="modal-title" id="myModalLabel">Eliminar valoració</h4>
                </div>
                {{ Form::open(array('route' => array('valoracio.eliminar', $immoble->id))) }}
                <div class="modal-body">
                    Estàs segur que vols eliminar aquesta valoració?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>


      

@stop

