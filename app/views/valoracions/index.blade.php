@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Llistat de valoracions
            <small><a href="{{ URL::route('valoracions.create') }}" class="btn btn-primary btn-lg pull-right">Afegir valoració</a></small>
        </h1>
    </div>

    <table class="table" id="immobles">
        <thead>
            <th>Adreça</th>
            <th>Ciutat</th>
            <th>Tipus</th>
            <th>Règim</th>
            <th class="text-right">Preu Client</th>
            <th class="text-right">Preu CòdolGestió</th>
            <th class="text-right">Data valoració</th>
            <th></th>
            </thead>
        <tbody>
        @forelse($valoracions as $valoracio)
            <tr>    
                <td>{{ $valoracio->adreca_immoble }}</td>
                <td>{{ $valoracio->ciutat->nom }}</td>
                <td>{{ $valoracio->tipus->nom }}</td>
                <td>{{ $valoracio->regim->nom }}</td>
                <td class="text-right">{{ $valoracio->valoracio_codol }} €</td>
                <td class="text-right">{{ $valoracio->valoracio_client }} €</td>
                <td class="text-right">{{ $valoracio->valoracio_data }}</td>
                <td class="text-right">
                    <a href="{{ URL::route('valoracions.visualitzar', $valoracio->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a style="margin-left:10px" href="{{ URL::route('valoracio.modificar', $valoracio->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
                </td>
            </tr>
        @empty
            <tr>
                <td>No hi ha valoracions actives</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@stop
