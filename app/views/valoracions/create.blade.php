@extends('layouts.layout')

@section('content')

    {{ Form::open(array('route' => array('valoracio.afegir'))) }}

    <div class="page-header">
        <h1>Afegint nova valoració
            <small><button type="submit" class="btn btn-primary pull-right">Guardar canvis i publicar imatges</button></small>
        </h1>
    </div>
    <div class="clearfix"></div>

    @include('common.errors')
    <div class="row">
        
        <div class="col-md-4">
            <div class="clearfix"></div>
            <p>Afegint immoble a la oficina de {{ Auth::user()->oficina->ciutat->nom }}...</p>
            {{ Form::hidden('oficina_id', Auth::user()->oficina_id) }}
            <div class="clearfix"></div>
            @include('immobles.form_parts.propietari')
            <div class="form-group">
                <label for="valoracio_data">Data Valoració</label>
                {{ Form::text('valoracio_data', null, array('class' => 'form-control datepicker', "data-date-format" => "dd-mm-yyyy")) }}
            </div>
            <div class="form-group">
                <label for="valoracio_codol">Valoració CòdolGestió</label>
                {{ Form::text('valoracio_codol', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                <label for="valoracio_client">Valoració Client</label>
                {{ Form::text('valoracio_client', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                <label for="valoracio_hipoteca">Hipoteca</label>
                {{ Form::text('valoracio_hipoteca', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                <label for="valoracio_entitat">Entitat</label>
                {{ Form::text('valoracio_entitat', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                <label for="valoracio_comentaris">Comentaris</label>
                <textarea class="form-control" id="valoracio_comentaris" name="valoracio_comentaris"></textarea>
            </div>
            <input type="hidden" name="es_valoracio" value="1">
        </div>

        <div class="col-md-8">
            @include('immobles.form_parts.principal')
            <div class="clearfix"></div>
            @include('immobles.form_parts.propietats')
        </div>

    </div>
    {{ Form::close() }}

@stop