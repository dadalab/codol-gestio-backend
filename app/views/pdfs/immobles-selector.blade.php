@extends('layouts.layout')

@section('content')
	<style>
		#immobles_length, #immobles_paginate {
			display: none;
		}
	</style>

	<div id="generar-pdf">
	<form method="GET" v-on="submit : formSubmit($event)" action="{{ route('pdfs.generate')}}">
	    <div class="page-header">
	        <h1>Generar PDF
	            <small>
	                <button type="submit" class="btn btn-primary btn-lg pull-right">Continuar</button>
	            </small>
	        </h1>
	    </div>
		
	    <div class="col-md-12">
	    	
	    	<b>Selecciona 1 o 4 immobles</b>
	    	
	    	<div id="alert-error" class="alert alert-danger" style="display:none">
				Has de seleccionar 1 o 4 immobles!
	    	</div>

	    	<table class="table" id="immobles">
		        <thead>
			        <th>Expedient</th>
			        <th>Adreça</th>
			        <th>Ciutat</th>
			        <th>Tipus</th>
			        <th>Règim</th>
			        <th></th>
		        </thead>
		        <tbody>
		        @foreach($immobles as $immoble)
		            <tr>
		                <td>{{ $immoble->num_expedient }}</td>
		                <td>{{ $immoble->adreca_immoble }}</td>
		                <td>{{ $immoble->ciutat->nom }}</td>
		                <td>{{ $immoble->tipus->nom }}</td>
		                <td>{{ $immoble->regim->nom }}</td>
		                <td><input name="immobles[]" value="{{ $immoble->id }}" type="checkbox"></td>
		            </tr>
		        @endforeach
		        </tbody>
	    	</table>
		</div>
	</div>


	<script>
		$.fn.dataTable.moment( 'DD-MM-YYYY' );
            var table = $('#immobles').DataTable({
                "iDisplayLength": 999
            });
    
	var tvsvue = new Vue({
		el: '#generar-pdf',

		methods: {
			formSubmit: function(e) {
				$('#alert-error').hide();
				var immobles = 0
				$('input[name="immobles[]"]').each(function () {
				 	immobles += (this.checked) ? 1 : 0;
				});

				if (immobles != 1 && immobles != 4)
				{
					e.preventDefault();
					$('#alert-error').fadeIn();
				}				
			}
		},

	});

    </script>
@stop