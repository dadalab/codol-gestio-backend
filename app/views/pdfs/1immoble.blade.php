<html>
	<head>
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700"><style type="text/css">.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px}</style><style type="text/css">@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style type="text/css">.gm-style{font-family:Roboto,Arial,sans-serif;font-size:11px;font-weight:400;text-decoration:none}.gm-style img{max-width:none}</style>
    <title>Impressió PDF | Còdol Gestió</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" id="open-sans-css" href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=4.0" type="text/css" media="all">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/css/bootstrap.min.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/style.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/custom.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/print.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="{{ url() }}/assets/js/vue.min.js"></script>

    <style>
    	.imprimir_wrapper {
    		padding-bottom: 0;
    	}

		.titular {
			background-color: #d3460e !important;
			font-size: 28px;
			color: #fff;
			border-radius: 10px;
			padding: 15px;
			font-style: italic;
			font-weight: bold;
			font-family: 'PT-Serif';
		}

		.entry-title {
			font-size: 22px;
			line-height: 22px;
			padding-top: 20px;
			font-style: italic;
			font-weight: bold;
			max-width: 315px;
			color: #d3460e !important;
		}

		.immoble-partial {
			margin-top: 15px;
			border: 1px solid #706f6f;
			overflow: hidden;
			border-radius: 10px;
			padding: 0;
		}

		.img-responsive {
			margin: 0;
		}

		.preu {
			color: #d3460e !important;
			font-size: 22px;
			margin: 10px 0;
			font-weight: bold;
		}
		.topos {
			margin: 20px 0 0 0;
		}
		.imatge-destacada {
			border-bottom: 6px solid #d3460e;
			background-size:cover;
			height: 550px;
		}
		.imatge-esquerra {
			border-right: 3px solid #d3460e;
			border-bottom: 6px solid #d3460e;
		}
		.imatge-dreta {
			border-left: 3px solid #d3460e;
			border-bottom: 6px solid #d3460e;
		}

		.etiqueta-immoble {	
			position: absolute;
		    z-index: 1;
		    background: #d3460e;
		    height: 35px;
		    line-height: 35px;    
		    color: #fff;
		    text-align: center;
			border-radius: 4px 4px 0 0;
			width: 100%;
			font-size: 18px;
			font-weight: bold;
		}

		.topo {
			width: 50px;
			height: 50px;
			font-size: 12px;
		}

		input, textarea {
			width: 100%;
		}
		
		#photoSelector {
			width: 150px;
			position: fixed;
			left:0;
			top:0;
			overflow-y: scroll; 
			height: 100%;
		}
		#photoSelector img {
			border-bottom: 1px solid white;
		}
		img {
			cursor: pointer;
		}

		.anys {
			text-align: left;
			font-size: 18px;
			text-transform: uppercase;
			line-height: 22px;
			font-weight: bold;
			margin-left:40px;
			color: #fff;
			background-color: #000;
			display: inline-block;
			margin-bottom: 2px;
			padding: 2px 4px;
		}
    </style>
<body style="background: white" id="print">

<div class="container content_wrapper imprimir_wrapper">
    <div class="row">
    	<a href="{{ route('pdfs') }}" style="position:absolute;top:0;left:30px;" class="hidden-print">Tornar</a>
		<button style="position:absolute;top:0;right:30px;padding:10px" class="hidden-print" onclick="window.print()">Imprimir</button>
        <div class="col-xs-4" style="padding:0"><img style="max-width:300px!important" src="{{ url() }}/images/logo-gran.jpg"></div>
        <div class="col-xs-4">
        	<span class="anys" style="text-align:left">Més de 40 anys</span><br>
        	<span class="anys">al teu servei!</span>
    	</div>
        <div class="col-xs-4 text-right" style="padding:0">
			<p style="font-size:15px;">{{ Auth::user()->oficina->ciutat->nom }}<br>
		    {{ Auth::user()->oficina->adreca }}<br>
		    T. {{ Auth::user()->oficina->telefon }}<br>
		    <span style="text-decoration: underline">www.codolgestio.net</span><br>
		</div>
		<div class="clearfix"></div>
		
		<div class="col-md-12 titular">
			<div id="documentTitle" v-on="click: changeTitle('documentTitle')" v-text="documentTitle"></div>
			<input id="documentTitleInput" type="text" v-model="documentTitle" v-on="keyup:changeTitle('documentTitle') | key 'enter'" value="Selecció de cases en venda" style="display:none; width:100%">
		</div>
		
		@foreach($immobles as $index => $immoble)
		<div class="col-md-12 immoble-partial">
				<div class="imatge-destacada" id="img1" v-on="click : setPosition(1)" style="background-image: url('http://codolgestio.net/content/{{ $immoble->imatgeDestacada->nom }}.jpg');">
				@if ($immoble->novetat || $immoble->oportunitat)
					<div class="etiqueta-immoble">
						{{ $immoble->oportunitat ? 'Oh!portunitat' : ''}}
						{{ ($immoble->novetat && $immoble->oportunitat) ? '|' : '' }} 	
						{{ $immoble->novetat ? 'Novetat' : '' }}
					</div>
            	@endif
            	</div>
			<div class="col-xs-6" style="padding: 0">
            	<img src="http://codolgestio.net/content/{{ $immoble->imatges->first()->nom }}.jpg" v-on="click : setPosition(2)" id="img2" class="img-responsive imatge-esquerra">
			</div>
			<div class="col-xs-6" style="padding:0">
            	<img src="http://codolgestio.net/content/{{ $immoble->imatges->get(1)->nom }}.jpg" v-on="click : setPosition(3)" id="img3" class="img-responsive imatge-dreta">
			</div>

	        <div class="col-xs-4">
	            <h1 class="entry-title pull-left" id="title{{ $index }}" v-on="click: changeTitle('title{{ $index }}')" v-text="title{{ $index }}"></h1>
	            <input type="text" id='title{{ $index }}Input' value="{{ $immoble->titol }}" v-model="title{{ $index }}" v-on="keyup:changeTitle('title{{ $index }}') | key 'enter'" style="display:none">

            	<div class="clearfix"></div>
	            <h2 class="preu" id="preu{{ $index }}" v-on="click: changeTitle('preu{{ $index }}')" v-text="preu{{ $index }}"></h2>
	            <input type="text" id='preu{{ $index }}Input' value="{{ $immoble->getNumberFormat('preu') }} € @if($immoble->quota > 0) | {{ $immoble->getNumberFormat('quota') }}€/mes @endif" v-model="preu{{ $index }}" v-on="keyup:changeTitle('preu{{ $index }}') | key 'enter'" style="display:none">
	            
	            <div class="property_location" style="margin:10px 0 20px 0">
	            	<span class="inforoom" style="color: #d3460e;font-size:15px;background-position: 0px 5px;">{{ $immoble->dormitoris }} </span> 
	            	<span class="infobath" style="color: #d3460e;font-size:15px;background-position: 0px 4px;">{{ $immoble->banys }}</span> 
	            	<span class="infosize" style="color: #d3460e;font-size:15px;background-position: 0px 4px;">{{ $immoble->superficie}} m<sup>2</sup></span>        
	        	</div>
	            <p><b>{{ $immoble->adreca_publica }} - {{ $immoble->ciutat->nom }}</b></p>
            </div>
        	<div class="col-xs-6" style="padding-top: 20px;line-height: 22px">  
        		<span id="descripcio{{ $index }}" v-text="descripcio{{ $index }}" v-on="click: changeTitle('descripcio{{ $index }}')" style="font-size: 15px;line-height: 17px;"></span>
            	<textarea id='descripcio{{ $index }}Input' v-model="descripcio{{ $index }}" style="display:none">{{ nl2br($immoble->descripcio) }}</textarea>
				<button class="descripcio{{ $index }}Input" v-on="click:changeTitle('descripcio{{ $index }}')" style="display:none">OK</button>
        	</div>
	        <div class="col-xs-2">     

	            <div class="topos pull-right">  
	            	@if ($immoble->financament ) 
		     			<div class="topo financament" style="background: #424242">
		     				<div class="contingut"><b>100%</b> FINAN.</div>
		 				</div>      
	 				@endif
	 				@if ($immoble->estat_id == 1)
	 					<div class="topo obra-nova">
	 						<div class="contingut">Obra nova</div>
						</div> 
					@endif
				</div>
        	</div>
    	</div>
        @endforeach
	</div>
        
    </div>
</div>

<div id="photoSelector" class="hidden-print">
	@foreach($immoble->imatges as $imatge)
		<img src="http://codolgestio.net/content/{{ $imatge->nom }}.jpg" v-on="click : setImage('http://codolgestio.net/content/{{ $imatge->nom }}.jpg')" class="img-responsive">
	@endforeach
</div>

<script>
	
new Vue({
  el: '#print',
  	data: {
  		position: 0
  	},
  	methods: {
	  	changeTitle: function(nomCamp)
	  	{
	  		$('#' + nomCamp + 'Input').toggle().focus;
	  		$('.' + nomCamp + 'Input').toggle();
	  		$('#' + nomCamp).toggle();
	  	},
	  	setPosition: function(n)
		{
			this.position = n;
		},
		setImage:function(url)
		{
			if (this.position > 1)
				$('#img' + this.position).attr('src', url);
			else if (this.position == 1)
				$('#img' + this.position).css('background-image', 'url(' + url + ')')
		}
	}	
});


</script>

</body>
</html>