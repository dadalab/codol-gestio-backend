<html>
<head>

    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <style type="text/css">
        @media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style {
            font-family: Roboto, Arial, sans-serif;
            font-size: 11px;
            font-weight: 400;
            text-decoration: none
        }

        .gm-style img {
            max-width: none
        }</style>
    <title></title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" id="open-sans-css" href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=4.0" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="{{ url() }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/style.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/custom.css">
    <link rel="stylesheet" media="all" href="http://codolgestio.net/wp-content/themes/codolgestio/print.css">
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/map.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/marker.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/onion.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/stats.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/controls.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://mt1.googleapis.com/vt?pb=!1m4!1m3!1i17!2i66356!3i48689!1m4!1m3!1i17!2i66357!3i48689!1m4!1m3!1i17!2i66356!3i48690!1m4!1m3!1i17!2i66357!3i48690!1m4!1m3!1i17!2i66358!3i48689!1m4!1m3!1i17!2i66358!3i48690!2m3!1e0!2sm!3i302170446!3m9!2sca-ES!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e3&amp;callback=_xdc_._r4n0l&amp;token=91869"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps/api/js/AuthenticationService.Authenticate?1shttp%3A%2F%2Fcodolgestio.net%2Fdetall%2Fpis-venda-vic-523%2F%3Fimprimir%3D1&amp;5e1&amp;callback=_xdc_._s8d9k8&amp;token=42708"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps/api/js/QuotaService.RecordEvent?1shttp%3A%2F%2Fcodolgestio.net%2Fdetall%2Fpis-venda-vic-523%2F%3Fimprimir%3D1&amp;4e1&amp;5e0&amp;6u1&amp;7s5wrdxt&amp;callback=_xdc_._6hh7kg&amp;token=66839"></script>
</head>
<body>

<div class="container content_wrapper imprimir_wrapper">
    <div class="row immoble">
        <div class="col-xs-12"><img src="http://codolgestio.net/wp-content/uploads/2014/09/codol-gestio-logotip.png"></div>

        <div class="col-xs-6" style="margin-top: 30px">
            <h2>Dades de l'immoble</h2>

            <table class="table">
                <tr>
                    <td>Títol</td>
                    <td>{{ $immoble->titol }}</td>
                </tr>
                <tr>
                    <td>Adreça</td>
                    <td>{{ $immoble->tipus_via}} {{ $immoble->adreca_immoble }}</td>
                </tr>
                <tr>
                    <td>Ciutat</td>
                    <td>{{ $immoble->ciutat->nom }}</td>
                </tr>
                <tr>
                    <td>Tipus</td>
                    <td>{{ $immoble->tipus->nom }}</td>
                </tr>
                <tr>
                    <td>Règim</td>
                    <td>{{ $immoble->regim->nom }}</td>
                </tr>
                <tr>
                    <td>Estat</td>
                    <td>{{ $immoble->estat->nom }}</td>
                </tr>
                <tr>
                    <td>Superfície</td>
                    <td>{{ $immoble->superficie }}m<sup>2</sup></td>
                </tr>
                <tr>
                    <td>Propietari</td>
                    <td>{{ $immoble->propietari->nom }} {{ $immoble->propietari->cognoms }}</td>
                </tr>
                <tr>
                    <td>Telèfon propietari</td>
                    <td>{{ $immoble->propietari->telefon }}</td>
                </tr>
                <tr>
                    <td>Email Propietari</td>
                    <td>{{ $immoble->propietari->email }} {{ $immoble->propietari->email }}</td>
                </tr>

            </table>
        </div>
        <!-- end 6col container-->

        <div class="col-xs-6" style="margin-top: 30px">
            <h2>Visites a l'immoble</h2>
            @foreach($immoble->visites as $visita)
                <p>{{ $visita->immoble->adreca_immoble }}</p>
                <p>{{ $visita->data->format("d/m/Y") }}</p>
                <p>{{ $visita->observacions }}</p>
                <hr>
            @endforeach



        </div>
        <!-- end 6col container-->

    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="http://maps.gstatic.com/maps-api-v3/api/js/21/0/intl/ca_ALL/main.js"></script>


</body>
</html>