@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Afegir imatges a l'immoble
            <small><a href="{{ URL::route('immoble.visualitzar', $id) }}" id="finalitzar"
                      class="btn btn-primary btn-lg pull-right">Finalitzar</a></small>
        </h1>
    </div>
    <div class="clearfix"></div>
    {{ Form::open(array('files' => true, 'class' => 'dropzone')) }}
    @foreach($images as $image)

        <!-- PENDENT! -->

    @endforeach
    {{ Form::close() }}
    </div>

    <script>
        $(function () {
            $(".dropzone").sortable({
                items: '.dz-preview',
                cursor: 'move',
                opacity: 0.5,
                containment: 'parent',
                distance: 20,
                tolerance: 'pointer',

            });
        });

        $('#finalitzar').click(function (e) {


            console.log('lele');
        });
    </script>
@stop