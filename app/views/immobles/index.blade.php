@extends('layouts.layout')

@section('content')

    <div class="page-header hidden-print">
        <h1>Llistat d'immobles <small>
                <a href="{{ URL::route('immoble.afegir') }}" class="btn btn-primary btn-lg pull-right">Afegir immoble</a>
                <a href="{{ URL::route('immobles.arxivats') }}" style="margin-right:10px" class="btn btn-primary btn-lg pull-right">Arxivats</a>
            </small></h1>
    </div>
    <a href="{{ URL::route('immobles') }}" class="pull-right hidden-print"><span class="glyphicon glyphicon-th-list"></span> </a>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-3 col-xs-3">
            <select class="selectpicker" id="filtre-immoble">
                <optgroup label="General">
                  <option value="all">Tots els immobles</option>
                  <option value="{{ Auth::user()->oficina->id }}">Immobles de la meva oficina</option>
                </optgroup>
                <optgroup label="Oficines">
                    @foreach($oficines as $oficina)
                        <option value="{{ $oficina->ciutat_id }}">{{ $oficina->ciutat->nom }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
        <div class="col-md-3 col-xs-3">
            <select class="selectpicker" id="regims">
                <option value="Tots">Règim: tots</option>
                @foreach($regims as $regim)
                    <option value="{{ $regim->id }}">{{ $regim->nom }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3 col-xs-3">
            <select class="selectpicker" id="tipus">
                <option value="Tots">Tipus: tots</option>
                @foreach($tipus as $tipu)
                    <option value="{{ $tipu->id }}">{{ $tipu->nom }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3 col-xs-3">
            <select class="selectpicker" id="estats">
                <option value="Tots">Estats: tots</option>
                @foreach($estats as $estat)
                    <option value="{{ $estat->id }}">{{ $estat->nom }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="clearfix" style="height: 30px"></div>
    <div class="row">
    @foreach($immobles as $immoble)
        <div class="col-md-3 col-sm-4 col-xs-4 grid-pisos oficina{{ $immoble->oficina_id }} @if (Auth::user()->oficina->id != $immoble->oficina_id) no-meva @endif tipus{{ $immoble->tipus_id }} regim{{ $immoble->regim_id }} estat{{ $immoble->estat_id }}">
            <div class="media">
                <a href="{{ URL::route('immoble.visualitzar', $immoble->id) }}">
                    @if (isset($immoble->imatgeDestacada->nom))
                        <img src="http://codolgestio.net/content/{{ $immoble->imatgeDestacada->nom }}-thumb.jpg" class="img-responsive">
                        <!--<img src="{{ url() }}/imatges/immobles/{{ $immoble->imatgeDestacada->nom }}-thumb.jpg" class="img-responsive">-->
                        @else
                        <div style="height: 168px"></div>
                    @endif
                </a>
                <div class="media-body">
                    <a href="{{ URL::route('immoble.visualitzar', $immoble->id) }}"><h4 class="media-heading">{{ $immoble->tipus_via }} {{ $immoble->adreca_immoble }}</h4></a>
                    <p>Règim: {{ $immoble->regim->nom }}<br>
                    Tipus: {{ $immoble->tipus->nom }}<br>
                    Expedient: {{ $immoble->num_expedient }}<br>
                    Preu: {{ $immoble->getNumberFormat('preu') }} €<br>
                    Ciutat: {{ $immoble->ciutat->nom }}</p>
                </div>
                <div class="media-bottom">
                    <span data-toggle="tooltip"  title="{{ $immoble->destacat ? 'destacat' : 'no destacat' }}" class="glyphicon glyphicon-pushpin @if ($immoble->destacat) text-primary @endif"></span>
                    <span data-toggle="tooltip"  title="{{ $immoble->publicat ? 'publicat' : 'no publicat' }}" class="glyphicon glyphicon-globe @if ($immoble->publicat) text-primary @endif"></span>
                    <span data-toggle="tooltip"  title="{{ $immoble->oportunitat ? 'oportunitat' : 'no oportunitat' }}" class="glyphicon glyphicon-star @if ($immoble->oportunitat) text-primary @endif"></span>
                    <span data-toggle="tooltip" title="{{ $immoble->novetat ? 'novetat' : 'no novetat' }}" class="glyphicon glyphicon-certificate @if ($immoble->novetat) text-primary @endif"></span>
                    <span data-toggle="tooltip" data-nom="fotocasa" data-val="{{ $immoble->fotocasa }}" title="fotocasa" class=" pull-right icon-portal @if ($immoble->fotocasa) text-primary @endif" style="margin-top:2px;margin-right:5px">fc</span>

                </div>
            </div>
        </div>
    @endforeach
    </div>

    <script>$('[data-toggle="tooltip"]').tooltip({'placement': 'top'});</script>

    <script>
        $('select').change( function() {

            var value = $('#filtre-immoble').val();
            var regims = $('#regims').val();
            var estats = $('#estats').val();
            var tipus = $('#tipus').val();
            console.log(tipus);
            $('.grid-pisos').hide();

            /*if (regims == 'Tots' && estats == 'Tots' && tipus == 'tots')
                $('.grid-pisos').fadeIn();
            else if (estats == 'Tots')
                $('.grid-pisos').filter('.regim' + regims).show();
            else if (regims == 'Tots')
                $('.grid-pisos').filter('.estat' + estats).show();
            else
                $('.grid-pisos').filter('.regim' + regims + '.estat' + estats).show();
*/
            if (regims == 'Tots' && estats == 'Tots' && tipus == 'Tots') $('.grid-pisos').fadeIn();
            else if (regims != 'Tots' && estats != 'Tots' && tipus != 'Tots') $('.grid-pisos').filter('.regim' + regims + '.estat' + estats + '.tipus' + tipus).show();
            else if (regims != 'Tots' && estats != 'Tots') $('.grid-pisos').filter('.regim' + regims + '.estat' + estats).show();
            else if (regims != 'Tots' && tipus != 'Tots') $('.grid-pisos').filter('.regim' + regims + '.tipus' + tipus).show();
            else if (estats != 'Tots' && tipus != 'Tots') $('.grid-pisos').filter('.estat' + estats + '.tipus' + tipus).show();
            else if (estats != 'Tots') $('.grid-pisos').filter('.estat' + estats).show();
            else if (tipus != 'Tots') $('.grid-pisos').filter('.tipus' + tipus).show();
            else if (regims != 'Tots') $('.grid-pisos').filter('.regim' + regims).show();

            //if (estats != 'Tots') $('.grid-pisos').filter('.estat' + estats).show();
            //if (tipus != 'Tots') $('.grid-pisos').filter('.tipus' + tipus).show();

            if (value != 'all')
            {
                $('.grid-pisos').not('.oficina' + value).hide();
            }
        });
    </script>
@stop
