@foreach ($oficines as $oficina)
    <div class="radio"><label>{{ Form::radio('oficina_id', $oficina->id ) }} {{ $oficina->ciutat->nom }}</label></div>
@endforeach

<div class="form-group">
	<label for="num_expedient">Número Expedient</label>
	{{ Form::text('num_expedient', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	<label for="titol">Títol</label>
	{{ Form::text('titol', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	<label for="descripcio">Descripció</label>
	<textarea class="form-control" id="descripcio" name="descripcio"></textarea>
</div>

<div class="col-md-6">
    <div class="form-group">
	    <label for="tipus_id">Tipus</label>
        {{ Form::select('tipus_id', $tipus, null, array('class' => 'form-control')) }}
	</div>
</div>

<div class="col-md-6">
    <div class="form-group">
	    <label for="regim_id">Règim</label>
	    {{ Form::select('regim_id', $regims, null, array('class' => 'form-control')) }}
	</div>
</div>

<div class="col-md-6">
    <div class="form-group">
	    <label for="estat_id">Estat</label>
		{{ Form::select('estat_id', $estats, null, array('class' => 'form-control')) }}
	</div>
</div>

<div class="col-md-6">
    <div class="form-group">
	    <label for="ciutat_id">Ciutat</label>
		{{ Form::select('ciutat_id', $ciutats, null, array('class' => 'form-control')) }}
	</div>
</div>

<div class="col-md-6">
    <div class="form-group">
	    <label for="preu">Preu</label>
	    <div class="input-group">
	        {{ Form::text('preu', null, array('class' => 'form-control')) }}
			<span class="input-group-addon">€</span>
		</div>
	</div>
</div>

<div class="col-md-6">
    <div class="form-group">
	    <label for="quota">Quota</label>
        {{ Form::text('quota', null, array('class' => 'form-control')) }}
        <p>Queda pendent el calcul</p>
    </div>
</div>

<div class="form-group">
    <label for="adreca">Adreça</label>
    <textarea class="form-control" id="adreca" name="adreca"></textarea>
</div>

<div class="col-md-4">
    <div class="form-group">
	    <label for="superficie">Superfície</label>
		<div class="input-group">
		    {{ Form::text('superficie', null, array('class' => 'form-control')) }}
			<span class="input-group-addon">m2</span>
		</div>
	</div>
</div>

<div class="col-md-4">
    <div class="form-group">
	    <label for="dormitoris">Dormitoris</label>
	    {{ Form::text('dormitoris', null, array('class' => 'form-control')) }}
	</div>
</div>

<div class="col-md-4">
    <div class="form-group">
	    <label for="banys">Banys</label>
	    {{ Form::text('banys', null, array('class' => 'form-control')) }}
    </div>
</div>