<div style="height: 20px"></div>
<div class="col-md-6">
    <ul class="list-group edit-list-group selects">

        <li class="list-group-item">
            <div class="col-md-7 row"><b>Tipus:</b></div>
            <div class="col-md-5 pull-right">{{ Form::select('tipus_id', $tipus, null, array('class' => 'form-control')) }}</div>
        </li>

        <li class="list-group-item">
            <div class="col-md-7 row"><b>Règim:</b></div>
            <div class="col-md-5 pull-right">{{ Form::select('regim_id', $regims, null, array('id' => 'vendailloguer', 'class' => 'form-control')) }}</div>
        </li>

        <li class="list-group-item">
            <div class="col-md-7 row"><b>Estat:</b></div>
            <div class="col-md-5 pull-right">{{ Form::select('estat_id', $estats, null, array('class' => 'form-control')) }}</div>
        </li>

        <li class="list-group-item">
            <div class="col-md-7 row"><b>Certificat eficiència energètica:</b></div>
            <div class="col-md-5 pull-right">{{ Form::select('certificat_eficiencia', array('No' => 'No', 'En tràmit' => 'En tràmit', 'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F', 'G' => 'G'), null, array('class' => 'form-control')) }}</div>
        </li>
        <li class="list-group-item">
            <div class="col-md-7 row">Cèdula Habitabilitat:</div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('cedula_habitabilitat', null, array('class' => 'form-control text-right')) }}</div>
        </li>
        <li class="list-group-item">

            <span data-name="llum" @if(isset($immoble->llum)) data-value="{{ $immoble->llum ? 1 : 0 }}"
                  @endif class="label label-default @if(isset($immoble->llum) && $immoble->llum) label-primary @endif edit-immoble-span" >Llum</span>
            <span data-name="aigua" @if(isset($immoble->aigua)) data-value="{{ $immoble->aigua ? 1 : 0 }}"
                  @endif class="label label-default @if(isset($immoble->aigua) && $immoble->aigua) label-primary @endif edit-immoble-span">Aigua</span>
            <span data-name="gas" @if(isset($immoble->gas)) data-value="{{ $immoble->gas ? 1 : 0 }}"
                  @endif class="label label-default @if(isset($immoble->gas) && $immoble->gas) label-primary @endif edit-immoble-span">Gas</span>
        </li>
        <li class="list-group-item">
            Publicitat:
            {{ Form::textarea('publicitat', null, ['class' => 'form-control']) }}
        </li>
    </ul>
</div>

<div class="col-md-6">
    <ul class="list-group edit-list-group selects">
        <li class="list-group-item">
            <div class="col-md-7 row"><b>Superfície:</b></div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('superficie', null, array('class' => 'form-control text-right')) }}</div>
        </li>
        <li class="list-group-item">
            <div class="col-md-7 row"><b>Dormitoris:</b></div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('dormitoris', null, array('class' => 'form-control text-right')) }}</div>
        </li>
        <li class="list-group-item">
            <div class="col-md-7 row"><b>Banys:</b></div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('banys', null, array('class' => 'form-control text-right')) }}</div>
        </li>
        <li class="list-group-item" style="min-height: 10px;"></li>
        <li class="list-group-item">
            <div class="col-md-7 row">Rètol penjat:</div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('retol_penjat', null, ['class' => 'form-control text-right datepicker', "data-date-format" => "dd-mm-yyyy"]) }}</div>
        </li>
        <li class="list-group-item">
            <div class="col-md-7 row">Finançament:</div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::checkbox('financament', '1', null, array('class' => 'form-control text-right')) }}</div>
        </li>
        <li class="list-group-item">
            <div class="col-md-7 row"><b>Preu:</b></div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('preu', null, array('class' => 'form-control text-right', 'id' => 'mcPrice')) }}</div>
        </li>
        <li class="list-group-item" style="display: {{ isset($immoble) && $immoble->regim_id == 3 ? 'block': 'none'}}">
            <div class="col-md-7 row"><b>Preu lloguer:</b></div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('preu_lloguer', null, array('id' =>'preu_lloguer', 'class' => 'form-control text-right')) }}</div>
        </li>
        <li class="list-group-item">
            <div class="col-md-3 row">Anys:</div>
            <div class="col-md-4 float-label-control"> {{ Form::text('anys', null, array('class' => 'form-control text-right', 'id' => 'mcTerm')) }}</div>
            <div class="col-md-3 row">Interès:</div>
            <div class="col-md-4 float-label-control"> {{ Form::text('interes', null, array('class' => 'form-control text-right', 'id' => 'mcRate')) }}</div>
        </li>
        <li class="list-group-item">
            <div class="col-md-7 row"><b>Quota:</b></div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('quota', null, array('class' => 'form-control text-right', 'id' => 'mcPayment')) }}</div>
        </li>
    </ul>
    <ul class="list-group edit-list-group selects extres 5" style="@if(isset($immoble) && $immoble->tipus_id == 5) display:block @else display: none @endif">
        <li class="list-group-item">
            <div class="col-md-7 row">Places:</div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('places', null, array('id' =>'places', 'class' => 'form-control text-right')) }}</div>
        </li>
    </ul>

    <ul class="list-group edit-list-group selects extres 4" style="@if(isset($immoble) && $immoble->tipus_id == 4) display:block @else display: none @endif">
        <li class="list-group-item">
            <div class="col-md-7 row">Superfície Total:</div>
            <div class="col-md-5 float-label-control pull-right"> {{ Form::text('superficie_total', null, array('id' =>'superficie_total', 'class' => 'form-control text-right')) }}</div>
        </li>
    </ul>
</div>
<div class="clearfix"></div>
<div class="col-md-2 row"><b>Etiquetes:</b></div>
<div class="col-md-10 pull-right">{{ Form::select('tags[]', $tags, isset($immoble) ? $immoble->tags->lists('id') : null, array('class' => 'form-control', 'multiple' => 'multiple')) }}</div>

<script>


</script>
