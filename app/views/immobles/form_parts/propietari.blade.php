<h4>Propietari</h4>


<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                   aria-controls="collapseOne">
                    Propietari existent
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
             aria-labelledby="headingOne">
            <div class="panel-body">
                {{ Form::select('propietari_id', withEmpty($propietaris), null, array('class' => 'form-control', 'data-live-search', 'true')) }}              </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                   aria-expanded="false" aria-controls="collapseTwo">
                    Nou propietari
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <div class="form-group">
                    <label for="nom">Nom</label>
                    {{ Form::text('nom', null,  array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    <label for="cognoms">Cognoms</label>
                    {{ Form::text('cognoms', null,  array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    <label for="telefon">Telèfon</label>
                    {{ Form::text('telefon', null,  array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    <label for="nom2">Nom 2</label>
                    {{ Form::text('nom2', null,  array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    <label for="cognoms2">Cognoms 2</label>
                    {{ Form::text('cognoms2', null,  array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    <label for="telefon2">Telèfon 2</label>
                    {{ Form::text('telefon2', null,  array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    <label for="nif">NIF</label>
                    {{ Form::text('nif', null,  array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    <label for="adreca_immoble">Adreça</label>
                    {{ Form::textarea('adreca', null,  array('class' => 'form-control', 'rows' => '2')) }}
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    {{ Form::text('email', null,  array('class' => 'form-control')) }}
                </div>


            </div>
        </div>
    </div>
</div>