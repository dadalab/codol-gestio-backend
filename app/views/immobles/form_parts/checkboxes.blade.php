<div class="form-group">
	<label for="certificat_eficiencia">Certificat eficiència energètica</label>
	{{ Form::select('certificat_eficiencia',
	    array('A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F', 'G' => 'G', 'En tràmit' => 'En tràmit'),
	    null, array('class' => 'form-control')) }}
</div>


<div class="checkbox">
	<label>
		<input type="checkbox" name="financament" value="">100% finançament
	</label>
</div>

<div class="checkbox">
	<label>
		{{ Form::checkbox('oportunitat', 'oportunitat') }} Oportunitat
	</label>
</div>

<div class="checkbox">
	<label>
		{{ Form::checkbox('destacat', 'destacat') }} Destacat
	</label>
</div>

<div class="checkbox">
	<label>
		{{ Form::checkbox('novetat', 'novetat') }} Novetat
	</label>
</div>

<div class="checkbox">
	<label>
	    {{ Form::checkbox('cedula_habitabilitat', 'cedula_habitabilitat') }} Cèdula habitabilitat
	</label>
</div>

<div class="checkbox">
	<label>
		{{ Form::checkbox('hipoteca', 'hipoteca') }} Hipoteca
	</label>
</div>

<div class="checkbox">
	<h3>Subministres</h3>
	<label>{{ Form::checkbox('llum', 'llum') }} Llum</label>
	<label>{{ Form::checkbox('aigua', 'aigua') }} Aigua</label>
	<label>{{ Form::checkbox('gas', 'gas') }} Gas</label>
</div>