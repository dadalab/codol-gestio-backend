<div class="col-md-6">
    <div class="form-group float-label-control label-bottom">
        {{ Form::label('titol', 'Títol', ['style' => 'font-weight:700']) }}
        {{ Form::text('titol', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group float-label-control label-bottom">
        {{ Form::label('ref_cadastral', 'Referència cadastral') }}
        {{ Form::text('ref_cadastral', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group float-label-control label-bottom">
        {{ Form::label('ref_externa', 'Referència externa') }}
        {{ Form::text('ref_externa', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group float-label-control label-bottom">
        {{ Form::label('inscripcio_registre', 'Inscripció registre') }}
        {{ Form::text('inscripcio_registre', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group float-label-control label-bottom">
        {{ Form::label('claus', 'Claus') }}
        {{ Form::text('claus', null, array('class' => 'form-control')) }}
    </div>
    <div style="height: 20px"></div>
    <div class="form-group float-label-control ">
        {{ Form::label('descripcio', 'Descripció', ['style' => 'font-weight:700']) }}
        {{ Form::textarea('descripcio', null, array('class' => 'form-control', 'rows' => 4)) }}
    </div>
    <div class="form-group float-label-control ">
        {{ Form::label('text_destacat', 'Text destacat', ['style' => 'font-weight:700']) }}
        {{ Form::textarea('text_destacat', null, array('class' => 'form-control', 'rows' => 4)) }}
        <span id="destacat_count"></span>
    </div>
    <div class="form-group float-label-control label-bottom">
        {{ Form::label('video_youtube', 'Video Youtube', ['style' => 'font-weight:700']) }}
        {{ Form::text('video_youtube', null, array('class' => 'form-control')) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('tipus_via', 'Tipus Via', ['style' => 'font-weight:700']) }}
        {{ Form::select('tipus_via',
        ['Carrer' => 'Carrer', 'Plaça' => 'Plaça', 'Avinguda' => 'Avinguda', 'Passeig' => 'Passeig', 'Urbanització' => 'Urbanització', 'Camí' => 'Camí', 'Carretera' => 'Carretera', 'Ronda' => 'Ronda', 'Mas' => 'Mas', 'Passatge' => 'Passatge', 'Polígon Industrial' => 'Polígon Industrial', 'Sector' => 'Sector', 'Rambla' => 'Rambla' ],
         null, array('class' => 'form-control', 'data-live-search', 'true')) }}
    </div>

    <div class="form-group float-label-control label-bottom">
        {{ Form::label('adreca_immoble', 'Adreça', ['style' => 'font-weight:700']) }}
        {{ Form::text('adreca_immoble', null, array('class' => 'form-control adreca')) }}
        {{ Form::hidden('coordx', null, ['id' => 'coordx']) }}
        {{ Form::hidden('coordy', null, ['id' => 'coordy']) }}
    </div>
    <div class="form-group float-label-control label-bottom">
        {{ Form::label('adreca_publica', 'Adreça Pública', ['style' => 'font-weight:700']) }}
        {{ Form::text('adreca_publica', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        <div class="col-md-8 row">
            {{ Form::label('ciutat_id', 'Ciutat', ['style' => 'font-weight:700']) }}  <span class="label label-default pull-right afegir_ciutat"
                                                            style="cursor: pointer"> + Afegir ciutat</span>
            {{ Form::select('ciutat_id', $ciutats, null, array('class' => 'form-control ciutat_id', 'data-live-search', 'true')) }}
            {{ Form::text('ciutat_name', null, ['class' => 'form-control ciutat_name', 'style' => 'display:none']) }}
        </div>
        <a href="#" id="buscar_adreca" class="btn btn-primary pull-right" style="margin-top: 20px">Buscar</a>
    </div>
    <style type="text/css">
        #map-canvas {
            width: 100%;
            height: 200px;
            margin: 20px 0;
            padding: 0;
        }
    </style>

    <!-- Show Google MAPS! -->
    <div id="map-canvas"></div>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript">
        var map;
        var marker = "";
        var geocoder;

        function initialize() {

            geocoder = new google.maps.Geocoder();

            @if (isset($immoble) && $immoble->coordx != '')

                var mapOptions = {
                    center: {lat: {{ $immoble->coordy }}, lng: {{ $immoble->coordx }} },
                    zoom: 16
                };

                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                marker = new google.maps.Marker({
                    map: map,
                    position: {lat: {{ $immoble->coordy }}, lng: {{ $immoble->coordx }}},
                    draggable: true
                });
                google.maps.event.addListener(marker, 'dragend', function (event) {
                    $('#coordx').val(event.latLng.lng());
                    $('#coordy').val(event.latLng.lat());
                });

            @else

                var mapOptions = {
                    center: {lat: 41.9304952, lng: 2.2575258},
                    zoom: 8
                };
                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            @endif
        }

        google.maps.event.addDomListener(window, 'load', initialize);

        // Fem clic a buscar l'adreça per tal d'ubicar-la al mapa
        $('#buscar_adreca').click(function (e) {
            e.preventDefault();
            // Si hi ha markers els eliminem
            if (marker) marker.setMap(null);
            // Codifiquem l'adreça
            codeAddress();
            // Canviem el zoom
            map.setZoom(16);
        });

        function codeAddress() {
            var address = $("button[data-id='tipus_via']").attr('title') + ' ' + $('.adreca').val() + ', ' + $("button[data-id='ciutat_id']").attr('title');
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        draggable: true
                    });

                    // Event when marker has been dragged
                    google.maps.event.addListener(marker, 'dragend', function (event) {
                        $('#coordx').val(event.latLng.lng());
                        $('#coordy').val(event.latLng.lat());
                    });
                    $('#coordx').val(results[0].geometry.location.D);
                    $('#coordy').val(results[0].geometry.location.k);
                } else {
                    alert('No hem pogut trobar la ubicació. (' + status + ')');
                }
            });

        }

        $('#text_destacat').keyup( function() {
            $('#destacat_count').html( $(this).val().length + '/50')

        })

    </script>
</div>