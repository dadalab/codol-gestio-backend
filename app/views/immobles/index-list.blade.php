@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Llistat d'immobles
            <small>
                <a href="{{ URL::route('immoble.afegir') }}" class="btn btn-primary btn-lg pull-right">Afegir immoble</a>
                <a href="{{ URL::route('immobles.arxivats') }}" style="margin-right:10px" class="btn btn-primary btn-lg pull-right">Arxivats</a>
                <a href="{{ URL::route('immoble.triar_destacat') }}" style="margin-right:10px" class="btn btn-primary btn-lg pull-right">Triar destacat</a>
            </small>
        </h1>
    </div>
    <a href="{{ URL::route('immobles.quadricula') }}" class="pull-right" style="margin: 0 10px"><span
                class="glyphicon glyphicon-th"></span> </a>
    <table class="table" id="immobles">
        <thead>
        <th id="oficina">Oficina</th>
        <th>Expedient</th>
        <th>Adreça</th>
        <th>Ciutat</th>
        <th>Tipus</th>
        <th>Règim</th>
        <th>Preu</th>
        <th>Alta</th>
        <th></th>
        </thead>
        <tbody>
        @foreach($immobles as $immoble)
            <tr @if (Auth::user()->oficina->id == $immoble->oficina_id) class="active" @endif>
                <td>{{ $immoble->oficina->ciutat->nom }}</td>
                <td>{{ $immoble->num_expedient }}</td>
                <td>{{ $immoble->adreca_immoble }}</td>
                <td>{{ $immoble->ciutat->nom }}</td>
                <td>{{ $immoble->tipus->nom }}</td>
                <td>{{ $immoble->regim->nom }}</td>
                <!--<td class="text-right">{{ $immoble->getNumberFormat('preu') }} €</td>-->
                <td class="text-right">{{ $immoble->preu }}</td>
                <td>{{ $immoble->created_at->format('d-m-Y') }}</td>
                <td class="text-right">
                    <a href="{{ URL::route('immoble.visualitzar', $immoble->id) }}"><span
                                class="glyphicon glyphicon-eye-open"></span></a>
                    @if (Auth::user()->oficina->id == $immoble->oficina_id)
                        <a style="margin-left:10px" href="{{ URL::route('immoble.modificar', $immoble->id) }}"><span
                                    class="glyphicon glyphicon-pencil"></span></a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <script>

        $(document).ready(function () {
            $('#immobles th#oficina').each(function () {
                var title = $('#immobles th').eq($(this).index()).text();
                $(this).html('<select id="oficina-val"><option value=""></option><option value="Vic">Vic</option><option value="Torelló">Torelló</option><option value="Manlleu">Manlleu</option></select>');
            });
            
            $.fn.dataTable.moment( 'DD-MM-YYYY' );
            var table = $('#immobles').DataTable({
                "iDisplayLength": 50
            });

            $('#oficina-val').on('change', function () {
                table.columns(0).search($(this).val()).draw();
            });

            var ciutat = "{{ Auth::user()->oficina->ciutat->nom }}";
            table.columns(0).search(ciutat).draw();
            $('#oficina-val').val(ciutat);
        });

    </script>

    <script>
        $('#mevaoficina').click(function () {
            $('.active').hide();
        })
    </script>
@stop
