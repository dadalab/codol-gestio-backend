@extends('layouts.layout')

@section('content')

    {{ Form::open(array('route' => array('immoble.afegir'))) }}

    <div class="page-header">
        <h1>Afegint immoble
            <small>
                <button type="submit" class="btn btn-primary pull-right">Guardar canvis i publicar imatges</button>
            </small>
        </h1>
    </div>
    <div class="clearfix"></div>

    @include('common.errors')
    <div class="row">
        <div class="col-md-4">

            <div class="clearfix"></div>

            <!--<h4>Oficina</h4>-->
            <p>Afegint immoble a la oficina de {{ Auth::user()->oficina->ciutat->nom }}...</p>
            amb el número d'expedient {{ Auth::user()->oficina->expedient_prefix }}{{ str_pad(Auth::user()->oficina->seguent_num_expedient, '5', '0',  STR_PAD_LEFT) }}
            {{ Form::hidden('num_expedient', Auth::user()->oficina->expedient_prefix . str_pad(Auth::user()->oficina->seguent_num_expedient, '5', '0',  STR_PAD_LEFT) ) }}

            {{ Form::hidden('oficina_id', Auth::user()->oficina_id) }}
            {{--@foreach ($oficines as $oficina)
                <div class="radio col-md-4" style="margin-top:0"><label>{{ Form::radio('oficina_id', $oficina->id ) }} {{ $oficina->ciutat->nom }}</label></div>
            @endforeach --}}
            <div class="clearfix"></div>

            @include('immobles.form_parts.propietari')

            <!--<span data-name="destacat" data-value="0" data-toggle="tooltip"  title="no destacat" class="glyphicon glyphicon-pushpin edit-immoble-span"></span>
            <span data-name="publicat" data-value="0" data-toggle="tooltip"  title="no publicat" class="glyphicon glyphicon-globe edit-immoble-span"></span>
            <span data-name="oportunitat" data-value="0" data-toggle="tooltip"  title="no oportunitat" class="glyphicon glyphicon-star edit-immoble-span"></span>
            <span data-name="novetat" data-value="0" data-toggle="tooltip" title="no novetat" class="glyphicon glyphicon-certificate edit-immoble-span"></span> -->
        </div>

        <div class="col-md-8">

            @include('immobles.form_parts.principal')

            <div class="clearfix"></div>

            @include('immobles.form_parts.propietats')


        </div>
    </div>
    {{ Form::close() }}

@stop

