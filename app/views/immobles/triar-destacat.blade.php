@extends('layouts.layout')

@section('content')

    <div class="col-md-4">
        <div class="jumbotron">
            <p>Des d'aquí pots escollir l'immoble destacat de la teva oficina.<br>
                L'immoble destacat és aquell que surt a la portada de la pàgina web de CòdolGestió.</p>
        </div>
    </div>

    <div class="col-md-4">
        <h3>Nou immoble destacat</h3>
        {{ Form::open() }}
        <div class="form-group">
            {{ Form::select('immoble_id', withEmpty($immobles), null, array('class' => 'form-control', 'data-live-search', 'true')) }}
        </div>
        {{ Form::submit('Canviar immoble destacat', ['class' => 'form-control btn btn-primary']) }}
        {{ Form::close() }}
    </div>

    <div class="col-md-4 col-sm-12 col-xs-12 grid-pisos">
        <h3>Immoble destacat actual</h3>

        <div class="media">
            <a href="{{ URL::route('immoble.visualitzar', $destacat->id) }}">
                @if (isset($destacat->imatgeDestacada->nom))
                    <img src="http://codolgestio.net/content/{{ $destacat->imatgeDestacada->nom }}-thumb.jpg"
                         class="img-responsive">
                @endif
            </a>

            <div class="media-body">
                <a href="{{ URL::route('immoble.visualitzar', $destacat->id) }}"><h4
                            class="media-heading">{{ $destacat->tipus_via }} {{ $destacat->adreca_immoble }}</h4></a>

                <p>Règim: {{ $destacat->regim->nom }}<br>
                    Expedient: {{ $destacat->num_expedient }}<br>
                    Preu: {{ $destacat->getNumberFormat('preu') }} €<br>
                    Ciutat: {{ $destacat->ciutat->nom }}</p>
            </div>
        </div>
    </div>


@stop