@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Visualitzant immoble
            @if ($immoble->id > 1) <a href="{{ URL::route('immoble.visualitzar', [$immoble->id - 1, '2']) }}"><small style="font-size: 12px">Anterior</small></a> @endif
            <a href="{{ URL::route('immoble.visualitzar', [$immoble->id + 1, '1']) }}"><small style="font-size: 12px">Següent</small></a>
            <small>
                <a href="{{ URL::route('immobles') }}" style="margin-left: 10px" class="btn btn-default pull-right">Tornar al llistat</a>
                @if ($immoble->oficina_id == Auth::user()->oficina_id)
                    <a href="{{ URL::route('immoble.modificar', $immoble->id) }}" style="margin-left:10px" class="btn btn-primary pull-right">Modificar
                        immoble</a>
                @endif
                <a href="{{ URL::route('immoble.imprimir', $immoble->id) }}" style="margin-left: 10px" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-print"></i> </a>
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#accioComercial">Afegir acció comercial</button>
            </small>
        </h1>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        @if($immoble->deleted_at)
            <div class="col-md-12">
                <div class="alert alert-danger">Aquest immoble està arxivat!</div>
            </div>      
        @endif

        @if(Session::get('message') != "")
            <div class="col-md-12">
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            </div>   
        @endif

        <div class="col-md-4">
            @if (isset($immoble->imatgeDestacada))
                <img src="http://codolgestio.net/content/{{ $immoble->imatgeDestacada->nom }}-thumb.jpg" class="img-responsive img-rounded img-thumbnail">
                <!--<img src="{{ url() }}/imatges/immobles/{{ $immoble->imatgeDestacada->nom }}-thumb.jpg" class="img-responsive img-rounded img-thumbnail">-->
            @endif
            <div class="clearfix"></div>

            <div class="col-md-12" style="font-size: 16px; margin: 10px 0;">
                <span data-toggle="tooltip" title="{{ $immoble->destacat ? 'destacat' : 'no destacat' }}" class="glyphicon glyphicon-pushpin @if ($immoble->destacat) text-primary @endif"></span>
                <span data-toggle="tooltip" title="{{ $immoble->publicat ? 'publicat' : 'no publicat' }}" class="glyphicon glyphicon-globe @if ($immoble->publicat) text-primary @endif"></span>
                <span data-toggle="tooltip" title="{{ $immoble->oportunitat ? 'oportunitat' : 'no oportunitat' }}" class="glyphicon glyphicon-star @if ($immoble->oportunitat) text-primary @endif"></span>
                <span data-toggle="tooltip" title="{{ $immoble->novetat ? 'novetat' : 'no novetat' }}" class="glyphicon glyphicon-certificate @if ($immoble->novetat) text-primary @endif"></span>
                <span data-toggle="tooltip" title="{{ $immoble->fet_a_mida ? 'fet a mida' : 'no fet a mida' }}" class="glyphicon glyphicon-ok-circle @if ($immoble->fet_a_mida) text-primary @endif"></span>
                <span data-toggle="tooltip" data-nom="fotocasa" data-val="{{ $immoble->fotocasa }}" title="fotocasa" class="icon-portal @if ($immoble->fotocasa) text-primary @endif">fc</span>
                <span data-toggle="tooltip" data-nom="reclam" data-val="{{ $immoble->reclam }}" title="reclam" class="icon-reclam @if ($immoble->reclam) text-primary @endif">R</span>
            </div>
        </div>

        <div class="col-md-8">
            <div class="col-md-6">
                <h4>{{ $immoble->titol }}<br>
                    <small>Núm. Expedient: {{ $immoble->num_expedient }}</small><br>
                    <small>Referència Cadastral: {{ $immoble->ref_cadastral }}</small><br>
                    <small>Referència Externa: {{ $immoble->ref_externa }}</small><br>
                    <small>Inscripció registre: {{ $immoble->inscripcio_registre }}</small><br>
                    <small>Claus: {{ $immoble->claus }}</small>
                </h4>
                <p>{{ $immoble->descripcio }}</p>
                <p><b>{{ $immoble->text_destacat }}</b></p>
                <p>{{ $immoble->video_youtube }}</p>
            </div>

            <div class="col-md-6">
                <p class="text-muted"
                   style="margin-top: 10px">{{ $immoble->tipus_via}} {{ $immoble->adreca_immoble }}<br>
                    {{ $immoble->ciutat->nom }}
                </p>
            </div>

            <div class="clearfix"></div>
            <div style="height: 20px"></div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#immoble" role="tab" data-toggle="tab">Informació immoble</a></li>
                    @if ($immoble->oficina_id == Auth::user()->oficina_id)
                        <li role="presentation"><a href="#propietari" role="tab" data-toggle="tab">Informació propietari</a></li> 
                    @endif
                <li role="presentation"><a href="#visites" role="tab" data-toggle="tab">Visites</a></li>
                <li role="presentation"><a href="#accions_comercials" role="tab" data-toggle="tab">Accions comercials</a></li>
                <li role="presentation"><a href="#clients_interessats" role="tab" data-toggle="tab">Clients interessats</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="immoble">
                    @include('immobles.info_parts.immoble')
                </div>
                <div role="tabpanel" class="tab-pane" id="propietari">
                    @include('immobles.info_parts.propietari')
                </div>
                <div role="tabpanel" class="tab-pane" id="visites">
                    @include('immobles.info_parts.visites')
                </div>
                <div role="tabpanel" class="tab-pane" id="accions_comercials">
                    @include('immobles.info_parts.accions_comercials')
                </div>
                <div role="tabpanel" class="tab-pane" id="clients_interessats">
                    @include('immobles.info_parts.clients_interessats')
                </div>
            </div>

        </div>

    </div>
    <div class="clearfix"></div>
    <div class="row">
        <!-- Mostra de les imatges -->
        <h3>Galeria imatges de l'immoble</h3>

        <div class="row">
            @forelse($immoble->imatges as $imatge)

                <div class="col-xs-6 col-md-3">
                    <a target="_blank" href="http://codolgestio.net/content/{{ $imatge->nom }}.jpg"><img src="http://codolgestio.net/content/{{ $imatge->nom }}-thumb.jpg" class="img-responsive img-rounded img-thumbnail"></a>
                </div>

            @empty
                <div class="col-md-12"><p>Encara no hi ha cap imatge per aquest immoble!</p></div>
            @endforelse
        </div>
        <div class="clearfix"></div>
        <!-- Mostra de les imatges -->
        <h3>Documents associats</h3>

        <div class="row">
            @forelse($immoble->documents as $document)

                <div class="col-xs-6 col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ url() }}/documents/immobles/{{ $document->nom }}" target="_blank"><span class="glyphicon glyphicon-file"></span>{{ $document->nom }}</a></div>
                    </div>
                </div>

            @empty
                <div class="col-md-12"><p>Encara no hi ha cap document per aquest immoble!</p></div>
            @endforelse
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="accioComercial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tancar</span></button>
                    <h4 class="modal-title" id="myModalLabel">Afegir acció comercial</h4>
                </div>
                {{ Form::open(array('url' => URL::route('accio.afegir', $immoble->id) ) ) }}
                <div class="modal-body" style="overflow: hidden">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('data', 'Data') }}
                            {{ Form::text('data', \Carbon::now()->format("d-m-Y"), array('class' => 'form-control datepicker', "data-date-format" => "dd-mm-yyyy")) }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('observacions', 'Observacions') }}
                            {{ Form::textarea('observacions', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                    <button type="submit" id="store" class="btn btn-primary">Guardar acció comercial</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    
@stop




