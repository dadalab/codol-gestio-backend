<div class="col-md-12">
    <div style="height: 20px"></div>
    <ul class="list-group">
    
        @forelse($immoble->accionsComercials as $accio_comercial)
            <li class="list-group-item" style="overflow:hidden">
                
                <span class="badge">{{ $accio_comercial->data->format("d/m/Y") }}</span>
                <h5><b>Informació acció comercial</b></h5>
                <form method="post" action="{{ route('accio.update', $accio_comercial->id ) }}">
                    <div class="text-accio">{{ $accio_comercial->observacions }}</div>
                    <textarea class="input-accio form-control" style="display:none;margin-bottom:10px" name="observacions">{{ $accio_comercial->observacions }}</textarea>
                    <a href="{{ route('accio.destroy', $accio_comercial->id) }}" class="eliminar-accio pull-left" style="display:none">Eliminar acció</a>
                    <button type="submit" class="update-accio pull-right btn btn-primary" style="display:none;margin-left:10px">Guardar canvis</button>
                    <button type="button" class="cancelar-accio pull-right btn btn-default" style="display:none">Esborrar canvis</button>
                    <button type="button" class="update-boto btn btn-primary pull-right accio-modificar">Modificar</button>
                </form>
            </li>
        @empty
            Aquest immoble no té accions comercials registrades.
        @endforelse
    </ul>
</div>