<div class="col-md-12">
    <div style="height: 20px"></div>
    <ul class="list-group">
        @forelse($immoble->visites as $visita)
            <li class="list-group-item" id="{{ $visita->id }}">
                <b>Client</b> <a href="{{ URL::route('client.visualitzar', $visita->client->id) }}">{{ $visita->client->nom }} {{ $visita->client->cognoms }}</a>  |  <b>Telèfon</b> {{ $visita->client->telefon_1 }}  |  <b>Email</b> {{ $visita->client->email }}
                <span class="badge">{{ $visita->data }}</span>
                <h5>Observacions</h5>
                {{ $visita->observacions }}
                <span style="color:red" onClick="eliminarVisita({{ $visita->id }})" class="glyphicon glyphicon-remove pull-right"></span>
            </li>
        @empty
            Aquest immoble no té visites registrades.
        @endforelse
    </ul>
</div>

<script>
    function eliminarVisita(id) {
        var r = confirm("Segur que vols eliminar la visita?");
        if (r == true) {
            $.get( "{{ url() }}/visites/" + id + "/eliminar", function( data ) {
                $('#' + id).fadeOut('slow');
            });
        } 
    }
</script>