<div class="col-md-12">
    <div style="height: 20px"></div>
    @if (isset($immoble->propietari))
        <table class="table table-hover">
            <tr>
                <td><b>Propietari:</b> {{ $immoble->propietari->nom }} {{ $immoble->propietari->cognoms }}</td>
            </tr>
            <tr>
                <td><b>Telèfon:</b> {{ $immoble->propietari->telefon }}</td>
            </tr>
            <tr>
                <td><b>Email:</b> {{ $immoble->propietari->email }}</td>
            </tr><tr>
                <td><b>Observacions:</b> {{ $immoble->propietari->observacions }}</td>
            </tr>
        </table>
        <a href="{{ URL::route('immoble.modificar', $immoble->id) }}" class="btn btn-primary pull-right" >Canviar propietari</a>
        <a href="{{ URL::route('propietari.modificar', $immoble->propietari->id) }}" style="margin-right: 20px" class="btn btn-primary pull-right">Modificar propietari</a>

    @else
        Aquest immoble no té propietari registrat.
    @endif
</div>