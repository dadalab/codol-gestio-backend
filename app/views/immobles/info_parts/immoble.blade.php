<div style="height: 20px"></div>
<div class="col-md-6">

    <table class="table table-hover">
        <tr>
            <td><b>Tipus:</b> {{ $immoble->tipus->nom }}</td>
        </tr>
        <tr>
            <td><b>Règim:</b> {{ $immoble->regim->nom }}</td>
        </tr>
        <tr>
            <td><b>Estat:</b> {{ $immoble->estat->nom }}</td>
        </tr>
        <tr>
            <td><b>Certificat eficiència energètica:</b> {{ $immoble->certificat_eficiencia }}</td>
        </tr>
        <tr>
            <td><b>Cèdula Habitabilitat:</b> <span
                        class="badge">{{ $immoble->cedula_habitabilitat ? 'Sí' : 'No' }}</span></td>
        </tr>
        <tr>
            <td><span class="label @if($immoble->llum) label-primary @else label-default @endif">Llum</span> <span
                        class="label @if($immoble->aigua) label-primary @else label-default @endif">Aigua</span> <span
                        class="label @if($immoble->gas) label-primary @else label-default @endif">Gas</span></td>
        </tr>
        <tr>
            <td><b>Publicitat: </b><br>{{ nl2br($immoble->publicitat) }}</td>
        </tr>
    </table>
</div>
<div class="col-md-6">
    <table class="table table-hover">
        <tr>
            <td><b>Superfície:</b> <span class="badge pull-right">{{ $immoble->superficie }} m<sup>2</sup></span></td>
        </tr>
        <tr>
            <td><b>Dormitoris:</b> <span class="badge pull-right">{{ $immoble->dormitoris }}</span></td>
        </tr>
        <tr>
            <td><b>Banys:</b> <span class="badge pull-right">{{ $immoble->banys }}</span></td>
        </tr>
        <tr></tr>
        <tr>
            <td><b>Rètol penjat:</b> <span class="badge pull-right">{{ $immoble->retol_penjat }}</span></td>
        </tr>
        <tr>
            <td><b>Finançament:</b> <span class="badge pull-right">{{ $immoble->financament ? 'Sí' : 'No' }}</span></td>
        </tr>
        <tr>
            <td><b>Quota: </b><span class="badge pull-right">{{ $immoble->getNumberFormat('quota') }} €/mes</span></td>
        </tr>
        <tr>
            <td><b>Preu: </b><span class="badge pull-right">{{ $immoble->getNumberFormat('preu') }} €</span></td>
        </tr>
        @if ($immoble->regim_id == 3)
            <tr>
                <td><b>Preu lloguer: </b><span class="badge pull-right">{{ $immoble->getNumberFormat('preu_lloguer') }}
                        €</span></td>
            </tr>
        @endif
    </table>

</div>
<div class="col-md-12">

    <h4>Altres característiques</h4>
    @forelse($immoble->tags as $tag)
        <span class="label label-primary">{{ $tag->nom }}</span>
    @empty
        Aquest immoble no té altres característiques assignades
    @endforelse
</div>


