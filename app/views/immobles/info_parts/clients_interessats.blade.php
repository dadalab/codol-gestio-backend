<div class="col-md-12">
    <div style="height: 20px"></div>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#enviarCorreu">Enviar email als interessats</button>
    <ul class="list-group">
        
        @forelse($immoble->clientsInteressats as $client)
            <li class="list-group-item">
                <b>Client: </b> <a href="{{ URL::route('client.visualitzar', $client->id) }}">{{ $client->nom }} {{ $client->cognoms }}</a>  |
                  <b>Telèfon</b> {{ $client->telefon_1 }}  |  <b>Email</b> {{ $client->email }} 
                  <a href="{{ route('clientsinteressat.destroy', [$immoble->id, $client->id] ) }}"class="pull-right"><i class="glyphicon glyphicon-remove"></i></a>
            </li>
        @empty
            Aquest immoble no té clients interessats
        @endforelse
    </ul>
</div>

<div class="modal fade" id="enviarCorreu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tancar</span></button>
                <h4 class="modal-title" id="myModalLabel">Enviar correu als interessats</h4>
            </div>
            {{ Form::open(array('url' => URL::route('immoble.enviaremail', $immoble->id) ) ) }}
            <div class="modal-body" style="overflow: hidden">
            
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('assumpte', 'Assumpte') }}
                        {{ Form::text('assumpte', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('missatge', 'Missatge') }}
                        {{ Form::textarea('missatge', null, array('class' => 'form-control')) }}
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                <button type="submit" id="store" class="btn btn-primary">Enviar correu als interessats</button>
            </div>
            {{ Form::close() }}
            </div>
        </div>
    </div>