@extends('layouts.layout')

@section('content')

	{{ Form::open() }}
		<div class="page-header">
			<h1>Creant immoble <small><button type="submit" class="btn btn-primary btn-lg pull-right">Guardar canvis i publicar imatges</button></small></h1>
		</div>
		<div class="clearfix"></div>

		@include('common.errors')

		<div class="col-md-6">

			@include('immobles.form_parts.caracteristiques_generals')

			<div class="clearfix"></div>

			@include('immobles.form_parts.checkboxes')

		</div>
		<div class="col-md-6">

	    <div class="form-group">
		    <label for="retol_penjat">Rètol penjat</label>
			<input type="text" class="form-control" id="retol_penjat" name="retol_penjat">
		</div>



		</div>
	{{ Form::close() }}




@stop
