@extends('layouts.layout')

@section('content')

    <table class="table" id="arxivats">
        <thead>
        <th>Expedient</th>
        <th>Motiu</th>
        <th>Adreça</th>
        <th>Ciutat</th>
        <th>Preu</th>
        <th></th>
        <th></th>
        </thead>
        <tbody>
        @foreach($immobles as $immoble)
            <tr>
                <td><a href="{{ URL::route('immoble.visualitzar', $immoble->id)}}">{{ $immoble->num_expedient }}</a></td>
                <td>{{ $immoble->motiu_arxivar }}</td>
                <td>{{ $immoble->adreca_immoble }}</td>
                <td>{{ $immoble->ciutat->nom }}</td>
                <td>{{ $immoble->preu }}</td>
                <td><a href="{{ URL::route('immobles.recuperar', $immoble->id) }}"><span
                                class="glyphicon glyphicon-upload"></span> Recuperar</a></td>
                <td><a href="{{ URL::route('immobles.eliminar_definitivament', $immoble->id) }}"><span
                                class="glyphicon glyphicon-remove"></span> Eliminar definitivament</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <script>
        $(document).ready(function () {
            $('#arxivats').dataTable({
                "iDisplayLength": 50
            });
        });
    </script>

@stop

