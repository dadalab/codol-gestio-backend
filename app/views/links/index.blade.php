@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Selecciona immobles per enviar</h1>
    </div>
    @include('common.errors')
    <form method="post" action="{{ route('enllacos.post') }}">
        <div class="col-md-2 col-md-offset-2">
            <div class="form-group text-center">
                <label for="nom">Nom</label>
                <input type="text" name="nom" id="nom" class="form-control"></input>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group text-center">
                <label for="email">Email</label>
                <input type="text" name="email" id="email" class="form-control"></input>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label></label>
                <input type="submit" value="Enviar correu" class="btn btn-primary" style="display: block; margin-top: 6px;">
            </div>
        </div>
        <div class="col-md-6 col-md-offset-2">
            <label for="text" class="text-center" style="display:block">Missatge</label>
            <textarea name="text" id="text" class="form-control">a continuació li adjuntem el catàleg amb els immobles que hem escollit per vostè.</textarea>
        </div>
        

        <table class="table no-selectpicker" id="export">
            <thead>
                <th style="width:30px">Marcats</th>
                <th>Expedient</th>
                <th>Publicat</th>
                <th>Adreça</th>
                <th>Ciutat</th>
                <th>Tipus</th>
                <th>Règim</th>
            </thead>
            <tbody>
            @foreach($immobles as $immoble)
                <tr data-immoble="{{ $immoble->id }}">
                    <td class="text-center"><input type="checkbox" value="{{ $immoble->id }}" id="immoble{{ $immoble->id }}" name="marcats[]"></input></td>
                    <td><label for="immoble{{ $immoble->id }}">{{ $immoble->num_expedient }}</label></td>
                    <td class="text-center">
                        {{ $immoble->publicat ? "<span class='label label-success'>Publicat</span>" : "<span class='label label-danger'>NO publicat</span>" }}
                    </td>
                    <td>{{ \Str::limit($immoble->adreca_immoble, 25) }}</td>
                    <td>{{ \Str::limit($immoble->ciutat->nom, 15) }}</td>
                    <td>{{ $immoble->tipus->nom }}</td>
                    <td>{{ $immoble->regim->nom }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </form>

    <script>
        $(document).ready(function () {

            var table = $('#export').DataTable({
                "iDisplayLength": 9999
            });
        });

    </script>

@stop