<style>
	body {
		font-family: sans-serif;
	}
	td {
		padding:0;
		vertical-align: top;
	}
	a {
		color: #000;
		text-decoration: none;
	}
</style>
<div style="max-width: 600px;margin: 0 auto 15px auto">
	<a href="http://codolgestio.net"><img src="http://codolgestio.net/wp-content/uploads/2014/09/codol-gestio-logotip.png"></a>
</div>

<div style="background: #d3460e;margin:0px auto;color: #fff;max-width: 600px;">
	<p style="padding:30px;text-align: center;margin:0;">
		Hola {{ $nom }},<br>
		{{ $text }}<br><br>
		<a href="{{ $link }}" style="text-decoration: underline; color:#fff">Veure sel·lecció a la web</a>
	</p>
</div>

<table style="width: 600px;margin:auto;" cellspacing="0" cellpadding="0">
	@foreach($immobles as $immoble)
		<?php $link = "http://codolgestio.net/detall/" . $immoble->url ?>
		<tr>
			<td style="width: 200px;border-bottom: 1px solid #d3460e;padding-bottom:15px;padding-top:15px;vertical-align: top;">
				@if ($immoble->imatgeDestacada)
                	<a href="{{ $link }}" target="_blank"><img src="http://codolgestio.net/content/{{ $immoble->imatgeDestacada->nom }}-thumb.jpg" style="max-width: 100%;"></a>
            	@endif
        	</td>
			<td style="width: 200px; border-bottom: 1px solid #d3460e;padding-bottom:15px;padding-top:15px;vertical-align: top;">
				<p style="padding: 0 20px;margin:0;">
					<a href="{{ $link }}" target="_blank" style="color: #000;text-decoration: none">
						<b style="text-transform: uppercase">{{ $immoble->titol }}</b>
					</a>
				</p>
			</td>
			<td style="border-bottom: 1px solid #d3460e;padding-bottom:15px;padding-top:15px;vertical-align: top;">
				<p style="font-size: 14px;margin:0">{{ str_limit($immoble->descripcio, 150) }}</p><a href="{{ $link }}" target="_blank" style="color:#d3460e;text-decoration: underline;display:block;margin-top: 10px;">més informació</a>
			</td>
		</tr>
	@endforeach

</table>