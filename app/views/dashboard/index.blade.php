@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <a href="{{ URL::route('immobles') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <h1>Immobles</h1>
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="{{ URL::route('registre_claus') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-tags"></span>
                    <h1>Registre claus</h1>
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="{{ URL::route('propietaris') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-user"></span>
                    <h1>Propietaris</h1>
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="{{ URL::route('visites') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-eye-open"></span>
                    <h1>Registre visites</h1>
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="{{ URL::route('clients') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-asterisk"></span>
                    <h1>Clients</h1>
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="{{ URL::route('portals') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-cloud"></span>
                    <h1>Publicació immobles</h1>
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="{{ URL::route('exportar') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-floppy-save"></span>
                    <h1>Exportació excel</h1>
                </div>
            </a>
        </div>
        @if (Auth::user()->username == 'marc.coll' || Auth::user()->username == 'torello' || Auth::user()->username == 'manlleu')
        <div class="col-md-4">
            <a href="{{ URL::route('tvs') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-off"></span>
                    <h1>Televisions</h1>
                </div>
            </a>
        </div>
        @endif
        <div class="col-md-4">
            <a href="{{ URL::route('valoracions') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-star"></span>
                    <h1>Valoracions</h1>
                </div>
            </a>
        </div>
        @if (Auth::user()->username == 'marc.coll' || Auth::user()->username == 'torello')
        <div class="col-md-4">
            <a href="{{ URL::route('pdfs') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-list-alt"></span>
                    <h1>Fulletons</h1>
                </div>
            </a>
        </div>
        @endif
        @if (Auth::user()->username == 'marc.coll' || Auth::user()->username == 'manlleu')
        <div class="col-md-4">
            <a href="{{ URL::route('carteles') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-file"></span>
                    <h1>Carteles</h1>
                </div>
            </a>
        </div>
        @endif
        <div class="col-md-4">
            <a href="{{ URL::route('enllacos') }}">
                <div class="home">
                    <span class="glyphicon glyphicon-send"></span>
                    <h1>Generador enllaços</h1> 
                </div>
            </a>
        </div>
    </div>

    
@stop
