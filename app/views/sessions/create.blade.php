<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Còdol Gestió</title>
    <link rel="stylesheet" type="text/css" href="{{ url() }}//assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url() }}//assets/css/custom.css">
     <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <style>
    body {
        /*background: url(http://habrastorage.org/files/c9c/191/f22/c9c191f226c643eabcce6debfe76049d.jpg);*/
        background-color: #d3460e;
    }

    .jumbotron {
    	text-align: center;
    	border-radius: 0.5rem;
    	top: 0;
    	bottom: 0;
    	left: 0;
    	right: 0;
    	position: absolute;
    	margin: 4rem auto;
    	background-color: #fff;
    	padding: 2rem;
    }

    .container .glyphicon-home {
    	font-size: 10rem;
    	margin-top: 3rem;
    	color: #444;
    }

    input {
    	width: 100%;
    	margin-bottom: 1.4rem;
    	padding: 1rem;
    	background-color: #ecf2f4;
    	border-radius: 0.2rem;
    	border: none;
    }
    h2 {
    	margin-bottom: 3rem;
    	font-weight: bold;
    	color: #444;
    }
    .btn {
    	border-radius: 0.2rem;
    }
    .btn .glyphicon {
    	font-size: 3rem;
    	color: #fff;
    }
    .full-width {
    	background-color: #8eb5e2;
    	width: 100%;
    	-webkit-border-top-right-radius: 0;
    	-webkit-border-bottom-right-radius: 0;
    	-moz-border-radius-topright: 0;
    	-moz-border-radius-bottomright: 0;
    	border-top-right-radius: 0;
    	border-bottom-right-radius: 0;
    	border: none;
        border-radius: 3px;
    }
    .full-width:hover {
        background-color: #666;
    }
    .box {

    	bottom: 0;
    	left: 0;
    	margin-bottom: 3rem;
    	margin-left: 3rem;
    	margin-right: 3rem;
    }
    </style>
</head>
<body>
    @include('common.notification')

    <div class="col-md-3 jumbotron">
      <div class="container">
       <!-- <div class="logotip-inici"></div><!-- glyphicon glyphicon-home -->
       <!-- <h2>Còdol Gestió</h2>-->
        <img src="{{ url() }}/images/codol-gestio-logotip.png" style="margin-top: 20px;">
        <div class="col-md-12" style="margin-top:30px;">

                @if ($errors->has())
                     <div class="alert alert-warning">
                        @foreach ($errors->all('<li>:message</li>') as $message)
                            {{ $message }}
                        @endforeach
                    </div>
                @endif


            {{ Form::open() }}
                {{ Form::text('username', null, ['placeholder' => 'nom d\'usuari']) }}
                {{ Form::password('password', ['placeholder' => 'contrasenya']) }}
                <button class="btn btn-default full-width"><span class="glyphicon glyphicon-ok"></span></button>
            {{ Form::close() }}
        </div>
      </div>
    </div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
   <script src="{{ url() }}/assets/js/bootstrap.min.js"></script>
</body>
</html>
