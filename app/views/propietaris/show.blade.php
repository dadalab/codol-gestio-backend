@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>
            Visualitzar propietari
            <a href="{{ URL::route('propietaris') }}" style="margin-left: 10px" class="btn btn-default pull-right">Tornar al llistat</a>
            <a href="{{ URL::route('propietari.modificar', array('id' => $propietari->id)) }}"
               class="btn btn-primary pull-right">Modificar propietari</a>
        </h1>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Característiques propietari</h4></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>Nom</td>
                        <td>{{ $propietari->nom }}</td>
                    </tr>
                    <tr>
                        <td>Cognoms</td>
                        <td>{{ $propietari->cognoms }}</td>
                    </tr>
                    <tr>
                        <td>Nom 2</td>
                        <td>{{ $propietari->nom2 }}</td>
                    </tr>
                    <tr>
                        <td>Cognoms 2</td>
                        <td>{{ $propietari->cognoms2 }}</td>
                    </tr>
                    <tr>
                        <td>NIF</td>
                        <td>{{ $propietari->nif }}</td>
                    </tr>
                    <tr>
                        <td>Adreça</td>
                        <td>{{ $propietari->adreca }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{ $propietari->email }}</td>
                    </tr>
                    <tr>
                        <td>Telèfon</td>
                        <td>{{ $propietari->telefon }}</td>
                    </tr>
                    <tr>
                        <td>Telèfon</td>
                        <td>{{ $propietari->telefon2 }}</td>
                    </tr>
                    <tr>
                        <td>Observacions</td>
                        <td>{{ $propietari->observacions }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button type="button" class="btn btn-danger btn-sm pull-right" data-toggle="modal" data-target="#myModal">Arxivar propietari</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">

        @forelse($propietari->immobles as $immoble)
            <div class="media">
                <a class="media-left" href="{{ URL::route('immoble.visualitzar', $immoble->id) }}">
                    @if(isset($immoble->imatgeDestacada))
                        <img src="http://codolgestio.net/content/{{ $immoble->imatgeDestacada->nom }}-thumb.jpg"
                             style="width: 200px;" class="img-responsive img-thumbnail">
                    @endif
                </a>

                <div class="media-body">
                    <h4 class="media-heading"><a
                                href="{{ URL::route('immoble.visualitzar', $immoble->id) }}"> {{ $immoble->adreca_immoble }}</a>
                        <br>
                        <small>{{ $immoble->num_expedient }}</small>
                    </h4>
                    {{ $immoble->descripcio }}
                </div>
            </div>
            <hr>
        @empty
            Aquest propietari no té cap propietat registrada
        @endforelse


    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Tancar</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Arxivar propietari</h4>
                </div>
                {{ Form::open(array('route' => array('propietari.eliminar', $propietari->id))) }}
                <div class="modal-body">
                    Estàs segur que vols arxivar aquest propietari?
                    {{ Form::text('motiu_arxivar', null, ['class' => 'form-control', 'placeholder' => 'Motiu arxivar', 'style' => 'margin-top: 15px']) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar
                    </button>
                    <button type="submit" class="btn btn-danger">Arxivar</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>



@stop
