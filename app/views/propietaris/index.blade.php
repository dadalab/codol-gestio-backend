@extends('layouts.layout')

@section('content')
    <div class="page-header">
        <h1>Propietaris <!--<small><a href="{{ URL::route('propietari.afegir') }}" class="btn btn-primary btn-lg pull-right">Afegir propietari</a></small>--></h1>
    </div>

    <table class="table table-striped" id="propietaris">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Cognoms</th>
                <th>Nom 2</th>
                <th>Cognoms 2</th>
                <th>NIF</th>
                <th>Telèfon</th>
                <th>Email</th>
                <th></th>
            </tr>
        </thead>
        @foreach($propietaris as $propietari)
        <tr>
            <td style="min-width:50px">{{ $propietari->nom }}</td>
            <td>{{ $propietari->cognoms }}</td>
            <td style="min-width:50px !important">{{ $propietari->nom2 }}</td>
            <td style="min-width:65px">{{ $propietari->cognoms2 }}</td>
            <td>{{ $propietari->nif }}</td>
            <td>{{ $propietari->telefon }}</td>
            <td>{{ $propietari->email }}</td>

            <td class="text-right" style="min-width:45px">
                <a href="{{URL::route('propietari.visualitzar', array('id' => $propietari->id)) }}" <span class="glyphicon glyphicon-eye-open"></span></a>
                <a style="margin-left: 10px" href="{{URL::route('propietari.modificar', array('id' => $propietari->id)) }}" <span class="glyphicon glyphicon-pencil"></span></a>
            </td>
        </tr>
        @endforeach
    </table>

     <script>
        $(document).ready(function() {
            $('#propietaris').dataTable({
                "iDisplayLength": 50
            });
        } );
    </script>

@stop