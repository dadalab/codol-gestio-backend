@extends('layouts.layout')

@section('content')

    {{ Form::model($propietari, array('route' => array('propietari.actualitzar', $propietari->id))) }}
    <div class="page-header">
        <h1>Modificar propietari: {{ $propietari->nom }}
            <small>
                <a href="{{ URL::route('propietari.visualitzar', $propietari->id) }}" style="margin-left: 10px"
                   class="btn btn-default pull-right">No guardar</a>
                <button type="submit" class="btn btn-primary pull-right">Guardar canvis</button>
            </small>
        </h1>
    </div>

    <div class="clearfix"></div>

    @include('common.errors')

    <div class="col-md-6">

        <div class="form-group">
            <label for="nom">Nom</label>
            {{ Form::text('nom', null,  array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            <label for="cognoms">Cognoms</label>
            {{ Form::text('cognoms', null,  array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            <label for="nom">Nom 2</label>
            {{ Form::text('nom2', null,  array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            <label for="cognoms">Cognoms 2</label>
            {{ Form::text('cognoms2', null,  array('class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="nif">NIF</label>
            {{ Form::text('nif', null,  array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            <label for="adreca">Adreça</label>
            {{ Form::text('adreca', null,  array('class' => 'form-control', 'rows' => '2')) }}
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            {{ Form::text('email', null,  array('class' => 'form-control')) }}
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="telefon">Telèfon</label>
                    {{ Form::text('telefon', null,  array('class' => 'form-control')) }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="telefon">Telèfon 2</label>
                    {{ Form::text('telefon2', null,  array('class' => 'form-control')) }}
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="observacions">Observacions</label>
            {{ Form::textarea('observacions', null,  array('class' => 'form-control')) }}
        </div>
    </div>


    {{ Form::close() }}

@stop
