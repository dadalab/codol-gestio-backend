<!doctype html>
<html lang="ca">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Codol Gestió</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
        <link class="rs-file" href="{{ asset('lib/royalslider/royalslider.css') }}" rel="stylesheet">
    </head>
    <body> 
        <div class="content">
            <div class="sliderContainer fullWidth clearfix">
                <div id="full-width-slider" class="royalSlider heroSlider rsMinW">
                    @foreach($tvs as $tv)
                        @for($i = 0; $i < $tv->num_fotos; $i++)
                            @if ($tv->immoble)
                                @include('tvs.slide-immoble')
                            @endif
                        @endfor
                    @endforeach
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script class="rs-file" src="{{ asset('lib/royalslider/jquery.royalslider.min.js') }}"></script>
        <script src="{{ asset('assets/js/main.js') }}"></script>

        <script>
        function internetIsWorking() {
            var xhr = new XMLHttpRequest();
            // ! Controlar per oficina ! //
            var file = "{{ url() }}/pantalla/manlleu";
            var randomNum = Math.round(Math.random() * 10000);
             
            xhr.open('HEAD', file + "?rand=" + randomNum, false);
             
            try {
                xhr.send();
                if (xhr.status >= 200 && xhr.status < 304) {
                    return true;
                } else {
                    return false;
                }
            } catch (e) {
                return false;
            }
        }

       setInterval(function() {
            if (internetIsWorking())
                window.location.reload();
        }, 1000 * 60 * 60 * 1);
        </script>
    </body>
</html>