<?php 

$image = "";
// Si és primera imatge, carreguem la destacada.
if ($i == 0 && isset($tv->immoble->imatgeDestacada->nom)) :
    if (file_exists("http://codolgestio.net/content/" . $tv->immoble->imatgeDestacada->nom . "-big.jpg")) :
        $image = "http://codolgestio.net/content/" . $tv->immoble->imatgeDestacada->nom . "-big.jpg";
    else :
        $image = "http://codolgestio.net/content/" . $tv->immoble->imatgeDestacada->nom . ".jpg";
    endif;
else :
    $imatge = $tv->immoble->imatges->get($i);
    if (isset($imatge)) :
        if (file_exists("http://codolgestio.net/content/" . $imatge->nom . "-big.jpg")) :
            $image = "http://codolgestio.net/content/" . $imatge->nom . "-big.jpg";
        else :
            $image = "http://codolgestio.net/content/" . $imatge->nom . ".jpg";
        endif;
    else :
        $image = null;
    endif;
endif;
?>

@if ($image)
    <div class="rsContent">
        @if ($tv->immoble->novetat || $tv->immoble->oportunitat)
            <div class="vitola rsABlock">
                 @if ($tv->immoble->novetat)<span @if(! $tv->immoble->oportunitat) style="border:none" @endif>NOVETAT</span>@endif
                 @if ($tv->immoble->oportunitat)<span @if(! $tv->immoble->novetat) style="border:none" @endif>Oh!PORTUNITAT</span>@endif
            </div>
        @endif

        <?php $possiblesMoviments = ['zoomIn', 'zoomOut'] ?>
        <img class="rsImg {{ $possiblesMoviments[rand(0,1)] }}" src="{{ $image }}" data-speed="1000" data-fade-effect="true" />

        <div class="infoBlock rsABlock infoBlockRight">
            <h4 style="padding-top:10px;">{{ $tv->immoble->titol }}</h4>
            <p>
                <strong>{{ $tv->immoble->tipus_via or '' }} {{ $tv->immoble->adreca_publica ?: $tv->immoble->adreca_immoble }}</strong><br>
                {{ $tv->immoble->superficie }} M<sup>2</sup>@if ($tv->immoble->dormitoris > 0) / {{ $tv->immoble->dormitoris }} HAB. @endif
                @if ($tv->immoble->banys > 0) / {{ $tv->immoble->banys }} BANY{{ $tv->immoble->banys > 1 ? 'S' : '' }}<br>@endif
            </p>
            <div class="destacats">
                            <div class="qualificacio" style="margin-bottom: 15px;font-size: 18px;text-transform: uppercase"><b>QUALIFICACIÓ ENERGÈTICA: {{ $tv->immoble->certificat_eficiencia }}</b></div>
                            <hr>
                @if ($tv->immoble->financament) <div class="financament">100%<br>FINANC.</div> @endif
                @if ($tv->immoble->estat_id == 1)<div class="obra-nova">OBRA<br>NOVA</div>@endif
                @if ($tv->immoble->destacat)<div class="topo destacat"><div class="contingut"><i class="fa fa-star"></i></div></div>@endif
    

            </div>
            <div class="preu-content">
                <div class="preu"><span>Preu</span>@if($tv->immoble->preu > 0){{ $tv->immoble->getNumberFormat() }}€ @else A consultar @endif</div>
                @if ($tv->immoble->quota > 0)<div class="preu" style="margin-top:0px"><span>Quota</span>{{ $tv->immoble->getNumberFormat('quota') }}€/mes</div>@endif
                <div class="web">CONSULTA TOTS ELS DETALLS A CODOLGESTIO.NET</div>
            </div>
        </div>
    </div>
@endif