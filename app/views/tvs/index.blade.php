@extends('layouts.layout')

@section('content')
<div id="tvs">
	<div class="col-md-6" style="margin-bottom: 25px;">
		<h4>Afegir nou immoble a la tv</h4>
		
		<div class="col-md-12 row">
			<div class="row">
				<div class="col-md-6">
					{{ Form::select('immoble_id', withEmpty($immobles), null, array('class' => 'form-control selector-immoble', 'data-live-search', 'true')) }}
					<input type="hidden" id="immoble_id">
				</div>
				<div class="col-md-6">	
					<button id="afegirImmoble" class="btn btn-primary" v-on="click : afegirImmoble">Afegir immoble</button>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<h4 style="color:white">a</h4>
		<a href="{{ url() }}/pantalla/{{ Auth::user()->username }}" target="_blank" class="btn btn-primary pull-right">Veure TV</a>
	</div>

    <table class="table table-striped" >
        <thead>
            <tr>
                <th>Nom</th>
                <th>Expedient</th>
                <th>Adreça</th>
                <th>N. Fotos</th>
                <th>Ordre</th>
                <th></th>
            </tr>
        </thead>
        <?php $count = 0 ?>
        <tr v-repeat="tv : tvs | orderBy 'order'">
        	<?php $count++ ?>
            <td>@{{ tv.immoble.titol }}</td>
            <td>@{{ tv.immoble.num_expedient }}</td>
            <td>@{{ tv.immoble.tipus_via }} @{{ tv.immoble.adreca_immoble }}</td>
            <td><input type="text" class="numFotos@{{ tv.id }} text-center" value="@{{ tv.num_fotos }}" v-on="change : actualitzarFotos(tv)" style="max-width:50px;"></td>
            <td>
            	<i class="glyphicon glyphicon-arrow-down" style="cursor:pointer" v-on="click : baixar(tv)"></i>
            	<i class="glyphicon glyphicon-arrow-up" style="cursor:pointer" v-on="click : pujar(tv)"></i>
            </td>
            <td><i class="glyphicon glyphicon-remove" style="color:red; cursor:pointer" v-on="click : eliminar(tv)"></i></td>
        </tr>
 
    </table>
</div>

<script>

	var tvsvue = new Vue({
		el: '#tvs',
		data: {
			tvs : {{ $tvs }},
			immobleAfegir: '',
		},

		ready: function() {
			$('.selector-immoble').change( function() {
				this.immobleAfegir = $(this).val();
				$('#immoble_id').val( $(this).val() );
			});
		}, 

		methods: {
			afegirImmoble: function() {
				this.immobleAfegir = $('#immoble_id').val();
				this.$http.post('tvs/immoble/' + this.immobleAfegir, function (data, status, request) {
            		this.tvs.push(data);
    			});
			},

			pujar: function(tv, index) {
				if (tv.order > 1)
				{	
					tv.order -= 1;
					
					this.$http.put('tvs/ordre/up/' + tv.id + '/' + tv.order);
					
					for (var i = 0; i < this.tvs.length; ++i)
					{
						if (this.tvs[i].order == tv.order && tv.id != this.tvs[i].id)		
							this.tvs[i].order += 1;
					}
				}
			},
			baixar: function(tv, index) {
				if (tv.order < this.tvs.length) 
				{	
					tv.order += 1;
					this.$http.put('tvs/ordre/down/' + tv.id + '/' + tv.order);
					for (var i = 0; i < this.tvs.length; ++i)
					{
						if (this.tvs[i].order == tv.order && tv.immoble.num_expedient != this.tvs[i].immoble.num_expedient)		
							this.tvs[i].order -= 1;
					}
				}
			},
			eliminar: function(tv)
			{
				for (var i = 0; i < this.tvs.length; ++i)
				{
					if (this.tvs[i].id == tv.id)		
					{
						this.tvs.$remove(i);
						break;
					}
				}

				this.$http.delete('tvs/immoble/' + tv.id, function (data, status, request) {});
			},
			actualitzarFotos: function(tv)
			{
				var numFotos = $( '.numFotos' + tv.id ).val();
				this.$http.put('tvs/' + tv.id + '/fotos/' + numFotos);
			}
		},

	});

</script>

@stop