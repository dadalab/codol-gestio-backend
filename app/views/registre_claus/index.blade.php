@extends('layouts.layout')

@section('content')

    <div class="page-header">
        <h1>Registre de claus</h1>
    </div>

    <table id="registre_claus" class="table table-striped">
        <thead>
            <tr>
                <th>Codi Clau</th>
                <th>Immoble</th>
                <th>Ciutat</th>
            </tr>
        </thead>

        @foreach($immobles as $immoble)
        <tr>
            <td>{{ $immoble->claus }}</td>
            <td><a href="{{ URL::route('immoble.visualitzar', $immoble->id) }}">{{ $immoble->adreca_immoble }}</td>
            <td>{{ $immoble->ciutat->nom }}</td>
        </tr>
        @endforeach
    </table>

     <script>
        $(document).ready(function() {
            $('#registre_claus').dataTable();
        } );
    </script>
@stop