<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOficinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oficines', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('ciutat_id');
			$table->integer('seguent_num_expedient');
			$table->string('expedient_prefix');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oficines');
	}

}
