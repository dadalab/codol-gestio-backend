<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visites', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('immoble_id');
            $table->integer('client_id');
            $table->date('data');
            $table->text('observacions');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visites');
	}

}
