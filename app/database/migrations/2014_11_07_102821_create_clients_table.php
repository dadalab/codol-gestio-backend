<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('oficina_id');
            $table->string('nom');
            $table->string('cognoms');
            $table->string('nom_altre');
            $table->string('cognoms_altre');
            $table->integer('ciutat_id');
            $table->string('comercial');
            $table->date('data_contacte');
            $table->string('poblacio');
            $table->string('telefon_1');
            $table->string('telefon_2');
            $table->string('email');
            $table->text('comentari');
            $table->integer('valoracio');
            $table->string('origen');
            $table->string('nif');
            $table->string('dades_economiques');
            $table->integer('regim_id');
            $table->integer('preu_maxim');
            $table->integer('poblacio_id');
            $table->integer('tipus_id');
            $table->string('moblat');
            $table->integer('habitacions');
            $table->string('motiu_arxivar');

			$table->timestamps();
            $table->date('deleted_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
