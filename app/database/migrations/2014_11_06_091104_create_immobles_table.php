<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImmoblesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('immobles', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('oficina_id');
            $table->integer('propietari_id');
            $table->integer('immobles_imatges_id');
            $table->string('num_expedient');
            $table->string('ref_cadastral');
            $table->string('inscripcio_registre');
            $table->string('claus');
            $table->string('titol');
            $table->text('descripcio');
            $table->string('video_youtube');
            $table->integer('tipus_id');
            $table->integer('regim_id');
            $table->integer('estat_id');
            $table->integer('ciutat_id');
            $table->float('preu');
            $table->float('preu_lloguer');
            $table->float('interes');
            $table->float('anys');
            $table->float('quota');
            $table->text('adreca_immoble');
            $table->integer('superficie');
            $table->integer('superficie_total');
            $table->integer('dormitoris');
            $table->integer('banys');
            $table->integer('places');
            $table->string('certificat_eficiencia');
            $table->boolean('financament');
            $table->boolean('publicat');
            $table->boolean('oportunitat');
            $table->boolean('destacat');
            $table->boolean('novetat');
            $table->boolean('fet_a_mida');
            $table->string('cedula_habitabilitat');
            $table->boolean('hipoteca');
            $table->boolean('llum');
            $table->boolean('aigua');
            $table->boolean('gas');
            $table->date('retol_penjat');
            $table->string('coordx');
            $table->string('coordy');
            $table->string('tipus_via');
            $table->text('publicitat');
            $table->string('url');
            $table->string('motiu_arxivar');

            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('immobles');
	}

}
