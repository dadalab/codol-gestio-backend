<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImmoblesImatgesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('immobles_imatges', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('immoble_id');
            $table->integer('order');
            $table->string('nom');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('immobles_imatges');
	}

}
