<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropietarisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('propietaris', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('oficina_id');
            $table->string('nom');
            $table->string('cognoms');
			$table->string('nom2');
            $table->string('cognoms2');
            $table->string('nif');
            $table->text('adreca');
            $table->string('telefon');
            $table->string('telefon2');
            $table->string('email');
            $table->text('observacions');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('propietaris');
	}

}
