<?php

use CodolGestio\Ciutat;
use CodolGestio\Oficina;
use CodolGestio\Estat;
use CodolGestio\Regim;
use CodolGestio\Tipus;
use CodolGestio\Tag;
use CodolGestio\User;
use CodolGestio\Immoble;


class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Assignem nums expedient
        $immobles = Immoble::all();

        foreach($immobles as $immoble)
        {
            if ( $immoble->num_expedient == "")
            {
                $immoble->num_expedient = $immoble->oficina->expedient_prefix . $immoble->oficina->seguent_num_expedient;
                $immoble->save();

                $immoble->oficina->seguent_num_expedient += 1;
                $immoble->oficina->save();
            }
        }

       // Eloquent::unguard();

        // $this->call('UserTableSeeder');
        /*User::create(array('email' => 'foo@bar.com', 'oficina_id' => 1, 'username' => 'marc.coll', 'password' => Hash::make('1234')));
        User::create(array('email' => 'foo@bar.com', 'oficina_id' => 1, 'username' => 'torello', 'password' => Hash::make('1234')));

        Ciutat::create(array('nom' => 'Vic'));
        Ciutat::create(array('nom' => 'Manlleu'));
        Ciutat::create(array('nom' => 'Torello'));

        Oficina::create(array('ciutat_id' => 1));
        Oficina::create(array('ciutat_id' => 2));
        Oficina::create(array('ciutat_id' => 3));

        Estat::create(array('nom' => 'Nou'));
        Estat::create(array('nom' => 'Seminou'));
        Estat::create(array('nom' => 'Correcte'));
        Estat::create(array('nom' => 'Reformat'));
        Estat::create(array('nom' => 'Antic'));
        Estat::create(array('nom' => 'A reformar'));

        Regim::create(array('nom' => 'Lloguer'));
        Regim::create(array('nom' => 'Venda'));
        Regim::create(array('nom' => 'Venda i/o Lloguer'));

        Tipus::create(array('nom' => 'Pis'));
        Tipus::create(array('nom' => 'Casa'));
        Tipus::create(array('nom' => 'Local'));
        Tipus::create(array('nom' => 'Nau'));
        Tipus::create(array('nom' => 'Pàrquing'));
        Tipus::create(array('nom' => 'Solar'));

        $tags = ['terrassa', 'jardí', 'balcó', 'amb mobles', 'ascensor', 'calefacció gas natural', 'pisicina', 'video porter', 'tancaments alumini', 'zona infantil', 'cuina amb electrodomèstic', 'armaris sencastats', 'despeses comunitat incloses', 'pàrquing inclòs', 'aire condicionat', 'llar de foc', 'garatge privat', 'traster', 'cuina office'];

        foreach ($tags as $tag)
        {
            Tag::create(['nom' => $tag]);
        } */
    }

}
