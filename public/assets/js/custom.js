/* Float Label Pattern Plugin for Bootstrap 3.1.0 by Travis Wilson
 **************************************************/

(function ($) {
    $.fn.floatLabels = function (options) {

        // Settings
        var self = this;
        var settings = $.extend({}, options);


        // Event Handlers
        function registerEventHandlers() {
            self.on('input keyup change', 'input, textarea', function () {
                actions.swapLabels(this);
            });
        }


        // Actions
        var actions = {
            initialize: function () {
                self.each(function () {
                    var $this = $(this);
                    var $label = $this.children('label');
                    var $field = $this.find('input,textarea').first();

                    if ($this.children().first().is('label')) {
                        $this.children().first().remove();
                        $this.append($label);
                    }

                    var placeholderText = ($field.attr('placeholder') && $field.attr('placeholder') != $label.text()) ? $field.attr('placeholder') : $label.text();

                    $label.data('placeholder-text', placeholderText);
                    $label.data('original-text', $label.text());

                    if ($field.val() == '') {
                        $field.addClass('empty')
                    }
                });
            },
            swapLabels: function (field) {
                var $field = $(field);
                var $label = $(field).siblings('label').first();
                var isEmpty = Boolean($field.val());

                if (isEmpty) {
                    $field.removeClass('empty');
                    $label.text($label.data('original-text'));
                }
                else {
                    $field.addClass('empty');
                    $label.text($label.data('placeholder-text'));
                }
            }
        }


        // Initialization
        function init() {
            registerEventHandlers();

            actions.initialize();
            self.each(function () {
                actions.swapLabels($(this).find('input,textarea').first());
            });
        }

        init();


        return this;
    };

    $(function () {
        $('.float-label-control').floatLabels();
    });
})(jQuery);


/** ACTUALITZAR LA IMATGE DESTACADA **/

$(document).ready(function ($) {

    $('.imatges-per-triar img').on('click', function () {

        var that = this;
        var immoble_id = $('#immoble_id').val();
        $.post(
            "/public/immobles/" + immoble_id + "/actualitzar-destacada",
            {
                "_token": $(this).find('input[name=_token]').val(),
                "imatge_id": $(this).attr('data-id')

            },
            function (data) {
            },
            'json'
        );

        $('.imatge-triar').removeClass('destacada');
        $(that).parent().addClass('destacada');
        $('#imatge-destacada').attr("src", $(that).attr("src"));

        return false;
    });

});

/** ELIMINAR UNA IMATGE DE L'IMMOBLE **/
$(document).ready(function ($) {

    $('.eliminar-imatge').on('click', function () {

        var imatge = $(this).parent().find('img');
        var imatge_id = imatge.attr('data-id');
        var immoble_id = $('#immoble_id').val();

        $.post(
            "/public/immobles/" + immoble_id + "/eliminar-imatge",
            {
                "_token": $(this).find('input[name=_token]').val(),
                "imatge_id": imatge_id

            },
            function (data) {
            },
            'json'
        );


        imatge.parent().fadeOut(400);

    });
});

/** MODIFICAR VISIBILITAT D'UNA IMATGE DE L'IMMOBLE **/
$(document).ready(function ($) {

    $('.visibilitat-imatge').on('click', function () {

        var imatge = $(this).parent().find('img');
        var visible = $(this).attr('data-id');
        var imatge_id = imatge.attr('data-id');
        var immoble_id = $('#immoble_id').val();

        $.post(
            "/public/immobles/" + immoble_id + "/visibilitat-imatge",
            {
                "_token": $(this).find('input[name=_token]').val(),
                "imatge_id": imatge_id,
                "visible": visible
            },
            function (data) {
            },
            'json'
        );

        if (visible == '0') {
            $(this).attr('data-id', '1');
            $(this).attr('style', 'color:green');
        }
        else {
            $(this).attr('data-id', '0');
            $(this).attr('style', 'color:red');
        }

    });
});

/* Quan actualitzem l'ordre de les imatges */

$(function () {
    $(".imatges-per-triar").sortable({

        update: function (event, ui) {

            var immoble_id = $('#immoble_id').val();

            $.post(
                "/public/immobles/" + immoble_id + "/ordenar_imatges",
                {
                    "_token": $(this).find('input[name=_token]').val(),
                    "imatges": $(this).sortable('toArray')
                }
            );

        }
    });
});

// ACTIVEM ELS TOOLTIPS
$(function () {
    $('[data-toggle="tooltip"]').tooltip(
        {'placement': 'top'}
    );
});

$(document).ready(function ($) {

    $('.edit-immoble-span').on('click', function () {
        var data_value = $(this).attr('data-value');
        var data_name = $(this).attr('data-name');
        var immoble_id = $('#immoble_id').val();
        var that = this;
        $.post(
            "/public/immobles/" + immoble_id + "/modificar-radios",
            {
                "_token": $(this).find('input[name=_token]').val(),
                "data_name": data_name,
                "data_value": data_value
            }
        );

        $(that).attr('data-value', !data_value);

        var title = "";
        if (data_value == 0) {
            if (data_name == "llum" || data_name == "gas" || data_name == "aigua")
                $(that).addClass('label-primary');
            else
                $(that).addClass('text-primary');

            $(that).attr('data-value', 1);
        }
        else {
            $(that).removeClass('text-primary').removeClass('label-primary');
            $(that).attr('data-value', 0);
            title += "no ";
        }
        $(that).attr('data-original-title', title + data_name);

    });


    $('#export span').on('click', function () {

        var data_value = $(this).attr('data-val');

        var data_name = $(this).attr('data-nom');
        var immoble_id = $(this).parent().parent().attr('data-immoble');
        var that = this;

        $.post(
            "/public/immobles/" + immoble_id + "/modificar-radios",
            {
                "_token": $(this).find('input[name=_token]').val(),
                "data_name": data_name,
                "data_value": data_value
            }
        );

        if (data_value == 1)
        {
            $(that).attr('data-val', '0');
            $(that).removeClass('text-primary');
        }
        else
        {
            $(that).attr('data-val', '1');
            $(that).addClass('text-primary');
        }
        updateTotals();

    });

    $(document).on('change', 'select[name=tipus_reclam]', function() {
        var data_value = $(this).val();
        console.log(data_value);
        var immoble_id = $(this).parent().parent().attr('data-immoble');
        $.post(
            "/public/immobles/" + immoble_id + "/modificar-tipus-reclam",
            {
                "_token": $(this).find('input[name=_token]').val(),
                "data_name": 'tipus_reclam',
                "data_value": data_value
            }
        );
    });

    updateTotals();

    function updateTotals()
    {
        var fotocasa = $('.icon-portal');
        var reclam = $('.icon-reclam');
        var habitaclia = $('.icon-portal-habitaclia');
        var web = $('.glyphicon-globe');
        var countFotocasa = 0;
        var countReclam = 0;
        var countHabitaclia = 0;
        var countWeb = 0;

        $.each(fotocasa, function(i, val) {
            if ($(val).attr('data-val') == 1) countFotocasa++;
        });

        $.each(habitaclia, function(i, val) {
            if ($(val).attr('data-val') == 1) countHabitaclia++;
        });

        $.each(reclam, function(i, val) {
            if ($(val).attr('data-val') == 1) countReclam++;
        });

        $.each(web, function(i, val) {
            if ($(val).attr('data-val') == 1) countWeb++;
        });

        if (countFotocasa > 100)
            $('#fotocasa_count').css('color', 'red');
        else
            $('#fotocasa_count').css('color', 'green');

        if (countReclam > 200)
            $('#reclam_count').css('color', 'red');
        else
            $('#reclam_count').css('color', 'green');
        
        $('#fotocasa_count').html(countFotocasa);
        $('#habitaclia_count').html(countHabitaclia);
        $('#reclam_count').html(countReclam);
        $('#web_count').html(countWeb);
    }


});

$(document).ready(function ($) {
// SCRIPT PER CREAR EL CAMP PER CREAR CIUTATS
    $('.afegir_ciutat').click(function () {
        if ($('.ciutat_name').is(':visible')) {
            $('.ciutat_name').toggle();
            $('.ciutat_name').val('');
            $('.ciutat_id').show();
            $('.afegir_ciutat').html('+ Afegir ciutat');
        }
        else {
            $('.ciutat_name').toggle();
            $('.ciutat_id').hide();
            $('.afegir_ciutat').html('- Ciutat per defecte');
        }
        $('select.ciutat_id').hide();
    });
});


/** ELIMINAR DOCUMENTS DE L'IMMOBLE **/
$(document).ready(function ($) {

    $('.remove-file').on('click', function () {

        var that = $(this);
        var fitxer = $(this).attr('data-id');
        var immoble_id = $('#immoble_id').val();

        $.post(
            "/public/immobles/" + immoble_id + "/eliminar-document",
            {
                "_token": $(this).find('input[name=_token]').val(),
                "fitxer": fitxer
            },
            function (data) {
            },
            'json'
        );

        that.parent().parent().parent().fadeOut(400);
    });

    /** ELIMINAR DOCUMENTS DEL CLIENT **/
    $('.remove-file-client').on('click', function () {

        var that = $(this);
        var fitxer = $(this).attr('data-id');
        var immoble_id = $('#immoble_id').val();

        $.post(
            "/public/clients/" + immoble_id + "/eliminar-document",
            {
                "_token": $(this).find('input[name=_token]').val(),
                "fitxer": fitxer
            },
            function (data) {
            },
            'json'
        );

        that.parent().parent().parent().fadeOut(400);
    });
});

$(document).ready(function ($) {
    $('#vendailloguer').on('change', function () {
        var value = $(this).val();
        if (value == 3)
        {
            $('#preu_lloguer').parent().parent().show();
        }
        else  {
            $('#preu_lloguer').parent().parent().hide();
            $('#preu_lloguer').val('');
        }
    });
});

$(document).on('change', 'select[name=tipus_id]', function() {
   var valor = $(this).val();
    $('.extres').hide();
    $('.' + valor).toggle();
});

$(document).on('click', '.accio-modificar', function() {
   $(this).parent().find('.input-accio').show();
   $(this).parent().find('.text-accio').hide();
   $(this).parent().find('.update-accio').show();
   $(this).parent().find('.cancelar-accio').show();
   $(this).parent().find('.eliminar-accio').show();
   
   $(this).hide();
});

$(document).on('click', '.cancelar-accio', function() {
   $(this).parent().find('.input-accio').hide();
   $(this).parent().find('.text-accio').show();
   $(this).parent().find('.update-accio').hide();
   $(this).parent().find('.accio-modificar').show();
   $(this).parent().find('.eliminar-accio').hide();
   $(this).hide();
});